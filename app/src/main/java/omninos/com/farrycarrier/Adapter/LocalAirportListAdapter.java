package omninos.com.farrycarrier.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;


import omninos.com.farrycarrier.R;

public class LocalAirportListAdapter extends RecyclerView.Adapter<LocalAirportListAdapter.MyViewHolder> {
    private Activity activity;
    private List<String> airportList;
    private CallbackForAirports callbackForAirports;

    public LocalAirportListAdapter(Activity activity, List<String> airportList, CallbackForAirports callbackForAirports) {
        this.activity = activity;
        this.airportList = airportList;
        this.callbackForAirports = callbackForAirports;
    }

    @NonNull
    @Override
    public LocalAirportListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_layout_localairportlist, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LocalAirportListAdapter.MyViewHolder myViewHolder, int i) {

        myViewHolder.textViewAirport.setText(airportList.get(i));
    }

    @Override
    public int getItemCount() {
        return airportList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private LinearLayout linearLayout;
        private TextView textViewAirport;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            linearLayout = itemView.findViewById(R.id.linearlayout_airportlist);
            textViewAirport = itemView.findViewById(R.id.tv_airport);
            textViewAirport.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tv_airport:
                    callbackForAirports.callbackForAirports(getAdapterPosition());
                    break;
            }

        }


    }

    public interface CallbackForAirports {
        void callbackForAirports(int i);
    }
}
