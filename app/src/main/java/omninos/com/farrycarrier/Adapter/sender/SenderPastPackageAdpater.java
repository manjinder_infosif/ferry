package omninos.com.farrycarrier.adapter.sender;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.model.sender.SenderPastPackageListModel;

public class SenderPastPackageAdpater extends RecyclerView.Adapter<SenderPastPackageAdpater.MyViewHolder> {
    private Activity activity;
    private List<SenderPastPackageListModel.Detail> pastPackageListModels;

    public SenderPastPackageAdpater(Activity activity, List<SenderPastPackageListModel.Detail> pastPackageListModels) {
        this.activity = activity;
        this.pastPackageListModels = pastPackageListModels;
    }

    @NonNull
    @Override
    public SenderPastPackageAdpater.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_layout_pastpackage, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SenderPastPackageAdpater.MyViewHolder myViewHolder, int i) {
        myViewHolder.packageNumber.setText("C.no: " + pastPackageListModels.get(i).getJobId());
        myViewHolder.packageStatus.setText(pastPackageListModels.get(i).getServiceStatus());
        myViewHolder.packageSize.setText(pastPackageListModels.get(i).getApproximateSize());
        myViewHolder.packageSrcCountry.setText(pastPackageListModels.get(i).getLocalAirportCode());
        myViewHolder.packageDestCountry.setText(pastPackageListModels.get(i).getDestinationAirportCode());
        myViewHolder.packageDate.setText(pastPackageListModels.get(i).getDeliveryDate());
        myViewHolder.packageAmt.setText("$" + pastPackageListModels.get(i).getAmount());

        if (pastPackageListModels.get(i).getServiceStatus().equalsIgnoreCase("1"))
            myViewHolder.packageStatus.setText("Picked Up");
        else if (pastPackageListModels.get(i).getServiceStatus().equalsIgnoreCase("0"))
            myViewHolder.packageStatus.setText("Pending");
        else
            myViewHolder.packageStatus.setText("Delivered");
    }

    @Override
    public int getItemCount() {
        return pastPackageListModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView packageNumber, packageStatus, packageSize, packageSrcCountry, packageDestCountry, packageDate, packageAmt;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            packageNumber = itemView.findViewById(R.id.tv_package_number);
            packageStatus = itemView.findViewById(R.id.tv_package_status);
            packageSize = itemView.findViewById(R.id.tv_package_size);
            packageSrcCountry = itemView.findViewById(R.id.tv_package_src_country);
            packageDestCountry = itemView.findViewById(R.id.tv_package_dest_country);
            packageDate = itemView.findViewById(R.id.tv_package_date);
            packageAmt = itemView.findViewById(R.id.tv_package_amt);
        }
    }
}
