package omninos.com.farrycarrier.adapter.sender;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.activity.sender.PackageDetailsCarrierActivity;
import omninos.com.farrycarrier.model.sender.SenderCurrentPackageListModel;

public class SenderCurrentPackageAdapter extends RecyclerView.Adapter<SenderCurrentPackageAdapter.MyViewHolder> {
    private Activity activity;
    private List<SenderCurrentPackageListModel.Detail> senderCurrentPackageListModalList;


    public SenderCurrentPackageAdapter(Activity activity, List<SenderCurrentPackageListModel.Detail> senderCurrentPackageListModalList) {
        this.activity = activity;
        this.senderCurrentPackageListModalList = senderCurrentPackageListModalList;
    }

    @NonNull
    @Override
    public SenderCurrentPackageAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_layout_sender_currentpackage, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SenderCurrentPackageAdapter.MyViewHolder myViewHolder, final int i) {
        final String courierId = senderCurrentPackageListModalList.get(i).getCarrierId();
        final String courierNumber = senderCurrentPackageListModalList.get(i).getJobId();
        final String courierWeight = senderCurrentPackageListModalList.get(i).getApproximateSize();
        final String courierSourceCountry = senderCurrentPackageListModalList.get(i).getLocalAirportCode();
        final String courierDestinationCountry = senderCurrentPackageListModalList.get(i).getDestinationAirportCode();
        final String courierDate = senderCurrentPackageListModalList.get(i).getDeliveryDate();
        String courierStatus = senderCurrentPackageListModalList.get(i).getServiceStatus();
        final String id = senderCurrentPackageListModalList.get(i).getId();
        final String packageAmount = senderCurrentPackageListModalList.get(i).getAmount();


        myViewHolder.CourierNumber.setText("C.No: " + courierNumber);
        myViewHolder.CourierWeight.setText(courierWeight);
        myViewHolder.CourierSourceCountry.setText(courierSourceCountry);
        myViewHolder.CourierDestinationCountry.setText(courierDestinationCountry);
        myViewHolder.CourierDate.setText(courierDate);
        myViewHolder.packageAmount.setText("$" + packageAmount);

        if (courierStatus.equalsIgnoreCase("1")) {
            myViewHolder.CourierStatus.setText("Picked Up");
        } else if (courierStatus.equalsIgnoreCase("0")) {
            myViewHolder.CourierStatus.setText("Pending");
        } else if (courierStatus.equalsIgnoreCase("3")) {
            myViewHolder.CourierStatus.setText("Canceled");
        } else {
            myViewHolder.CourierStatus.setText("Delivered");
        }

        myViewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, PackageDetailsCarrierActivity.class);
                intent.putExtra("details", senderCurrentPackageListModalList.get(i));
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return senderCurrentPackageListModalList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView CourierNumber, CourierWeight, CourierSourceCountry, CourierDestinationCountry, CourierDate, CourierStatus, packageAmount;
        private LinearLayout linearLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            linearLayout = itemView.findViewById(R.id.linear_layout_current_packages);
            CourierNumber = itemView.findViewById(R.id.tv_courier_number);
            CourierWeight = itemView.findViewById(R.id.tv_courier_weight);
            CourierSourceCountry = itemView.findViewById(R.id.tv_source_country);
            CourierDestinationCountry = itemView.findViewById(R.id.tv_destination_country);
            CourierDate = itemView.findViewById(R.id.tv_date);
            CourierStatus = itemView.findViewById(R.id.tv_status);
            packageAmount = itemView.findViewById(R.id.tv_package_amt);

        }
    }
}
