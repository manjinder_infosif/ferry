package omninos.com.farrycarrier.adapter.sender;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import omninos.com.farrycarrier.activity.ChatActivity;
import omninos.com.farrycarrier.model.sender.RecieverInformationModel;
import omninos.com.farrycarrier.R;

public class NearbyCarrierListAdapter extends RecyclerView.Adapter<NearbyCarrierListAdapter.DriverViewHodler> {
    Activity activity;
    private ClickListner clickListner;
    private List<RecieverInformationModel.Detail> recieverInformationModalList;
    private Context context;

    public NearbyCarrierListAdapter(Activity activity, List<RecieverInformationModel.Detail> recieverInformationModalList, ClickListner clickListner) {
        this.activity = activity;
        this.clickListner = clickListner;
        this.recieverInformationModalList = recieverInformationModalList;
    }

    @NonNull
    @Override
    public DriverViewHodler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(activity).
                inflate(R.layout.item_nearby_sender, parent, false);
        return new DriverViewHodler(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final DriverViewHodler holder, final int position) {
        holder.textViewname.setText(recieverInformationModalList.get(holder.getAdapterPosition()).getName());
        holder.textViewemail.setText(recieverInformationModalList.get(position).getLocalAirportCode()+" -> "+recieverInformationModalList.get(holder.getAdapterPosition()).getDestinationAirportCode());
        holder.textViewphone.setText(recieverInformationModalList.get(holder.getAdapterPosition()).getTravelDate() +","+recieverInformationModalList.get(position).getTravelEndTime());
        holder.tvPackageAmount.setText("$" + recieverInformationModalList.get(holder.getAdapterPosition()).getAmount());
        String Imagepath = recieverInformationModalList.get(holder.getAdapterPosition()).getUserImage();
        if (Imagepath.isEmpty())
            holder.imageViewProfile.setImageResource(R.drawable.ic_profile_placeholder);
        else
            Glide.with(activity).load(Imagepath).into(holder.imageViewProfile);



        holder.dcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + recieverInformationModalList.get(holder.getAdapterPosition()).getPhone()));//change the number
                activity.startActivity(callIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return recieverInformationModalList.size();
    }

    public interface ClickListner {
        void Onclick(int position);
    }


    public class DriverViewHodler extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView dcall, dchat;
        TextView SelectButton, textViewname, textViewemail, textViewphone, tvPackageAmount;
        private ImageView imageViewProfile;


        public DriverViewHodler(View itemView) {
            super(itemView);
            dcall = itemView.findViewById(R.id.dcall);
            dcall.setOnClickListener(this);
            dchat = itemView.findViewById(R.id.dchat);
            dchat.setOnClickListener(this);
            SelectButton = itemView.findViewById(R.id.select_btn);
            SelectButton.setOnClickListener(this);
            imageViewProfile = itemView.findViewById(R.id.profile_img);
            textViewname = itemView.findViewById(R.id.tv_name);
            textViewemail = itemView.findViewById(R.id.tv_email);
            textViewphone = itemView.findViewById(R.id.tv_phone);
            tvPackageAmount = itemView.findViewById(R.id.tv_package_amt);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.dchat:
                    Intent intent = new Intent(activity, ChatActivity.class);
                    intent.putExtra("reciverId", recieverInformationModalList.get(getAdapterPosition()).getUserId());
                    intent.putExtra("name", recieverInformationModalList.get(getAdapterPosition()).getName());
                    activity.startActivity(intent);
//                    activity.startActivity(new Intent(activity, ChatActivity.class));
                    break;

                case R.id.select_btn:
                    clickListner.Onclick(getAdapterPosition());
                    break;
            }

        }
    }
}
