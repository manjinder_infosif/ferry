package omninos.com.farrycarrier.adapter.carrier;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import omninos.com.farrycarrier.model.carrier.PendingRequestListModel;
import omninos.com.farrycarrier.R;

public class PendingRequestsAdapter extends RecyclerView.Adapter<PendingRequestsAdapter.MyViewHolder> {
    private Activity activity;

    private List<PendingRequestListModel.Detail> pendingrequestlist;
    private ClickListener clickListner;


    public PendingRequestsAdapter(Activity activity, List<PendingRequestListModel.Detail> pendingrequestlist, ClickListener clickListner) {
        this.activity = activity;
        this.pendingrequestlist = pendingrequestlist;
        this.clickListner = clickListner;
    }

    @NonNull
    @Override
    public PendingRequestsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_layout_requests, viewGroup, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final PendingRequestsAdapter.MyViewHolder myViewHolder, final int i) {
        String couriernumber=pendingrequestlist.get(i).getJobId();
        String courierweight=pendingrequestlist.get(i).getApproximateSize();
        String courierSourceCountry=pendingrequestlist.get(i).getLocalAirportCode();
        String courierDestinationCountry=pendingrequestlist.get(i).getDestinationAirportCode();
        String courierDate=pendingrequestlist.get(i).getDeliveryDate();
        myViewHolder.CourierNumber.setText(couriernumber);
        myViewHolder.CourierWeight.setText(courierweight);
        myViewHolder.CourierSourceCountry.setText(courierSourceCountry);
        myViewHolder.CourierDestinationCountry.setText(courierDestinationCountry);
        myViewHolder.CourierDate.setText(courierDate);



        myViewHolder.RejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListner.Onclick(i, "reject");
//                pendingrequestlist.remove(i);
//                CommonUtils.showSnackbarAlert(myViewHolder.RejectButton,"Request Rejected!");
//                notifyItemRemoved(i);
//                notifyItemRangeChanged(i,pendingrequestlist.size());
//                notifyDataSetChanged();

            }
        });

        myViewHolder.AcceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListner.Onclick(i, "accept");
//                pendingrequestlist.remove(i);
////                Toast.makeText(activity, "Request Accepted!", Toast.LENGTH_SHORT).show();
//                notifyItemRemoved(i);
//                notifyItemRangeChanged(i,pendingrequestlist.size());
//                notifyDataSetChanged();
            }
        });


    }

    @Override
    public int getItemCount() {
        return pendingrequestlist.size();
    }

    public interface ClickListener {

        void Onclick(int position, String status);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private Button AcceptButton, RejectButton;
        private LinearLayout linearLayoutRequests;
        private TextView CourierNumber, CourierDetails, CourierWeight, CourierSourceCountry, CourierDestinationCountry, CourierDate;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            linearLayoutRequests = itemView.findViewById(R.id.linear_layout_requests);
            AcceptButton = itemView.findViewById(R.id.accept_btn);
            RejectButton = itemView.findViewById(R.id.reject_btn);
            CourierNumber = itemView.findViewById(R.id.tv_courier_number);
            CourierDetails = itemView.findViewById(R.id.tv_courier_details);
            CourierWeight = itemView.findViewById(R.id.tv_courier_weight);
            CourierSourceCountry = itemView.findViewById(R.id.tv_source_country);
            CourierDestinationCountry = itemView.findViewById(R.id.tv_destination_country);
            CourierDate = itemView.findViewById(R.id.tv_date);


        }


    }
}
