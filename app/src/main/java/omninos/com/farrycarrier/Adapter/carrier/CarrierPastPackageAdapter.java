package omninos.com.farrycarrier.adapter.carrier;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.model.carrier.CarrierPastPackageListModel;

public class CarrierPastPackageAdapter extends RecyclerView.Adapter<CarrierPastPackageAdapter.MyViewHolder> {
    private Activity activity;
    private List<CarrierPastPackageListModel.Detail> carrierPastPackageListModelList;

    public CarrierPastPackageAdapter(Activity activity, List<CarrierPastPackageListModel.Detail> carrierPastPackageListModelList) {
        this.activity = activity;
        this.carrierPastPackageListModelList = carrierPastPackageListModelList;
    }

    @NonNull
    @Override
    public CarrierPastPackageAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_layout_pastpackage, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CarrierPastPackageAdapter.MyViewHolder myViewHolder, int i) {
        myViewHolder.packageNumber.setText("C.no: " + carrierPastPackageListModelList.get(i).getJobId());
        myViewHolder.packageStatus.setText(carrierPastPackageListModelList.get(i).getServiceStatus());
        myViewHolder.packageSize.setText(carrierPastPackageListModelList.get(i).getApproximateSize());
        myViewHolder.packageSrcCountry.setText(carrierPastPackageListModelList.get(i).getLocalAirportCode());
        myViewHolder.packageDestCountry.setText(carrierPastPackageListModelList.get(i).getDestinationAirportCode());
        myViewHolder.packageDate.setText(carrierPastPackageListModelList.get(i).getDeliveryDate());
        int amt = Integer.parseInt(carrierPastPackageListModelList.get(i).getAmount()) - 15;
        myViewHolder.packageAmt.setText("$" + amt);

        if (carrierPastPackageListModelList.get(i).getServiceStatus().equalsIgnoreCase("1")) {
            myViewHolder.packageStatus.setText("Picked Up");
        } else if (carrierPastPackageListModelList.get(i).getServiceStatus().equalsIgnoreCase("0")) {
            myViewHolder.packageStatus.setText("Pending");
        } else if (carrierPastPackageListModelList.get(i).getServiceStatus().equalsIgnoreCase("3")) {
            myViewHolder.packageStatus.setText("Canceled");
        } else {
            myViewHolder.packageStatus.setText("Delivered");
        }

    }

    @Override
    public int getItemCount() {
        return carrierPastPackageListModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView packageNumber, packageStatus, packageSize, packageSrcCountry, packageDestCountry, packageDate, packageAmt;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            packageNumber = itemView.findViewById(R.id.tv_package_number);
            packageStatus = itemView.findViewById(R.id.tv_package_status);
            packageSize = itemView.findViewById(R.id.tv_package_size);
            packageSrcCountry = itemView.findViewById(R.id.tv_package_src_country);
            packageDestCountry = itemView.findViewById(R.id.tv_package_dest_country);
            packageDate = itemView.findViewById(R.id.tv_package_date);
            packageAmt = itemView.findViewById(R.id.tv_package_amt);
        }
    }
}
