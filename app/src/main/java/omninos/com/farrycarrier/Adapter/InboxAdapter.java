package omninos.com.farrycarrier.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.util.App;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.model.ConversationModel;

public class InboxAdapter extends RecyclerView.Adapter<InboxAdapter.InboxViewHodler> {

    Activity activity;
    private List<ConversationModel.MessageDetail> conversationList;
    private String type;

    public InboxAdapter(Activity activity,List<ConversationModel.MessageDetail> conversationList) {
        this.activity = activity;
        this.conversationList=conversationList;

    }

    @NonNull
    @Override
    public InboxViewHodler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(activity).
                inflate(R.layout.item_inbox_chat, parent, false);

        return new InboxViewHodler(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InboxViewHodler holder, int position) {
        if (conversationList.get(position).getReciverId().equalsIgnoreCase(App.getAppPreference().GetString(ConstantData.USERID))){
            holder.sendingMsgRL.setVisibility(View.VISIBLE);
            holder.recieveMsgRL.setVisibility(View.GONE);
            holder.tvInputMsg.setText(conversationList.get(position).getMessage());
        }else{
            holder.sendingMsgRL.setVisibility(View.GONE);
            holder.recieveMsgRL.setVisibility(View.VISIBLE);
            holder.tvOutputMsg.setText(conversationList.get(position).getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return conversationList.size();
    }

    public class InboxViewHodler extends RecyclerView.ViewHolder {
    private RelativeLayout sendingMsgRL, recieveMsgRL;
    private TextView tvInputMsg, tvOutputMsg;

        public InboxViewHodler(View itemView) {
            super(itemView);
            sendingMsgRL=itemView.findViewById(R.id.sendingMsgRL);
            recieveMsgRL=itemView.findViewById(R.id.recieveMsgRL);
            tvOutputMsg=itemView.findViewById(R.id.tv_output_message);
            tvInputMsg=itemView.findViewById(R.id.tv_input_message);

        }
    }

}
