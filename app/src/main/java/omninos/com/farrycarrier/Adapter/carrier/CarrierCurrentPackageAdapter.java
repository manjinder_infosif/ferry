package omninos.com.farrycarrier.adapter.carrier;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.activity.carrier.PackageDetailsSenderActivity;
import omninos.com.farrycarrier.model.carrier.CarrierCurrentAcceptedPackagesModel;

public class CarrierCurrentPackageAdapter extends RecyclerView.Adapter<CarrierCurrentPackageAdapter.MyViewHolder> {

    private Activity activity;
    private List<CarrierCurrentAcceptedPackagesModel.Detail> carriercurrentpackageList;


    public CarrierCurrentPackageAdapter(Activity activity, List<CarrierCurrentAcceptedPackagesModel.Detail> carriercurrentpackageList) {
        this.activity = activity;
        this.carriercurrentpackageList = carriercurrentpackageList;
    }

    @NonNull
    @Override
    public CarrierCurrentPackageAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_layout_currentpackages, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CarrierCurrentPackageAdapter.MyViewHolder myViewHolder, final int i) {
        final String bookingId = carriercurrentpackageList.get(i).getId();
        String courierNumber = carriercurrentpackageList.get(i).getJobId();
        String courierSize = carriercurrentpackageList.get(i).getApproximateSize();
        String courierSourceCountry = carriercurrentpackageList.get(i).getLocalAirportCode();
        String courierDestinationCountry = carriercurrentpackageList.get(i).getDestinationAirportCode();
        String date = carriercurrentpackageList.get(i).getDeliveryDate();


        if (carriercurrentpackageList.get(i).getRecieverStatus().equalsIgnoreCase("0")) {
            myViewHolder.textViewStatus.setText("Pending");
        } else if (carriercurrentpackageList.get(i).getRecieverStatus().equalsIgnoreCase("2")) {
            myViewHolder.textViewStatus.setText("Rejected");
        } else {
            String serviceStatus = carriercurrentpackageList.get(i).getServiceStatus();

            if (serviceStatus.equalsIgnoreCase("0")) {
                myViewHolder.textViewStatus.setText("Pending");
                myViewHolder.updateButton.setVisibility(View.VISIBLE);
            } else if (serviceStatus.equalsIgnoreCase("1")) {
                myViewHolder.textViewStatus.setText("Picked up");
                myViewHolder.updateButton.setVisibility(View.VISIBLE);
            } else if (serviceStatus.equalsIgnoreCase("2")) {
                myViewHolder.textViewStatus.setText("Delivered");
                myViewHolder.updateButton.setVisibility(View.VISIBLE);
            } else if (serviceStatus.equalsIgnoreCase("3")) {
                myViewHolder.textViewStatus.setText("Canceled");
                myViewHolder.updateButton.setVisibility(View.GONE);
            }
        }

        myViewHolder.courierNumber.setText(courierNumber);
        myViewHolder.courierSize.setText(courierSize);
        myViewHolder.courierSourceCountry.setText(courierSourceCountry);
        myViewHolder.coruierDestinationCountry.setText(courierDestinationCountry);
        myViewHolder.date.setText(date);


        myViewHolder.updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, PackageDetailsSenderActivity.class);
                intent.putExtra("details", carriercurrentpackageList.get(i));
                activity.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return carriercurrentpackageList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout linearLayout;
        private TextView textViewStatus, courierNumber, courierSize, courierSourceCountry, coruierDestinationCountry, date;
        private Button updateButton;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);

            linearLayout = itemView.findViewById(R.id.linear_layout_currentpackages);
            textViewStatus = itemView.findViewById(R.id.tv_status);
            courierNumber = itemView.findViewById(R.id.tv_courier_number);
            courierSize = itemView.findViewById(R.id.tv_package_size);
            courierSourceCountry = itemView.findViewById(R.id.tv_source_country);
            coruierDestinationCountry = itemView.findViewById(R.id.tv_destination_country);
            date = itemView.findViewById(R.id.tv_date);
            updateButton = itemView.findViewById(R.id.update_btn);

        }
    }
}
