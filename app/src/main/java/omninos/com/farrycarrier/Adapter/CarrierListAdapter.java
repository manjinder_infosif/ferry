package omninos.com.farrycarrier.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.model.CarrierInfoList;

/**
 * Created by Manjinder Singh on 19 , November , 2019
 */
public class CarrierListAdapter extends RecyclerView.Adapter<CarrierListAdapter.MyViewHolder> {
    Context context;
    private List<CarrierInfoList.Detail> list;

    public CarrierListAdapter(Context context, List<CarrierInfoList.Detail> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_carrier_list, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        myViewHolder.enddate.setText("Travel End Date or Time: " + list.get(i).getTravelEndDate() + "," + list.get(i).getTravelEndTime());
        myViewHolder.traveldate.setText("Travel Date or Time: " + list.get(i).getTravelDate() + "," + list.get(i).getTravelTime());
        myViewHolder.sourceCode.setText("Source Airport Code: " + list.get(i).getLocalAirportCode());
        myViewHolder.destinationCode.setText("Destination Airport Code: " + list.get(i).getDestinationAirportCode());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView sourceCode, destinationCode, traveldate, enddate;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            sourceCode = itemView.findViewById(R.id.sourceCode);
            destinationCode = itemView.findViewById(R.id.destinationCode);
            traveldate = itemView.findViewById(R.id.traveldate);
            enddate = itemView.findViewById(R.id.enddate);
        }
    }
}
