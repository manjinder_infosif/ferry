package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import omninos.com.farrycarrier.model.SocialLoginModel;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SocialLoginVM extends ViewModel {
    private MutableLiveData<SocialLoginModel> socialLoginVMMutableLiveData;

    public LiveData<SocialLoginModel> SocialLogin(final Activity activity, String social_id, String name,String lastName, String email, String userImage, String device_type, String reg_id, String login_type ){
        socialLoginVMMutableLiveData=new MutableLiveData<>();
        if(CommonUtils.isNetworkConnected(activity)){
            CommonUtils.showProgress(activity, ConstantData.SHOW_PROGRESS_MESSAGE);

            ApiService api= ApiClient.getApiClient().create(ApiService.class);
            api.socialLogin(social_id, name,lastName, email, userImage, device_type, reg_id, login_type).enqueue(new Callback<SocialLoginModel>() {
                @Override
                public void onResponse(Call<SocialLoginModel> call, Response<SocialLoginModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.isSuccessful()){
                        socialLoginVMMutableLiveData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<SocialLoginModel> call, Throwable t) {

                    CommonUtils.dismissProgress();
                    SocialLoginModel socialLoginModel = new SocialLoginModel();
                    socialLoginModel.setSuccess("0");
                    socialLoginModel.setMessage("Server Error");
                    socialLoginVMMutableLiveData.setValue(socialLoginModel);

                }
            });
        }else{
            SocialLoginModel socialLoginModel = new SocialLoginModel();
            socialLoginModel.setSuccess("0");
            socialLoginModel.setMessage("Network Issue");
            socialLoginVMMutableLiveData.setValue(socialLoginModel);
        }
        return socialLoginVMMutableLiveData;
    }
}
