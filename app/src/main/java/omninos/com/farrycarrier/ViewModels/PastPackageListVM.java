package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;

import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.model.sender.SenderPastPackageListModel;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PastPackageListVM extends ViewModel {
    private MutableLiveData<SenderPastPackageListModel> pastPackageListModelMutableLiveData;

    public LiveData<SenderPastPackageListModel> pastPackageList(final Context activity, String senderId) {
        pastPackageListModelMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {


            ApiService api = ApiClient.getApiClient().create(ApiService.class);
            api.pastpackagelist(senderId).enqueue(new Callback<SenderPastPackageListModel>() {
                @Override
                public void onResponse(Call<SenderPastPackageListModel> call, Response<SenderPastPackageListModel> response) {

                    if (response.isSuccessful()) {
                        pastPackageListModelMutableLiveData.setValue(response.body());
                    }else{ SenderPastPackageListModel senderPastPackageListModel = new SenderPastPackageListModel();
                        senderPastPackageListModel.setSuccess("0");
                        senderPastPackageListModel.setMessage("Server Error");
                        pastPackageListModelMutableLiveData.setValue(senderPastPackageListModel);
                    }
                }

                @Override
                public void onFailure(Call<SenderPastPackageListModel> call, Throwable t) {

                    SenderPastPackageListModel senderPastPackageListModel = new SenderPastPackageListModel();
                    senderPastPackageListModel.setSuccess("0");
                    senderPastPackageListModel.setMessage("Server Error");
                    pastPackageListModelMutableLiveData.setValue(senderPastPackageListModel);
                }
            });
        } else {
            SenderPastPackageListModel senderPastPackageListModel = new SenderPastPackageListModel();
            senderPastPackageListModel.setSuccess("0");
            senderPastPackageListModel.setMessage("Network ISsue");
            pastPackageListModelMutableLiveData.setValue(senderPastPackageListModel);
        }
        return pastPackageListModelMutableLiveData;
    }
}
