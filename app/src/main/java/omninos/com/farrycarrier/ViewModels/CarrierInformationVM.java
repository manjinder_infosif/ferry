package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import omninos.com.farrycarrier.model.carrier.CarrierInformationModel;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarrierInformationVM extends ViewModel {

    private MutableLiveData<CarrierInformationModel> carrierInformationModalMutableLiveData;

    public LiveData<CarrierInformationModel> carrierinformation(final Activity activity, String userId, String localAirportCode, String destinationAirportCode, String travelDate, String travelEndDate, String deliveryStatus,String travelTime,String travelEndTime) {
        carrierInformationModalMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, ConstantData.SHOW_PROGRESS_MESSAGE);
            ApiService api = ApiClient.getApiClient().create(ApiService.class);
            api.carrierInfo(userId, localAirportCode, destinationAirportCode, travelDate, deliveryStatus, travelEndDate,travelTime,travelEndTime).enqueue(new Callback<CarrierInformationModel>() {
                @Override
                public void onResponse(Call<CarrierInformationModel> call, Response<CarrierInformationModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.isSuccessful()) {
                        carrierInformationModalMutableLiveData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<CarrierInformationModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    CarrierInformationModel carrierInformationModel = new CarrierInformationModel();
                    carrierInformationModel.setSuccess("0");
                    carrierInformationModel.setMessage("Server Error");
                    carrierInformationModalMutableLiveData.setValue(carrierInformationModel);
                }
            });
        } else {
            CarrierInformationModel carrierInformationModel = new CarrierInformationModel();
            carrierInformationModel.setSuccess("0");
            carrierInformationModel.setMessage("Network Issue");
            carrierInformationModalMutableLiveData.setValue(carrierInformationModel);
        }
        return carrierInformationModalMutableLiveData;
    }
}
