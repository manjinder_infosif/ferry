package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import omninos.com.farrycarrier.model.carrier.PendingRequestListModel;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PendingRequestListVM extends ViewModel {

    private MutableLiveData<PendingRequestListModel> pendingRequestListModalMutableLiveData;

    public LiveData<PendingRequestListModel> pendingrequestlist(final Activity activity, String carrierId) {
        pendingRequestListModalMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, ConstantData.SHOW_PROGRESS_MESSAGE);
            ApiService api = ApiClient.getApiClient().create(ApiService.class);
            api.pendingrequestlist(carrierId).enqueue(new Callback<PendingRequestListModel>() {
                @Override
                public void onResponse(Call<PendingRequestListModel> call, Response<PendingRequestListModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.isSuccessful()) {
                        pendingRequestListModalMutableLiveData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<PendingRequestListModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    PendingRequestListModel pendingRequestListModel = new PendingRequestListModel();
                    pendingRequestListModel.setSuccess("0");
                    pendingRequestListModel.setMessage("Server Error");
                    pendingRequestListModalMutableLiveData.setValue(pendingRequestListModel);
                }
            });
        } else {
            PendingRequestListModel pendingRequestListModel = new PendingRequestListModel();
            pendingRequestListModel.setSuccess("0");
            pendingRequestListModel.setMessage("Network Issue");
            pendingRequestListModalMutableLiveData.setValue(pendingRequestListModel);

        }
        return pendingRequestListModalMutableLiveData;
    }
}
