package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import omninos.com.farrycarrier.model.SendOTPModel;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendOtpVM extends ViewModel {

    private MutableLiveData<SendOTPModel> sendOTPMutableLiveData;

    public LiveData<SendOTPModel> checkphonenumber(final Activity activity, String phonenumber) {
        sendOTPMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, ConstantData.SHOW_PROGRESS_MESSAGE);

            ApiService api = ApiClient.getApiClient().create(ApiService.class);
            api.checkphonenumber(phonenumber).enqueue(new Callback<SendOTPModel>() {
                @Override
                public void onResponse(Call<SendOTPModel> call, Response<SendOTPModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.isSuccessful()) {
                        sendOTPMutableLiveData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<SendOTPModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    SendOTPModel sendOTPModel = new SendOTPModel();
                    sendOTPModel.setSuccess("0");
                    sendOTPModel.setMessage("Server Error");
                    sendOTPMutableLiveData.setValue(sendOTPModel);
                }
            });
        } else {
            SendOTPModel sendOTPModel = new SendOTPModel();
            sendOTPModel.setSuccess("0");
            sendOTPModel.setMessage("Network Issue");
            sendOTPMutableLiveData.setValue(sendOTPModel);
        }
        return sendOTPMutableLiveData;
    }
}
