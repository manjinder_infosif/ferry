package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import omninos.com.farrycarrier.model.carrier.CarrierBookingModel;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;

public class CarrierBookingVM extends ViewModel {
    private MutableLiveData<CarrierBookingModel> carrierBookingModalMutableLiveData;

    public LiveData<CarrierBookingModel> carrierbooking(final Activity activity, String senderInformationId, String carrierId, String type) {
        carrierBookingModalMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, ConstantData.SHOW_PROGRESS_MESSAGE);
            ApiService api = ApiClient.getApiClient().create(ApiService.class);
//            api.carrierBooking(senderInformationId, carrierId,type).enqueue(new Callback<CarrierBookingModel>() {
//                @Override
//                public void onResponse(Call<CarrierBookingModel> call, Response<CarrierBookingModel> response) {
//                    CommonUtils.dismissProgress();
//                    if (response.isSuccessful()) {
//                        carrierBookingModalMutableLiveData.setValue(response.body());
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<CarrierBookingModel> call, Throwable t) {
//                    CommonUtils.dismissProgress();
//                    CarrierBookingModel carrierBookingModel = new CarrierBookingModel();
//                    carrierBookingModel.setSuccess("0");
//                    carrierBookingModel.setMessage("Server Error");
//                    carrierBookingModalMutableLiveData.setValue(carrierBookingModel);
//                }
//            });
        } else {
            CarrierBookingModel carrierBookingModel = new CarrierBookingModel();
            carrierBookingModel.setSuccess("0");
            carrierBookingModel.setMessage("Network Issue");
            carrierBookingModalMutableLiveData.setValue(carrierBookingModel);
        }
        return carrierBookingModalMutableLiveData;
    }
}
