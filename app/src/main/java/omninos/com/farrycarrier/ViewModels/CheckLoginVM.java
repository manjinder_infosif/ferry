package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import omninos.com.farrycarrier.model.CheckLoginModel;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckLoginVM extends ViewModel {

    private MutableLiveData<CheckLoginModel> checkLoginModalMutableLiveData;

    public LiveData<CheckLoginModel> checklogin(final Activity activity, String email, String password, String reg_id, String device_type, String latitude, String longitude) {
        checkLoginModalMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, ConstantData.SHOW_PROGRESS_MESSAGE);

            ApiService api = ApiClient.getApiClient().create(ApiService.class);
            api.checklogin(email, password, reg_id, device_type,latitude,longitude).enqueue(new Callback<CheckLoginModel>() {
                @Override
                public void onResponse(Call<CheckLoginModel> call, Response<CheckLoginModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.isSuccessful()) {
                        checkLoginModalMutableLiveData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<CheckLoginModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    CheckLoginModel checkLoginModel = new CheckLoginModel();
                    checkLoginModel.setSuccess("0");
                    checkLoginModel.setMessage("Server Error");
                    checkLoginModalMutableLiveData.setValue(checkLoginModel);

                }
            });
        } else {
            CheckLoginModel checkLoginModel = new CheckLoginModel();
            checkLoginModel.setSuccess("0");
            checkLoginModel.setMessage("Network Issue");
            checkLoginModalMutableLiveData.setValue(checkLoginModel);
        }
        return checkLoginModalMutableLiveData;
    }
}
