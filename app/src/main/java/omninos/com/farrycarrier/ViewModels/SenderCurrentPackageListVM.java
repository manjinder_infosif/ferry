package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;

import omninos.com.farrycarrier.model.sender.SenderCurrentPackageListModel;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SenderCurrentPackageListVM extends ViewModel {
    private MutableLiveData<SenderCurrentPackageListModel> senderCurrentPackageListModalMutableLiveData;

    public LiveData<SenderCurrentPackageListModel> sendercurrentpackagelist(final Activity activity, String senderId) {
        senderCurrentPackageListModalMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {

            ApiService api = ApiClient.getApiClient().create(ApiService.class);
            api.sendercurrentpackagelist(senderId).enqueue(new Callback<SenderCurrentPackageListModel>() {
                @Override
                public void onResponse(Call<SenderCurrentPackageListModel> call, Response<SenderCurrentPackageListModel> response) {
                    if (response.isSuccessful()) {
                        senderCurrentPackageListModalMutableLiveData.setValue(response.body());
                    } else {
                        SenderCurrentPackageListModel senderCurrentPackageListModel = new SenderCurrentPackageListModel();
                        senderCurrentPackageListModel.setSuccess("0");
                        senderCurrentPackageListModel.setMessage("Server Error");
                        senderCurrentPackageListModalMutableLiveData.setValue(senderCurrentPackageListModel);
                    }
                }

                @Override
                public void onFailure(Call<SenderCurrentPackageListModel> call, Throwable t) {
                    SenderCurrentPackageListModel senderCurrentPackageListModel = new SenderCurrentPackageListModel();
                    senderCurrentPackageListModel.setSuccess("0");
                    senderCurrentPackageListModel.setMessage("Server Error");
                    senderCurrentPackageListModalMutableLiveData.setValue(senderCurrentPackageListModel);

                }
            });
        } else {
            SenderCurrentPackageListModel senderCurrentPackageListModel = new SenderCurrentPackageListModel();
            senderCurrentPackageListModel.setSuccess("0");
            senderCurrentPackageListModel.setMessage("Network Issue");
            senderCurrentPackageListModalMutableLiveData.setValue(senderCurrentPackageListModel);
        }
        return senderCurrentPackageListModalMutableLiveData;
    }
}
