package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import omninos.com.farrycarrier.model.sender.RecieverInformationModel;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecieverInformationVM extends ViewModel {
    private MutableLiveData<RecieverInformationModel> recieverInformationModalMutableLiveData;

    public LiveData<RecieverInformationModel> recieverInfo(final Activity activity, String senderId) {
        recieverInformationModalMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, ConstantData.SHOW_PROGRESS_MESSAGE);

            ApiService api = ApiClient.getApiClient().create(ApiService.class);
            api.recieverInfo(senderId).enqueue(new Callback<RecieverInformationModel>() {
                @Override
                public void onResponse(Call<RecieverInformationModel> call, Response<RecieverInformationModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.isSuccessful()) {
                        recieverInformationModalMutableLiveData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<RecieverInformationModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    RecieverInformationModel recieverInformationModel = new RecieverInformationModel();
                    recieverInformationModel.setSuccess("0");
                    recieverInformationModel.setMessage("Server Error");
                    recieverInformationModalMutableLiveData.setValue(recieverInformationModel);

                }
            });
        } else {
            RecieverInformationModel recieverInformationModel = new RecieverInformationModel();
            recieverInformationModel.setSuccess("0");
            recieverInformationModel.setMessage("Network Issue");
            recieverInformationModalMutableLiveData.setValue(recieverInformationModel);

        }
        return recieverInformationModalMutableLiveData;
    }
}
