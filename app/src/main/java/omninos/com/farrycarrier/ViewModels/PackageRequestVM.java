package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import omninos.com.farrycarrier.model.carrier.PackageRequestModel;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PackageRequestVM extends ViewModel {
    private MutableLiveData<PackageRequestModel> packageRequestModalMutableLiveData;

    public LiveData<PackageRequestModel> packagerequest(final Activity activity, String type, String bookingServiceId) {
        packageRequestModalMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, ConstantData.SHOW_PROGRESS_MESSAGE);

            ApiService api = ApiClient.getApiClient().create(ApiService.class);
            api.packagerequest(type, bookingServiceId).enqueue(new Callback<PackageRequestModel>() {
                @Override
                public void onResponse(Call<PackageRequestModel> call, Response<PackageRequestModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.isSuccessful()) {
                        packageRequestModalMutableLiveData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<PackageRequestModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    PackageRequestModel packageRequestModel = new PackageRequestModel();
                    packageRequestModel.setSuccess("0");
                    packageRequestModel.setMessage("Server Error");
                    packageRequestModalMutableLiveData.setValue(packageRequestModel);
                }
            });
        } else {
            PackageRequestModel packageRequestModel = new PackageRequestModel();
            packageRequestModel.setSuccess("0");
            packageRequestModel.setMessage("Network Issue");
            packageRequestModalMutableLiveData.setValue(packageRequestModel);

        }
        return packageRequestModalMutableLiveData;
    }
}
