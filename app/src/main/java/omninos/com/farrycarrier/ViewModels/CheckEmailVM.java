package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import omninos.com.farrycarrier.model.CheckEmailModel;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckEmailVM extends ViewModel {


    private MutableLiveData<CheckEmailModel> checkEmailMutableLiveData;

    public LiveData<CheckEmailModel> checkemail(final Activity activity, String email) {

        checkEmailMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, ConstantData.SHOW_PROGRESS_MESSAGE);
            ApiService api = ApiClient.getApiClient().create(ApiService.class);
            api.checkemail(email).enqueue(new Callback<CheckEmailModel>() {
                @Override
                public void onResponse(Call<CheckEmailModel> call, Response<CheckEmailModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.isSuccessful()) {
                        checkEmailMutableLiveData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<CheckEmailModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    CheckEmailModel checkEmailModel = new CheckEmailModel();
                    checkEmailModel.setSuccess("0");
                    checkEmailModel.setMessage("Server Error");
                    checkEmailMutableLiveData.setValue(checkEmailModel);

                }
            });
        } else {
            CheckEmailModel checkEmailModel = new CheckEmailModel();
            checkEmailModel.setSuccess("0");
            checkEmailModel.setMessage("Network Issue");
            checkEmailMutableLiveData.setValue(checkEmailModel);

        }
        return checkEmailMutableLiveData;
    }
}
