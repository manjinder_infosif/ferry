package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import omninos.com.farrycarrier.model.sender.RecieverInformationModel;
import omninos.com.farrycarrier.model.sender.SenderInformationModel;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SenderInformationVM extends ViewModel {
    private MutableLiveData<RecieverInformationModel> senderInformationModalMutableLiveData;

    public LiveData<RecieverInformationModel> senderinformation(final Activity activity, String userId, String packageType, String approximateSize, String country, String localAirportCode, String destinationAirportCode, String deliveryDate, String suggestion) {
        senderInformationModalMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, ConstantData.SHOW_PROGRESS_MESSAGE);
            ApiService api = ApiClient.getApiClient().create(ApiService.class);
            api.senderInfo(userId, packageType, approximateSize, country, localAirportCode, destinationAirportCode, deliveryDate, suggestion).enqueue(new Callback<RecieverInformationModel>() {
                @Override
                public void onResponse(Call<RecieverInformationModel> call, Response<RecieverInformationModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.isSuccessful()) {
                        senderInformationModalMutableLiveData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<RecieverInformationModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    RecieverInformationModel senderInformationModel = new RecieverInformationModel();
                    senderInformationModel.setSuccess("0");
                    senderInformationModel.setMessage("Server Error");
                    senderInformationModalMutableLiveData.setValue(senderInformationModel);
                }
            });
        } else {
            RecieverInformationModel senderInformationModel = new RecieverInformationModel();
            senderInformationModel.setSuccess("0");
            senderInformationModel.setMessage("Network Issue");
            senderInformationModalMutableLiveData.setValue(senderInformationModel);
        }
        return senderInformationModalMutableLiveData;
    }
}
