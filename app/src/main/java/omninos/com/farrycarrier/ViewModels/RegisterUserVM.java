package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import omninos.com.farrycarrier.model.RegisterUserModel;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterUserVM extends ViewModel {
    private MutableLiveData<RegisterUserModel> registerUserModalMutableLiveData;

    public LiveData<RegisterUserModel> registeruser(final Activity activity, String name, String email, String phone, String password, String device_type, String reg_id, String longitude, String latitude,String lastName) {
        registerUserModalMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, ConstantData.SHOW_PROGRESS_MESSAGE);

            ApiService api = ApiClient.getApiClient().create(ApiService.class);
            api.registerUser(name, email, phone, password, device_type, reg_id, longitude, latitude,lastName).enqueue(new Callback<RegisterUserModel>() {
                @Override
                public void onResponse(Call<RegisterUserModel> call, Response<RegisterUserModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.isSuccessful()) {
                        registerUserModalMutableLiveData.setValue(response.body());
                    }
                }
                @Override
                public void onFailure(Call<RegisterUserModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    RegisterUserModel registerUserModel = new RegisterUserModel();
                    registerUserModel.setSuccess("0");
                    registerUserModel.setMessage("Server Error");
                    registerUserModalMutableLiveData.setValue(registerUserModel);
                }
            });
        } else {
            RegisterUserModel registerUserModel = new RegisterUserModel();
            registerUserModel.setSuccess("0");
            registerUserModel.setMessage("Network Issue");
            registerUserModalMutableLiveData.setValue(registerUserModel);
        }
        return registerUserModalMutableLiveData;
    }
}
