package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import omninos.com.farrycarrier.model.carrier.CarrierCurrentAcceptedPackagesModel;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarrierCurrentAcceptedPackageVM extends ViewModel {

    private MutableLiveData<CarrierCurrentAcceptedPackagesModel> carrierCurrentAcceptedPackagesModalMutableLiveData;

    public LiveData<CarrierCurrentAcceptedPackagesModel> carriercurrentpackagelist(final Activity activity, String carrierId) {
        carrierCurrentAcceptedPackagesModalMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            ApiService api = ApiClient.getApiClient().create(ApiService.class);
            api.carriercurrentpackagelist(carrierId).enqueue(new Callback<CarrierCurrentAcceptedPackagesModel>() {
                @Override
                public void onResponse(Call<CarrierCurrentAcceptedPackagesModel> call, Response<CarrierCurrentAcceptedPackagesModel> response) {
                    if (response.isSuccessful()) {
                        carrierCurrentAcceptedPackagesModalMutableLiveData.setValue(response.body());
                    } else {
                        CarrierCurrentAcceptedPackagesModel carrierCurrentAcceptedPackagesModel = new CarrierCurrentAcceptedPackagesModel();
                        carrierCurrentAcceptedPackagesModel.setSuccess("0");
                        carrierCurrentAcceptedPackagesModel.setMessage("Server Error");
                        carrierCurrentAcceptedPackagesModalMutableLiveData.setValue(carrierCurrentAcceptedPackagesModel);
                    }
                }

                @Override
                public void onFailure(Call<CarrierCurrentAcceptedPackagesModel> call, Throwable t) {
                    CarrierCurrentAcceptedPackagesModel carrierCurrentAcceptedPackagesModel = new CarrierCurrentAcceptedPackagesModel();
                    carrierCurrentAcceptedPackagesModel.setSuccess("0");
                    carrierCurrentAcceptedPackagesModel.setMessage("Server Error");
                    carrierCurrentAcceptedPackagesModalMutableLiveData.setValue(carrierCurrentAcceptedPackagesModel);
                }
            });
        } else {
            CarrierCurrentAcceptedPackagesModel carrierCurrentAcceptedPackagesModel = new CarrierCurrentAcceptedPackagesModel();
            carrierCurrentAcceptedPackagesModel.setSuccess("0");
            carrierCurrentAcceptedPackagesModel.setMessage("Network Issue");
            carrierCurrentAcceptedPackagesModalMutableLiveData.setValue(carrierCurrentAcceptedPackagesModel);
        }
        return carrierCurrentAcceptedPackagesModalMutableLiveData;
    }
}
