package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.model.PayPalPaymentModel;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PayPalPaymentVM extends ViewModel {
    private MutableLiveData<PayPalPaymentModel> payPalPaymentModelMutableLiveData;

    public LiveData<PayPalPaymentModel> payPalPayment(final Activity activity, String bookingServiceId, String amount, String token, String senderId, String carrierId) {
        payPalPaymentModelMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, ConstantData.SHOW_PROGRESS_MESSAGE);
            ApiService api = ApiClient.getApiClient().create(ApiService.class);
            api.paypalpayments(bookingServiceId, amount, token, senderId, carrierId).enqueue(new Callback<PayPalPaymentModel>() {
                @Override
                public void onResponse(Call<PayPalPaymentModel> call, Response<PayPalPaymentModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.isSuccessful()) {
                        payPalPaymentModelMutableLiveData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<PayPalPaymentModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    PayPalPaymentModel payPalPaymentModel = new PayPalPaymentModel();
                    payPalPaymentModel.setSuccess("0");
                    payPalPaymentModel.setMessage("Server Error");
                    payPalPaymentModelMutableLiveData.setValue(payPalPaymentModel);
                }
            });
        } else {
            PayPalPaymentModel payPalPaymentModel = new PayPalPaymentModel();
            payPalPaymentModel.setSuccess("0");
            payPalPaymentModel.setMessage("Network Issue");
            payPalPaymentModelMutableLiveData.setValue(payPalPaymentModel);
        }
        return payPalPaymentModelMutableLiveData;
    }
}
