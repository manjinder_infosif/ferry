package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.model.InboxModel;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InboxVM extends ViewModel {
    private MutableLiveData<InboxModel> inboxModelMutableLiveData;

    public LiveData<InboxModel> userInbox(final Activity activity, String sender_id) {
        inboxModelMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, ConstantData.SHOW_PROGRESS_MESSAGE);

            ApiService api = ApiClient.getApiClient().create(ApiService.class);
            api.userinbox(sender_id).enqueue(new Callback<InboxModel>() {
                @Override
                public void onResponse(Call<InboxModel> call, Response<InboxModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.isSuccessful()) {
                        inboxModelMutableLiveData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<InboxModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    InboxModel inboxModel = new InboxModel();
                    inboxModel.setSuccess("0");
                    inboxModel.setMessage("Server Error");
                    inboxModelMutableLiveData.setValue(inboxModel);
                }
            });
        } else {
            InboxModel inboxModel = new InboxModel();
            inboxModel.setSuccess("0");
            inboxModel.setMessage("Server Error");
            inboxModelMutableLiveData.setValue(inboxModel);
        }
        return inboxModelMutableLiveData;
    }
}
