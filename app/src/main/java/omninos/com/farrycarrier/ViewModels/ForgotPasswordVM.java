package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.model.ForgotPasswordModel;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordVM extends ViewModel {

    private MutableLiveData<ForgotPasswordModel> forgotPasswordModelMutableLiveData;

    public LiveData<ForgotPasswordModel> forgotPassword(final Activity activity, String email) {
        forgotPasswordModelMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, ConstantData.SHOW_PROGRESS_MESSAGE);
            ApiService api = ApiClient.getApiClient().create(ApiService.class);
            api.forgotpassword(email).enqueue(new Callback<ForgotPasswordModel>() {
                @Override
                public void onResponse(Call<ForgotPasswordModel> call, Response<ForgotPasswordModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.isSuccessful()) {
                        forgotPasswordModelMutableLiveData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<ForgotPasswordModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    ForgotPasswordModel forgotPasswordModel = new ForgotPasswordModel();
                    forgotPasswordModel.setSuccess("0");
                    forgotPasswordModel.setMessage("Server Error");
                    forgotPasswordModelMutableLiveData.setValue(forgotPasswordModel);
                }
            });
        } else {
            ForgotPasswordModel forgotPasswordModel = new ForgotPasswordModel();
            forgotPasswordModel.setSuccess("0");
            forgotPasswordModel.setMessage("Network Issue");
            forgotPasswordModelMutableLiveData.setValue(forgotPasswordModel);
        }
        return forgotPasswordModelMutableLiveData;
    }

}
