package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import omninos.com.farrycarrier.model.carrier.CarrierPastPackageListModel;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import omninos.com.farrycarrier.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarrierPastPackageListVM extends ViewModel {
    private MutableLiveData<CarrierPastPackageListModel> pastPackageListModelMutableLiveData;

    public LiveData<CarrierPastPackageListModel> carrierPastPackageList(final Activity activity, String careerId) {
        pastPackageListModelMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            ApiService api = ApiClient.getApiClient().create(ApiService.class);
            api.carrierpastpackagelist(careerId).enqueue(new Callback<CarrierPastPackageListModel>() {
                @Override
                public void onResponse(Call<CarrierPastPackageListModel> call, Response<CarrierPastPackageListModel> response) {
                    if (response.isSuccessful()) {
                        pastPackageListModelMutableLiveData.setValue(response.body());
                    } else {
                        CarrierPastPackageListModel carrierPastPackageListModel = new CarrierPastPackageListModel();
                        carrierPastPackageListModel.setSuccess("0");
                        carrierPastPackageListModel.setMessage("Server Error");
                        pastPackageListModelMutableLiveData.setValue(carrierPastPackageListModel);
                    }
                }

                @Override
                public void onFailure(Call<CarrierPastPackageListModel> call, Throwable t) {
                    CarrierPastPackageListModel carrierPastPackageListModel = new CarrierPastPackageListModel();
                    carrierPastPackageListModel.setSuccess("0");
                    carrierPastPackageListModel.setMessage("Server Error");
                    pastPackageListModelMutableLiveData.setValue(carrierPastPackageListModel);
                }
            });
        } else {
            CarrierPastPackageListModel carrierPastPackageListModel = new CarrierPastPackageListModel();
            carrierPastPackageListModel.setSuccess("0");
            carrierPastPackageListModel.setMessage("Network Issue");
            pastPackageListModelMutableLiveData.setValue(carrierPastPackageListModel);
        }
        return pastPackageListModelMutableLiveData;
    }

}
