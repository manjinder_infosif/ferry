package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import omninos.com.farrycarrier.model.WalletRechargeModel;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WalletRechargeVM extends ViewModel {
    private MutableLiveData<WalletRechargeModel> walletRechargeModelMutableLiveData;

    public LiveData<WalletRechargeModel> walletRecharge(final Activity activity, String amount, String userId, String nonce) {
        walletRechargeModelMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, ConstantData.SHOW_PROGRESS_MESSAGE);

            ApiService api = ApiClient.getApiClient().create(ApiService.class);
            api.walletrecharge(amount, userId, nonce).enqueue(new Callback<WalletRechargeModel>() {
                @Override
                public void onResponse(Call<WalletRechargeModel> call, Response<WalletRechargeModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.isSuccessful()) {
                        walletRechargeModelMutableLiveData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<WalletRechargeModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    WalletRechargeModel walletRechargeModel = new WalletRechargeModel();
                    walletRechargeModel.setSuccess("0");
                    walletRechargeModel.setMessage("Server Error");
                    walletRechargeModelMutableLiveData.setValue(walletRechargeModel);
                }
            });
        } else {
            WalletRechargeModel walletRechargeModel = new WalletRechargeModel();
            walletRechargeModel.setSuccess("0");
            walletRechargeModel.setMessage("Server Error");
            walletRechargeModelMutableLiveData.setValue(walletRechargeModel);
        }
        return walletRechargeModelMutableLiveData;
    }
}
