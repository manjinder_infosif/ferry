package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import omninos.com.farrycarrier.model.WalletBalanceModel;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WalletBalanceVM extends ViewModel {

    private MutableLiveData<WalletBalanceModel> walletBalanceModelMutableLiveData;

    public LiveData<WalletBalanceModel> walletBalance(final Activity activity, String userId) {
        walletBalanceModelMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, ConstantData.SHOW_PROGRESS_MESSAGE);

            ApiService api = ApiClient.getApiClient().create(ApiService.class);
            api.walletbalance(userId).enqueue(new Callback<WalletBalanceModel>() {
                @Override
                public void onResponse(Call<WalletBalanceModel> call, Response<WalletBalanceModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.isSuccessful())
                        walletBalanceModelMutableLiveData.setValue(response.body());
                }

                @Override
                public void onFailure(Call<WalletBalanceModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    WalletBalanceModel walletBalanceModel = new WalletBalanceModel();
                    walletBalanceModel.setSuccess("0");
                    walletBalanceModel.setMessage("Server Error");
                    walletBalanceModelMutableLiveData.setValue(walletBalanceModel);
                }
            });

        } else {
            WalletBalanceModel walletBalanceModel = new WalletBalanceModel();
            walletBalanceModel.setSuccess("0");
            walletBalanceModel.setMessage("Network Issue");
            walletBalanceModelMutableLiveData.setValue(walletBalanceModel);
        }

        return walletBalanceModelMutableLiveData;
    }
}
