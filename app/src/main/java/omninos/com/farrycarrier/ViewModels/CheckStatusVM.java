package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import omninos.com.farrycarrier.model.CheckStatusModel;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckStatusVM extends ViewModel {
    private MutableLiveData<CheckStatusModel> checkStatusMutableLiveData;

    public LiveData<CheckStatusModel> checkStatus(final Activity activity, String Status, String userId, String reg_id) {
        checkStatusMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, ConstantData.SHOW_PROGRESS_MESSAGE);
            ApiService api = ApiClient.getApiClient().create(ApiService.class);
            api.checkUserType(Status, userId, reg_id).enqueue(new Callback<CheckStatusModel>() {
                @Override
                public void onResponse(Call<CheckStatusModel> call, Response<CheckStatusModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.isSuccessful()) {
                        checkStatusMutableLiveData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<CheckStatusModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    CheckStatusModel checkStatusModel = new CheckStatusModel();
                    checkStatusModel.setSuccess("0");
                    checkStatusModel.setMessage("Server Error");
                    checkStatusMutableLiveData.setValue(checkStatusModel);
                }
            });
        } else {
            CommonUtils.dismissProgress();
            CheckStatusModel checkStatusModel = new CheckStatusModel();
            checkStatusModel.setSuccess("0");
            checkStatusModel.setMessage("Network Issue");
            checkStatusMutableLiveData.setValue(checkStatusModel);
        }
        return checkStatusMutableLiveData;
    }
}
