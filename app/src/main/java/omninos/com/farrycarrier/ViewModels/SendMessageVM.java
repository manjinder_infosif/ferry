package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.model.sender.SendMessageModel;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendMessageVM extends ViewModel {
    private MutableLiveData<SendMessageModel> sendMessageModelMutableLiveData;

    public LiveData<SendMessageModel> sendMessage(final Activity activity, String sender_id, String reciver_id, String message) {
        sendMessageModelMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, ConstantData.SHOW_PROGRESS_MESSAGE);
            ApiService api = ApiClient.getApiClient().create(ApiService.class);
            api.sendmessage(sender_id, reciver_id, message).enqueue(new Callback<SendMessageModel>() {
                @Override
                public void onResponse(Call<SendMessageModel> call, Response<SendMessageModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.isSuccessful()) {
                        sendMessageModelMutableLiveData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<SendMessageModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    SendMessageModel sendMessageModel = new SendMessageModel();
                    sendMessageModel.setSuccess("0");
                    sendMessageModel.setMessage("Server Error");
                    sendMessageModelMutableLiveData.setValue(sendMessageModel);

                }
            });
        } else {
            SendMessageModel sendMessageModel = new SendMessageModel();
            sendMessageModel.setSuccess("0");
            sendMessageModel.setMessage("Network Issue");
            sendMessageModelMutableLiveData.setValue(sendMessageModel);
        }
        return sendMessageModelMutableLiveData;
    }
}
