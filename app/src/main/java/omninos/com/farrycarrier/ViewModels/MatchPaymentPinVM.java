package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.model.MatchPaymentPinModel;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MatchPaymentPinVM extends ViewModel {
    private MutableLiveData<MatchPaymentPinModel> matchPaymentPinModelMutableLiveData;

    public LiveData<MatchPaymentPinModel> matchpaymentpin(final Activity activity, String bookingServiceId, String pinNumber) {
        matchPaymentPinModelMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, ConstantData.SHOW_PROGRESS_MESSAGE);
            ApiService api = ApiClient.getApiClient().create(ApiService.class);
            api.matchpaymentpin(bookingServiceId, pinNumber).enqueue(new Callback<MatchPaymentPinModel>() {
                @Override
                public void onResponse(Call<MatchPaymentPinModel> call, Response<MatchPaymentPinModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.isSuccessful()) {
                        matchPaymentPinModelMutableLiveData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<MatchPaymentPinModel> call, Throwable t) {
                    MatchPaymentPinModel matchPaymentPinModel = new MatchPaymentPinModel();
                    matchPaymentPinModel.setSuccess("0");
                    matchPaymentPinModel.setMessage("Server Error");
                    matchPaymentPinModelMutableLiveData.setValue(matchPaymentPinModel);
                }
            });
        } else {
            MatchPaymentPinModel matchPaymentPinModel = new MatchPaymentPinModel();
            matchPaymentPinModel.setSuccess("0");
            matchPaymentPinModel.setMessage("Network Issue");
            matchPaymentPinModelMutableLiveData.setValue(matchPaymentPinModel);
        }
        return matchPaymentPinModelMutableLiveData;
    }
}
