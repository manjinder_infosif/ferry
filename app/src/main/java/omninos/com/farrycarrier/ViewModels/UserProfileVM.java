package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import omninos.com.farrycarrier.model.UserProfileModel;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserProfileVM extends ViewModel {
    private MutableLiveData<UserProfileModel> userProfileModalMutableLiveData;

    public LiveData<UserProfileModel> userprofile(final Activity activity, String userId) {
        userProfileModalMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, ConstantData.SHOW_PROGRESS_MESSAGE);

            ApiService api = ApiClient.getApiClient().create(ApiService.class);
            api.userprofile(userId).enqueue(new Callback<UserProfileModel>() {
                @Override
                public void onResponse(Call<UserProfileModel> call, Response<UserProfileModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.isSuccessful()) {
                        userProfileModalMutableLiveData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<UserProfileModel> call, Throwable t) {
                    UserProfileModel userProfileModel = new UserProfileModel();
                    userProfileModel.setSuccess("0");
                    userProfileModel.setMessage("Server Error");
                    userProfileModalMutableLiveData.setValue(userProfileModel);
                }
            });
        } else {
            UserProfileModel userProfileModel = new UserProfileModel();
            userProfileModel.setSuccess("0");
            userProfileModel.setMessage("Network Issue");
            userProfileModalMutableLiveData.setValue(userProfileModel);
        }
        return userProfileModalMutableLiveData;
    }
}
