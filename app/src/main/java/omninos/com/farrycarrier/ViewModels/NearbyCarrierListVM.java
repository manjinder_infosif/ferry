package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import omninos.com.farrycarrier.model.sender.NearbyCarrierListModel;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NearbyCarrierListVM extends ViewModel {
    private MutableLiveData<NearbyCarrierListModel> pendingRequestListModalMutableLiveData;

    public LiveData<NearbyCarrierListModel> nearbycarrierlist(final Activity activity, String senderInformationId, String carrierInformationId, String senderId, String carrierId, String type) {
        pendingRequestListModalMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, ConstantData.SHOW_PROGRESS_MESSAGE);
            ApiService api = ApiClient.getApiClient().create(ApiService.class);
            api.nearbycarrierlist(senderInformationId, carrierInformationId, senderId, carrierId,type).enqueue(new Callback<NearbyCarrierListModel>() {
                @Override
                public void onResponse(Call<NearbyCarrierListModel> call, Response<NearbyCarrierListModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.isSuccessful()) {
                        pendingRequestListModalMutableLiveData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<NearbyCarrierListModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    NearbyCarrierListModel nearbyCarrierListModel = new NearbyCarrierListModel();
                    nearbyCarrierListModel.setSuccess("0");
                    nearbyCarrierListModel.setMessage("Server Error");
                    pendingRequestListModalMutableLiveData.setValue(nearbyCarrierListModel);
                }
            });
        } else {
            NearbyCarrierListModel nearbyCarrierListModel = new NearbyCarrierListModel();
            nearbyCarrierListModel.setSuccess("0");
            nearbyCarrierListModel.setMessage("Network Issue");
            pendingRequestListModalMutableLiveData.setValue(nearbyCarrierListModel);

        }
        return pendingRequestListModalMutableLiveData;
    }
}
