package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.widget.Toast;

import java.util.Map;

import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import omninos.com.farrycarrier.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccountDetailsViewModel extends ViewModel {

    private MutableLiveData<Map> mapMutableLiveData;

    private MutableLiveData<Map> requestMoney;

    public LiveData<Map> Account(Activity activity, String userId, String accountNumber, String accountName, String brachName, String ifsc,String routingNumber,String username) {
        if (CommonUtils.isNetworkConnected(activity)) {

            CommonUtils.showProgress(activity,"Please wait...");
            mapMutableLiveData = new MutableLiveData<>();

            ApiService apiService = ApiClient.getApiClient().create(ApiService.class);

            apiService.addDetail(userId, accountNumber, accountName, brachName, ifsc,routingNumber,username).enqueue(new Callback<Map>() {
                @Override
                public void onResponse(Call<Map> call, Response<Map> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        mapMutableLiveData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<Map> call, Throwable t) {
                    CommonUtils.dismissProgress();
                }
            });

        } else {
            Toast.makeText(activity, "Network Issue", Toast.LENGTH_SHORT).show();
        }

        return mapMutableLiveData;
    }


    public LiveData<Map> requestMoneyData(Activity activity,String userId){
        requestMoney=new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)){

            CommonUtils.showProgress(activity,"Please wait...");

            ApiService apiService=ApiClient.getApiClient().create(ApiService.class);
            apiService.requestMoney(userId).enqueue(new Callback<Map>() {
                @Override
                public void onResponse(Call<Map> call, Response<Map> response) {
                    CommonUtils.dismissProgress();
                    if (response.body()!=null){
                        requestMoney.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<Map> call, Throwable t) {
                    CommonUtils.dismissProgress();
                }
            });

        }else {
            Toast.makeText(activity, "Network Issue", Toast.LENGTH_SHORT).show();
        }

        return requestMoney;
    }
}
