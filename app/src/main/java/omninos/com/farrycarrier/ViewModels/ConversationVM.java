package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.model.ConversationModel;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConversationVM extends ViewModel {
    private MutableLiveData<ConversationModel> conversationModelMutableLiveData;

    public LiveData<ConversationModel> userConversation(final Activity activity, String sender_id, String reciver_id) {
        conversationModelMutableLiveData = new MutableLiveData<>();

        if (CommonUtils.isNetworkConnected(activity)) {
            ApiService api = ApiClient.getApiClient().create(ApiService.class);
            api.userconversation(sender_id, reciver_id).enqueue(new Callback<ConversationModel>() {
                @Override
                public void onResponse(Call<ConversationModel> call, Response<ConversationModel> response) {
                    if (response.isSuccessful()) {
                        conversationModelMutableLiveData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<ConversationModel> call, Throwable t) {
                    ConversationModel conversationModel = new ConversationModel();
                    conversationModel.setSuccess("0");
                    conversationModel.setMessage("Server Error");
                    conversationModelMutableLiveData.setValue(conversationModel);
                }
            });
        } else {
            ConversationModel conversationModel = new ConversationModel();
            conversationModel.setSuccess("0");
            conversationModel.setMessage("Server Error");
            conversationModelMutableLiveData.setValue(conversationModel);
        }
        return conversationModelMutableLiveData;
    }
}
