package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import omninos.com.farrycarrier.model.AirportListModel;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AirportListVM extends ViewModel {

    private MutableLiveData<AirportListModel> airportListModalMutableLiveData;

    public LiveData<AirportListModel> airportlist(final Activity activity, String countryName, String search ){
        airportListModalMutableLiveData=new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)){
            CommonUtils.showProgress(activity, ConstantData.SHOW_PROGRESS_MESSAGE);
            ApiService api= ApiClient.getApiClient().create(ApiService.class);
            api.airportCodeList(countryName, search).enqueue(new Callback<AirportListModel>() {
                @Override
                public void onResponse(Call<AirportListModel> call, Response<AirportListModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.isSuccessful()){
                        airportListModalMutableLiveData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<AirportListModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    AirportListModel airportListModel = new AirportListModel();
                    airportListModel.setSuccess("0");
                    airportListModel.setMessage("Server Error");
                    airportListModalMutableLiveData.setValue(airportListModel);


                }
            });
        }else{
            AirportListModel airportListModel = new AirportListModel();
            airportListModel.setSuccess("0");
            airportListModel.setMessage("Network Issue");
            airportListModalMutableLiveData.setValue(airportListModel);
        }
        return airportListModalMutableLiveData;
    }
}
