package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import omninos.com.farrycarrier.model.UpdateProfileModel;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateProfileVM extends ViewModel {
    private MutableLiveData<UpdateProfileModel> updateProfileModalMutableLiveData;

    public LiveData<UpdateProfileModel> updateprofile(final Activity activity, RequestBody userIdbody, RequestBody phonebody, RequestBody addressbody, RequestBody workExperiencebody, RequestBody namebody,RequestBody lastName, MultipartBody.Part userImage) {
        updateProfileModalMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, ConstantData.SHOW_PROGRESS_MESSAGE);

            ApiService api = ApiClient.getApiClient().create(ApiService.class);
            api.updateprofile(userIdbody, phonebody, addressbody, workExperiencebody, namebody,lastName, userImage).enqueue(new Callback<UpdateProfileModel>() {
                @Override
                public void onResponse(Call<UpdateProfileModel> call, Response<UpdateProfileModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.isSuccessful()) {
                        updateProfileModalMutableLiveData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<UpdateProfileModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    UpdateProfileModel updateProfileModel = new UpdateProfileModel();
                    updateProfileModel.setSuccess("0");
                    updateProfileModel.setMessage("Server Error");
                    updateProfileModalMutableLiveData.setValue(updateProfileModel);
                }
            });
        } else {
            UpdateProfileModel updateProfileModel = new UpdateProfileModel();
            updateProfileModel.setSuccess("0");
            updateProfileModel.setMessage("Network Issue");
            updateProfileModalMutableLiveData.setValue(updateProfileModel);
        }
        return updateProfileModalMutableLiveData;
    }
}
