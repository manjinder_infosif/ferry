package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.widget.Toast;

import java.util.Map;

import omninos.com.farrycarrier.model.CarrierInfoList;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import omninos.com.farrycarrier.util.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Manjinder Singh on 19 , November , 2019
 */
public class CancelPackageViewModel extends ViewModel {

    private MutableLiveData<Map> cancel;

    private MutableLiveData<CarrierInfoList> carrierInfoListMutableLiveData;


    public LiveData<Map> PackageCancel(Activity activity, String jobId) {
        cancel = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, "Please Wait...");
            ApiService apiService = ApiClient.getApiClient().create(ApiService.class);
            apiService.cancelPackage(jobId).enqueue(new Callback<Map>() {
                @Override
                public void onResponse(Call<Map> call, Response<Map> response) {
                    CommonUtils.dismissProgress();
                    if (response.isSuccessful()) {
                        cancel.setValue(response.body());
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<Map> call, Throwable t) {
                    CommonUtils.dismissProgress();
                }
            });

        } else {
            Toast.makeText(activity, "Network Issue", Toast.LENGTH_SHORT).show();
        }
        return cancel;
    }

    public LiveData<CarrierInfoList> carrierInfoListLiveData(Activity activity, String userId) {
        carrierInfoListMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, "Please wait...");
            ApiService apiService = ApiClient.getApiClient().create(ApiService.class);
            apiService.getCarrierList(userId).enqueue(new Callback<CarrierInfoList>() {
                @Override
                public void onResponse(Call<CarrierInfoList> call, Response<CarrierInfoList> response) {
                    CommonUtils.dismissProgress();
                    if (response.isSuccessful()) {
                        carrierInfoListMutableLiveData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<CarrierInfoList> call, Throwable t) {
                    CommonUtils.dismissProgress();
                }
            });
        } else {
            Toast.makeText(activity, "Network Issue", Toast.LENGTH_SHORT).show();
        }
        return carrierInfoListMutableLiveData;
    }
}
