package omninos.com.farrycarrier.viewmodels;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import omninos.com.farrycarrier.model.carrier.UpdatePackageStatusModel;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdatePackageStatusVM extends ViewModel {
    private MutableLiveData<UpdatePackageStatusModel> updatePackageStatusModalMutableLiveData;

    public LiveData<UpdatePackageStatusModel> updatepackagestatus(final Activity activity, String bookingServiceId, String serviceStatus, String type) {
        updatePackageStatusModalMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity, ConstantData.SHOW_PROGRESS_MESSAGE);
            ApiService api = ApiClient.getApiClient().create(ApiService.class);
            api.updateservicestatus(bookingServiceId, serviceStatus,type).enqueue(new Callback<UpdatePackageStatusModel>() {
                @Override
                public void onResponse(Call<UpdatePackageStatusModel> call, Response<UpdatePackageStatusModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.isSuccessful()) {
                        updatePackageStatusModalMutableLiveData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<UpdatePackageStatusModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    UpdatePackageStatusModel updatePackageStatusModel =new UpdatePackageStatusModel();
                    updatePackageStatusModel.setSuccess("0");
                    updatePackageStatusModel.setMessage("Server Error");
                    updatePackageStatusModalMutableLiveData.setValue(updatePackageStatusModel);
                }
            });
        } else {
            CommonUtils.dismissProgress();
            UpdatePackageStatusModel updatePackageStatusModel =new UpdatePackageStatusModel();
            updatePackageStatusModel.setSuccess("0");
            updatePackageStatusModel.setMessage("Network Issue");
            updatePackageStatusModalMutableLiveData.setValue(updatePackageStatusModel);

        }
        return updatePackageStatusModalMutableLiveData;
    }
}
