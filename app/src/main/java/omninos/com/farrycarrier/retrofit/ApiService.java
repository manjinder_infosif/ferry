package omninos.com.farrycarrier.retrofit;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import omninos.com.farrycarrier.model.AirportListModel;
import omninos.com.farrycarrier.model.CarrierInfoList;
import omninos.com.farrycarrier.model.CheckEmailModel;
import omninos.com.farrycarrier.model.CheckLoginModel;
import omninos.com.farrycarrier.model.CheckStatusModel;
import omninos.com.farrycarrier.model.ConversationModel;
import omninos.com.farrycarrier.model.ForgotPasswordModel;
import omninos.com.farrycarrier.model.GenerateTokenModel;
import omninos.com.farrycarrier.model.InboxModel;
import omninos.com.farrycarrier.model.MatchPaymentPinModel;
import omninos.com.farrycarrier.model.PayPalPaymentModel;
import omninos.com.farrycarrier.model.RegisterUserModel;
import omninos.com.farrycarrier.model.SendOTPModel;
import omninos.com.farrycarrier.model.SocialLoginModel;
import omninos.com.farrycarrier.model.UpdateProfileModel;
import omninos.com.farrycarrier.model.UserProfileModel;
import omninos.com.farrycarrier.model.WalletBalanceModel;
import omninos.com.farrycarrier.model.WalletRechargeModel;
import omninos.com.farrycarrier.model.carrier.CarrierCurrentAcceptedPackagesModel;
import omninos.com.farrycarrier.model.carrier.CarrierInformationModel;
import omninos.com.farrycarrier.model.carrier.CarrierPastPackageListModel;
import omninos.com.farrycarrier.model.carrier.PackageRequestModel;
import omninos.com.farrycarrier.model.carrier.PendingRequestListModel;
import omninos.com.farrycarrier.model.carrier.UpdatePackageStatusModel;
import omninos.com.farrycarrier.model.sender.NearbyCarrierListModel;
import omninos.com.farrycarrier.model.sender.RecieverInformationModel;
import omninos.com.farrycarrier.model.sender.SendMessageModel;
import omninos.com.farrycarrier.model.sender.SenderCurrentPackageListModel;
import omninos.com.farrycarrier.model.sender.SenderPastPackageListModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiService {

    //user check Email Api
    @FormUrlEncoded
    @POST("checkEmail")
    Call<CheckEmailModel> checkemail(@Field("email") String email);

    //mobile and otp Api
    @FormUrlEncoded
    @POST("checkPhoneNumberAndSendOtp")
    Call<SendOTPModel> checkphonenumber(@Field("phone") String phonenumber);

    //user login Api
    @FormUrlEncoded
    @POST("userLogin")
    Call<CheckLoginModel> checklogin(@Field("email") String email,
                                     @Field("password") String password,
                                     @Field("reg_id") String reg_id,
                                     @Field("device_type") String device_type,
                                     @Field("latitude") String latitude,
                                     @Field("longitude") String longitude);

    //user registeration api
    @FormUrlEncoded
    @POST("userRegister")
    Call<RegisterUserModel> registerUser(@Field("name") String name,
                                         @Field("email") String email,
                                         @Field("phone") String phone,
                                         @Field("password") String password,
                                         @Field("device_type") String device_type,
                                         @Field("reg_id") String reg_id,
                                         @Field("latitude") String latitude,
                                         @Field("longitude") String longitude,
                                         @Field("lastName") String lastName);

    //social logins
    @FormUrlEncoded
    @POST("UserSocialLogin")
    Call<SocialLoginModel> socialLogin(@Field("social_id") String social_id,
                                       @Field("name") String name,
                                       @Field("lastName") String lastName,
                                       @Field("email") String email,
                                       @Field("userImage") String userImage,
                                       @Field("device_type") String device_type,
                                       @Field("reg_id") String reg_id,
                                       @Field("login_type") String login_type);

    // User Type Api(Sender/Carrier)
    @FormUrlEncoded
    @POST("userType")
    Call<CheckStatusModel> checkUserType(@Field("senderCareerStatus") String Status,
                                         @Field("userId") String userId,
                                         @Field("reg_id") String reg_id);

    //Airport Codes List Api
    @FormUrlEncoded
    @POST("airportList")
    Call<AirportListModel> airportCodeList(@Field("countryName") String countryName,
                                           @Field("search") String search);

    //Send a Package Api
    @FormUrlEncoded
    @POST("senderInformation")
    Call<RecieverInformationModel> senderInfo(@Field("userId") String userId,
                                              @Field("packageType") String packageType,
                                              @Field("approximateSize") String approximateSize,
                                              @Field("country") String country,
                                              @Field("localAirportCode") String localAirportCode,
                                              @Field("destinationAirportCode") String destinationAirportCode,
                                              @Field("deliveryDate") String deliveryDate,
                                              @Field("suggestion") String suggestion);

    //Carry a Package Api
    @FormUrlEncoded
    @POST("careerInformation")
    Call<CarrierInformationModel> carrierInfo(@Field("userId") String userId,
                                              @Field("localAirportCode") String localAirportCode,
                                              @Field("destinationAirportCode") String destinationAirportCode,
                                              @Field("travelDate") String travelDate,
                                              @Field("deliveryStatus") String deliveryStatus,
                                              @Field("travelEndDate") String travelEndDate,
                                              @Field("travelTime") String travelTime,
                                              @Field("travelEndTime") String travelEndTime);


    //nearby courier List
    @FormUrlEncoded
    @POST("getRecieverInformation")
    Call<RecieverInformationModel> recieverInfo(@Field("senderId") String senderId);


    //Package Accept/Reject Api
    @FormUrlEncoded
    @POST("providerServiceRejectAccpet")
    Call<PackageRequestModel> packagerequest(@Field("type") String type,
                                             @Field("bookingServiceId") String bookingServiceId);

    //nearby Carrier booked
    @FormUrlEncoded
    @POST("userBookingServices")
    Call<NearbyCarrierListModel> nearbycarrierlist(@Field("senderInformationId") String senderInformationId,
                                                   @Field("carrierInformationId") String carrierInformationId,
                                                   @Field("senderId") String senderId,
                                                   @Field("carrierId") String carrierId,
                                                   @Field("type") String type);

    //Pending Request Api
    @FormUrlEncoded
    @POST("getNotificationList")
    Call<PendingRequestListModel> pendingrequestlist(@Field("carrierId") String carrierId);

    //Accepted Package List
    @FormUrlEncoded
    @POST("getCarrierAcceptList")
    Call<CarrierCurrentAcceptedPackagesModel> carriercurrentpackagelist(@Field("carrierId") String carrierId);

    //Set Package StatusApi(Pickup/Delivered)
    @FormUrlEncoded
    @POST("updateServiceStatus")
    Call<UpdatePackageStatusModel> updateservicestatus(@Field("bookingServiceId") String bookingServiceId,
                                                       @Field("serviceStatus") String serviceStatus,
                                                       @Field("type") String type);

    @FormUrlEncoded
    @POST("getSenderServicesList")
    Call<SenderCurrentPackageListModel> sendercurrentpackagelist(@Field("senderId") String senderId);

    //Get Profile Api
    @FormUrlEncoded
    @POST("getProfile")
    Call<UserProfileModel> userprofile(@Field("userId") String userId);

    //Edit Profile Api
    @Multipart
    @POST("updateProfile")
    Call<UpdateProfileModel> updateprofile(@Part("userId") RequestBody userIdbody,
                                           @Part("phone") RequestBody phonebody,
                                           @Part("address") RequestBody addressbody,
                                           @Part("workExperience") RequestBody workExperiencebody,
                                           @Part("name") RequestBody namebody,
                                           @Part("lastName") RequestBody lastName,
                                           @Part MultipartBody.Part userImage);

    //Chat Send MessageApi
    @FormUrlEncoded
    @POST("sendMessage")
    Call<SendMessageModel> sendmessage(@Field("sender_id") String sender_id,
                                       @Field("reciver_id") String reciver_id,
                                       @Field("message") String message);

    //Chat Conversation api
    @FormUrlEncoded
    @POST("conversationMessage")
    Call<ConversationModel> userconversation(@Field("sender_id") String sender_id,
                                             @Field("reciver_id") String reciver_id);

    @FormUrlEncoded
    @POST("inboxMessage")
    Call<InboxModel> userinbox(@Field("sender_id") String sender_id);

    //Forgot Password Api
    @FormUrlEncoded
    @POST("forgotPassword")
    Call<ForgotPasswordModel> forgotpassword(@Field("email") String email);

    //Unique Pin number Api
    @FormUrlEncoded
    @POST("matchPinNumber")
    Call<MatchPaymentPinModel> matchpaymentpin(@Field("bookingServiceId") String bookingServiceId,
                                               @Field("pinNumber") String pinNumber);

    @GET("genrateToken")
    Call<GenerateTokenModel> generatetoken();


    //PayPal Payment Api
    @FormUrlEncoded
    @POST("userPaymentAcceptance1")
    Call<PayPalPaymentModel> paypalpayments(@Field("bookingServiceId") String bookingServiceId,
                                            @Field("amount") String amount,
                                            @Field("nonce") String nonce,
                                            @Field("senderId") String senderId,
                                            @Field("carrierId") String carrierId);

    //Sender past Packages
    @FormUrlEncoded
    @POST("getCarrierDeliveredList")
    Call<SenderPastPackageListModel> pastpackagelist(@Field("senderId") String senderId);

    //Carrier past packages
    @FormUrlEncoded
    @POST("getCarrierDeliveredList1")
    Call<CarrierPastPackageListModel> carrierpastpackagelist(@Field("careerId") String careerId);


    //wallet recharge
    @FormUrlEncoded
    @POST("rechargeWallet")
    Call<WalletRechargeModel> walletrecharge(@Field("amount") String amount,
                                             @Field("userId") String userId,
                                             @Field("nonce") String nonce);

    //wallet balance api
    @FormUrlEncoded
    @POST("getUserWalletBalance")
    Call<WalletBalanceModel> walletbalance(@Field("userId") String userId);

    //payment acceptance api
    @FormUrlEncoded
    @POST("userPaymentAcceptance1")
    Call<Map> paymentacceptance(@Field("senderId") String senderId,
                                @Field("bookingServiceId") String bookingServiceId,
                                @Field("amount") String amount,
                                @Field("carrierId") String carrierId);

    @FormUrlEncoded
    @POST("careerBankDetails")
    Call<Map> addDetail(@Field("userId") String userId,
                        @Field("accountNumber") String accountNumber,
                        @Field("accountName") String accountName,
                        @Field("branchName") String branchName,
                        @Field("ifscCode") String ifscCode,
                        @Field("routingNumber") String routingNumber,
                        @Field("username") String username);

    @FormUrlEncoded
    @POST("requestMoney")
    Call<Map> requestMoney(@Field("userId") String userId);

    //http://infosif.in/ferryApplication/index.php/api/user/senderCancelPackage
    @FormUrlEncoded
    @POST("senderCancelPackage")
    Call<Map> cancelPackage(@Field("jobId") String jobId);

    //http://infosif.in/ferryApplication/index.php/api/user/carrierInformationList
    @FormUrlEncoded
    @POST("carrierInformationList")
    Call<CarrierInfoList> getCarrierList(@Field("userId") String userId);
}
