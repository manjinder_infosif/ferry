package omninos.com.farrycarrier.model.sender;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RecieverInformationModel {


    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("details")
    @Expose
    private List<Detail> details = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public RecieverInformationModel withSuccess(String success) {
        this.success = success;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public RecieverInformationModel withMessage(String message) {
        this.message = message;
        return this;
    }

    public List<Detail> getDetails() {
        return details;
    }

    public void setDetails(List<Detail> details) {
        this.details = details;
    }

    public RecieverInformationModel withDetails(List<Detail> details) {
        this.details = details;
        return this;
    }
    public class Detail {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("userId")
        @Expose
        private String userId;
        @SerializedName("localAirportCode")
        @Expose
        private String localAirportCode;
        @SerializedName("destinationAirportCode")
        @Expose
        private String destinationAirportCode;
        @SerializedName("travelDate")
        @Expose
        private String travelDate;
        @SerializedName("travelEndDate")
        @Expose
        private String travelEndDate;
        @SerializedName("travelTime")
        @Expose
        private String travelTime;
        @SerializedName("travelEndTime")
        @Expose
        private String travelEndTime;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("deliveryStatus")
        @Expose
        private String deliveryStatus;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("userImage")
        @Expose
        private String userImage;
        @SerializedName("senderInformationId")
        @Expose
        private String senderInformationId;

        public String getSenderInformationId() {
            return senderInformationId;
        }

        public void setSenderInformationId(String senderInformationId) {
            this.senderInformationId = senderInformationId;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getLocalAirportCode() {
            return localAirportCode;
        }

        public void setLocalAirportCode(String localAirportCode) {
            this.localAirportCode = localAirportCode;
        }

        public String getDestinationAirportCode() {
            return destinationAirportCode;
        }

        public void setDestinationAirportCode(String destinationAirportCode) {
            this.destinationAirportCode = destinationAirportCode;
        }

        public String getTravelDate() {
            return travelDate;
        }

        public void setTravelDate(String travelDate) {
            this.travelDate = travelDate;
        }

        public String getTravelEndDate() {
            return travelEndDate;
        }

        public void setTravelEndDate(String travelEndDate) {
            this.travelEndDate = travelEndDate;
        }

        public String getTravelTime() {
            return travelTime;
        }

        public void setTravelTime(String travelTime) {
            this.travelTime = travelTime;
        }

        public String getTravelEndTime() {
            return travelEndTime;
        }

        public void setTravelEndTime(String travelEndTime) {
            this.travelEndTime = travelEndTime;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getDeliveryStatus() {
            return deliveryStatus;
        }

        public void setDeliveryStatus(String deliveryStatus) {
            this.deliveryStatus = deliveryStatus;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getUserImage() {
            return userImage;
        }

        public void setUserImage(String userImage) {
            this.userImage = userImage;
        }

    }
}







