package omninos.com.farrycarrier.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MatchPaymentPinModel {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("details")
    @Expose
    private Details details;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Details getDetails() {
        return details;
    }

    public void setDetails(Details details) {
        this.details = details;
    }

    public class Details {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("recieverStatus")
        @Expose
        private String recieverStatus;
        @SerializedName("serviceStatus")
        @Expose
        private String serviceStatus;
        @SerializedName("senderInformationId")
        @Expose
        private String senderInformationId;
        @SerializedName("carrierInformationId")
        @Expose
        private String carrierInformationId;
        @SerializedName("senderId")
        @Expose
        private String senderId;
        @SerializedName("carrierId")
        @Expose
        private String carrierId;
        @SerializedName("jobId")
        @Expose
        private String jobId;
        @SerializedName("uniquePinNumber")
        @Expose
        private String uniquePinNumber;
        @SerializedName("matchPinStatus")
        @Expose
        private String matchPinStatus;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getRecieverStatus() {
            return recieverStatus;
        }

        public void setRecieverStatus(String recieverStatus) {
            this.recieverStatus = recieverStatus;
        }

        public String getServiceStatus() {
            return serviceStatus;
        }

        public void setServiceStatus(String serviceStatus) {
            this.serviceStatus = serviceStatus;
        }

        public String getSenderInformationId() {
            return senderInformationId;
        }

        public void setSenderInformationId(String senderInformationId) {
            this.senderInformationId = senderInformationId;
        }

        public String getCarrierInformationId() {
            return carrierInformationId;
        }

        public void setCarrierInformationId(String carrierInformationId) {
            this.carrierInformationId = carrierInformationId;
        }

        public String getSenderId() {
            return senderId;
        }

        public void setSenderId(String senderId) {
            this.senderId = senderId;
        }

        public String getCarrierId() {
            return carrierId;
        }

        public void setCarrierId(String carrierId) {
            this.carrierId = carrierId;
        }

        public String getJobId() {
            return jobId;
        }

        public void setJobId(String jobId) {
            this.jobId = jobId;
        }

        public String getUniquePinNumber() {
            return uniquePinNumber;
        }

        public void setUniquePinNumber(String uniquePinNumber) {
            this.uniquePinNumber = uniquePinNumber;
        }

        public String getMatchPinStatus() {
            return matchPinStatus;
        }

        public void setMatchPinStatus(String matchPinStatus) {
            this.matchPinStatus = matchPinStatus;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

    }
}
