package omninos.com.farrycarrier.model.sender;


public class RequestLIstModel {

    private String CourierNumber;
    private String CourierDetails;
    private String CourierWeight;
    private String CourierSourceCountry;
    private String CorurierDestinationCountry;
    private String CourierDate;
    private String CourierStatus;


    public RequestLIstModel() {
    }

    public RequestLIstModel(String CourierNumber, String CourierDetails, String CourierWeight, String CourierSourceCountry, String CorurierDestinationCountry, String CourierDate, String CourierStatus) {
        this.CourierNumber = CourierNumber;
        this.CourierDetails = CourierDetails;
        this.CourierWeight = CourierWeight;
        this.CourierSourceCountry = CourierSourceCountry;
        this.CorurierDestinationCountry = CorurierDestinationCountry;
        this.CourierDate = CourierDate;
        this.CourierStatus = CourierStatus;
    }

    public String getCourierNumber() {
        return CourierNumber;
    }

    public void setCourierNumber(String courierNumber) {
        CourierNumber = courierNumber;
    }

    public String getCourierDetails() {
        return CourierDetails;
    }

    public void setCourierDetails(String courierDetails) {
        CourierDetails = courierDetails;
    }

    public String getCourierWeight() {
        return CourierWeight;
    }

    public void setCourierWeight(String courierWeight) {
        CourierWeight = courierWeight;
    }

    public String getCourierSourceCountry() {
        return CourierSourceCountry;
    }

    public void setCourierSourceCountry(String courierSourceCountry) {
        CourierSourceCountry = courierSourceCountry;
    }

    public String getCorurierDestinationCountry() {
        return CorurierDestinationCountry;
    }

    public void setCorurierDestinationCountry(String corurierDestinationCountry) {
        CorurierDestinationCountry = corurierDestinationCountry;
    }

    public String getCourierDate() {
        return CourierDate;
    }

    public void setCourierDate(String courierDate) {
        CourierDate = courierDate;
    }

    public String getCourierStatus() {
        return CourierStatus;
    }

    public void setCourierStatus(String courierStatus) {
        CourierStatus = courierStatus;
    }


}
