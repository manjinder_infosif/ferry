package omninos.com.farrycarrier.model.carrier;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class PendingRequestListModel {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("details")
    @Expose
    private List<Detail> details = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Detail> getDetails() {
        return details;
    }

    public void setDetails(List<Detail> details) {
        this.details = details;
    }

    public class Detail {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("recieverStatus")
        @Expose
        private String recieverStatus;
        @SerializedName("senderInformationId")
        @Expose
        private String senderInformationId;
        @SerializedName("carrierInformationId")
        @Expose
        private String carrierInformationId;
        @SerializedName("senderId")
        @Expose
        private String senderId;
        @SerializedName("carrierId")
        @Expose
        private String carrierId;
        @SerializedName("jobId")
        @Expose
        private String jobId;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;
        @SerializedName("packageType")
        @Expose
        private String packageType;
        @SerializedName("approximateSize")
        @Expose
        private String approximateSize;
        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("localAirportCode")
        @Expose
        private String localAirportCode;
        @SerializedName("destinationAirportCode")
        @Expose
        private String destinationAirportCode;
        @SerializedName("deliveryDate")
        @Expose
        private String deliveryDate;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("phone")
        @Expose
        private String phone;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getRecieverStatus() {
            return recieverStatus;
        }

        public void setRecieverStatus(String recieverStatus) {
            this.recieverStatus = recieverStatus;
        }

        public String getSenderInformationId() {
            return senderInformationId;
        }

        public void setSenderInformationId(String senderInformationId) {
            this.senderInformationId = senderInformationId;
        }

        public String getCarrierInformationId() {
            return carrierInformationId;
        }

        public void setCarrierInformationId(String carrierInformationId) {
            this.carrierInformationId = carrierInformationId;
        }

        public String getSenderId() {
            return senderId;
        }

        public void setSenderId(String senderId) {
            this.senderId = senderId;
        }

        public String getCarrierId() {
            return carrierId;
        }

        public void setCarrierId(String carrierId) {
            this.carrierId = carrierId;
        }

        public String getJobId() {
            return jobId;
        }

        public void setJobId(String jobId) {
            this.jobId = jobId;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

        public String getPackageType() {
            return packageType;
        }

        public void setPackageType(String packageType) {
            this.packageType = packageType;
        }

        public String getApproximateSize() {
            return approximateSize;
        }

        public void setApproximateSize(String approximateSize) {
            this.approximateSize = approximateSize;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getLocalAirportCode() {
            return localAirportCode;
        }

        public void setLocalAirportCode(String localAirportCode) {
            this.localAirportCode = localAirportCode;
        }

        public String getDestinationAirportCode() {
            return destinationAirportCode;
        }

        public void setDestinationAirportCode(String destinationAirportCode) {
            this.destinationAirportCode = destinationAirportCode;
        }

        public String getDeliveryDate() {
            return deliveryDate;
        }

        public void setDeliveryDate(String deliveryDate) {
            this.deliveryDate = deliveryDate;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

    }
}