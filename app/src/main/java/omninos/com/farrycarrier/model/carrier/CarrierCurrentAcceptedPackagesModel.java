package omninos.com.farrycarrier.model.carrier;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CarrierCurrentAcceptedPackagesModel implements Serializable {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("details")
    @Expose
    private List<Detail> details = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public CarrierCurrentAcceptedPackagesModel withSuccess(String success) {
        this.success = success;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CarrierCurrentAcceptedPackagesModel withMessage(String message) {
        this.message = message;
        return this;
    }

    public List<Detail> getDetails() {
        return details;
    }

    public void setDetails(List<Detail> details) {
        this.details = details;
    }

    public CarrierCurrentAcceptedPackagesModel withDetails(List<Detail> details) {
        this.details = details;
        return this;
    }

    public class Detail implements Serializable {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("recieverStatus")
        @Expose
        private String recieverStatus;
        @SerializedName("serviceStatus")
        @Expose
        private String serviceStatus;
        @SerializedName("senderInformationId")
        @Expose
        private String senderInformationId;
        @SerializedName("carrierInformationId")
        @Expose
        private String carrierInformationId;
        @SerializedName("senderId")
        @Expose
        private String senderId;
        @SerializedName("carrierId")
        @Expose
        private String carrierId;
        @SerializedName("jobId")
        @Expose
        private String jobId;
        @SerializedName("uniquePinNumber")
        @Expose
        private String uniquePinNumber;
        @SerializedName("matchPinStatus")
        @Expose
        private String matchPinStatus;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;
        @SerializedName("senderPhone")
        @Expose
        private String senderPhone;
        @SerializedName("packageType")
        @Expose
        private String packageType;
        @SerializedName("approximateSize")
        @Expose
        private String approximateSize;
        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("localAirportCode")
        @Expose
        private String localAirportCode;
        @SerializedName("destinationAirportCode")
        @Expose
        private String destinationAirportCode;
        @SerializedName("deliveryDate")
        @Expose
        private String deliveryDate;
        @SerializedName("senderName")
        @Expose
        private String senderName;
        @SerializedName("senderImage")
        @Expose
        private String senderImage;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Detail withId(String id) {
            this.id = id;
            return this;
        }

        public String getRecieverStatus() {
            return recieverStatus;
        }

        public void setRecieverStatus(String recieverStatus) {
            this.recieverStatus = recieverStatus;
        }

        public Detail withRecieverStatus(String recieverStatus) {
            this.recieverStatus = recieverStatus;
            return this;
        }

        public String getServiceStatus() {
            return serviceStatus;
        }

        public void setServiceStatus(String serviceStatus) {
            this.serviceStatus = serviceStatus;
        }

        public Detail withServiceStatus(String serviceStatus) {
            this.serviceStatus = serviceStatus;
            return this;
        }

        public String getSenderInformationId() {
            return senderInformationId;
        }

        public void setSenderInformationId(String senderInformationId) {
            this.senderInformationId = senderInformationId;
        }

        public Detail withSenderInformationId(String senderInformationId) {
            this.senderInformationId = senderInformationId;
            return this;
        }

        public String getCarrierInformationId() {
            return carrierInformationId;
        }

        public void setCarrierInformationId(String carrierInformationId) {
            this.carrierInformationId = carrierInformationId;
        }

        public Detail withCarrierInformationId(String carrierInformationId) {
            this.carrierInformationId = carrierInformationId;
            return this;
        }

        public String getSenderId() {
            return senderId;
        }

        public void setSenderId(String senderId) {
            this.senderId = senderId;
        }

        public Detail withSenderId(String senderId) {
            this.senderId = senderId;
            return this;
        }

        public String getCarrierId() {
            return carrierId;
        }

        public void setCarrierId(String carrierId) {
            this.carrierId = carrierId;
        }

        public Detail withCarrierId(String carrierId) {
            this.carrierId = carrierId;
            return this;
        }

        public String getJobId() {
            return jobId;
        }

        public void setJobId(String jobId) {
            this.jobId = jobId;
        }

        public Detail withJobId(String jobId) {
            this.jobId = jobId;
            return this;
        }

        public String getUniquePinNumber() {
            return uniquePinNumber;
        }

        public void setUniquePinNumber(String uniquePinNumber) {
            this.uniquePinNumber = uniquePinNumber;
        }

        public Detail withUniquePinNumber(String uniquePinNumber) {
            this.uniquePinNumber = uniquePinNumber;
            return this;
        }

        public String getMatchPinStatus() {
            return matchPinStatus;
        }

        public void setMatchPinStatus(String matchPinStatus) {
            this.matchPinStatus = matchPinStatus;
        }

        public Detail withMatchPinStatus(String matchPinStatus) {
            this.matchPinStatus = matchPinStatus;
            return this;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public Detail withCreated(String created) {
            this.created = created;
            return this;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

        public Detail withUpdated(String updated) {
            this.updated = updated;
            return this;
        }

        public String getSenderPhone() {
            return senderPhone;
        }

        public void setSenderPhone(String senderPhone) {
            this.senderPhone = senderPhone;
        }

        public Detail withSenderPhone(String senderPhone) {
            this.senderPhone = senderPhone;
            return this;
        }

        public String getPackageType() {
            return packageType;
        }

        public void setPackageType(String packageType) {
            this.packageType = packageType;
        }

        public Detail withPackageType(String packageType) {
            this.packageType = packageType;
            return this;
        }

        public String getApproximateSize() {
            return approximateSize;
        }

        public void setApproximateSize(String approximateSize) {
            this.approximateSize = approximateSize;
        }

        public Detail withApproximateSize(String approximateSize) {
            this.approximateSize = approximateSize;
            return this;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public Detail withCountry(String country) {
            this.country = country;
            return this;
        }

        public String getLocalAirportCode() {
            return localAirportCode;
        }

        public void setLocalAirportCode(String localAirportCode) {
            this.localAirportCode = localAirportCode;
        }

        public Detail withLocalAirportCode(String localAirportCode) {
            this.localAirportCode = localAirportCode;
            return this;
        }

        public String getDestinationAirportCode() {
            return destinationAirportCode;
        }

        public void setDestinationAirportCode(String destinationAirportCode) {
            this.destinationAirportCode = destinationAirportCode;
        }

        public Detail withDestinationAirportCode(String destinationAirportCode) {
            this.destinationAirportCode = destinationAirportCode;
            return this;
        }

        public String getDeliveryDate() {
            return deliveryDate;
        }

        public void setDeliveryDate(String deliveryDate) {
            this.deliveryDate = deliveryDate;
        }

        public Detail withDeliveryDate(String deliveryDate) {
            this.deliveryDate = deliveryDate;
            return this;
        }

        public String getSenderName() {
            return senderName;
        }

        public void setSenderName(String senderName) {
            this.senderName = senderName;
        }

        public Detail withSenderName(String senderName) {
            this.senderName = senderName;
            return this;
        }

        public String getSenderImage() {
            return senderImage;
        }

        public void setSenderImage(String senderImage) {
            this.senderImage = senderImage;
        }

        public Detail withSenderImage(String senderImage) {
            this.senderImage = senderImage;
            return this;
        }

    }
}


