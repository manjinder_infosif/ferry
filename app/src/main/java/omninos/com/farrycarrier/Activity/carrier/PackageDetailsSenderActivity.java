package omninos.com.farrycarrier.activity.carrier;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.activity.ChatActivity;
import omninos.com.farrycarrier.model.MatchPaymentPinModel;
import omninos.com.farrycarrier.model.carrier.CarrierCurrentAcceptedPackagesModel;
import omninos.com.farrycarrier.model.carrier.UpdatePackageStatusModel;
import omninos.com.farrycarrier.util.App;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.viewmodels.MatchPaymentPinVM;
import omninos.com.farrycarrier.viewmodels.PayPalPaymentVM;
import omninos.com.farrycarrier.viewmodels.PaymentAcceptanceVM;
import omninos.com.farrycarrier.viewmodels.UpdatePackageStatusVM;

public class PackageDetailsSenderActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView BackButton, Call, Chat;
    private Button SubmitButton, payPalButton;
    private Spinner spinner;
    private EditText firstNumberVerify, secondNumberVerify, thirdNumberVerify, fourthNumberVerify, fifthNumberVerify;
    private AlertDialog dialog;
    private String status, status1, bookingID, senderProfile, senderName, CourierNumber, CourierSize, CourierSourceCountry, CourierDestinationCountry, senderId, name;
    private TextView tvSenderName, textViewCourierNumber, textViewCourierWeight, textViewSourceCountry, textViewDestinationCountry;
    private UpdatePackageStatusVM updatePackageStatusVM;
    private List<UpdatePackageStatusModel.Details> updatePackageStatusModalList = new ArrayList<>();
    private CarrierCurrentAcceptedPackagesModel.Detail detail;
    private CircleImageView SenderProfilePic;
    String token;
    private int REQUEST_OUT = 1234;
    private PayPalPaymentVM payPalPaymentVM;
    private MatchPaymentPinVM matchPaymentPinVM;
    private PaymentAcceptanceVM paymentAcceptanceVM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package_details_sender);

        findIds();
        setUps();
        onClicks();
    }

    @SuppressLint("ResourceType")
    private void findIds() {
        BackButton = findViewById(R.id.back_btn);
        SubmitButton = findViewById(R.id.submit_btn);
        spinner = findViewById(R.id.spinner_status);
        Call = findViewById(R.id.call);
        Chat = findViewById(R.id.chat);

        textViewCourierWeight = findViewById(R.id.tv_courier_weight);
        textViewSourceCountry = findViewById(R.id.tv_source_country);
        textViewDestinationCountry = findViewById(R.id.tv_destination_country);
        textViewCourierNumber = findViewById(R.id.tv_courier_number);
        tvSenderName = findViewById(R.id.tv_sendername);
        SenderProfilePic = findViewById(R.id.senderProfile);


    }

    private void setUps() {
        //viewmodels
        updatePackageStatusVM = ViewModelProviders.of(this).get(UpdatePackageStatusVM.class);
        payPalPaymentVM = ViewModelProviders.of(this).get(PayPalPaymentVM.class);
        matchPaymentPinVM = ViewModelProviders.of(this).get(MatchPaymentPinVM.class);
        paymentAcceptanceVM = ViewModelProviders.of(this).get(PaymentAcceptanceVM.class);

        //serializable object
        detail = (CarrierCurrentAcceptedPackagesModel.Detail) getIntent().getSerializableExtra("details");
        senderName = detail.getSenderName();
        senderProfile = detail.getSenderImage();
        CourierSize = detail.getApproximateSize();
        CourierNumber = detail.getJobId();
        CourierSourceCountry = detail.getLocalAirportCode();
        CourierDestinationCountry = detail.getDestinationAirportCode();
        bookingID = detail.getId();
        status1 = detail.getServiceStatus();
        senderId = detail.getSenderId();
        name = detail.getSenderName();

        //set text to textviews
        textViewCourierWeight.setText(CourierSize);
        textViewSourceCountry.setText(CourierSourceCountry);
        textViewDestinationCountry.setText(CourierDestinationCountry);
        textViewCourierNumber.setText(CourierNumber);
        tvSenderName.setText(senderName);

        if (senderProfile.isEmpty())
            SenderProfilePic.setImageResource(R.drawable.ic_profile_placeholder);
        else
            Glide.with(PackageDetailsSenderActivity.this).load(senderProfile).into(SenderProfilePic);
    }

    private void onClicks() {
        BackButton.setOnClickListener(this);
        SubmitButton.setOnClickListener(this);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getSelectedItem().toString().equalsIgnoreCase("Select Status"))
                    status = "0";
                else if (parent.getSelectedItem().toString().equalsIgnoreCase("picked up"))
                    ShowpopUp();
                else
                    status = "2";
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        if (App.getAppPreference().GetString(ConstantData.Type).equalsIgnoreCase("Package_Delivered")) {
            spinner.setSelection(1);
        } else {

        }
        Call.setOnClickListener(this);
        Chat.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                finish();
                break;

            case R.id.submit_btn:
                if (status.equalsIgnoreCase("2")) {
                    showPinDialog();
//                    showPinDialog();
//                    startActivity(new Intent(PackageDetailsSenderActivity.this, TermActivity.class).putExtra("Status", "0"));
//                    App.getSinltonPojo().setStatus("0");
                } else
                    getUpdatePackageStatusApi();
                break;

            case R.id.call:
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + detail.getSenderPhone()));//change the number
                startActivity(callIntent);
                break;

            case R.id.chat:
                Intent chat = new Intent(PackageDetailsSenderActivity.this, ChatActivity.class);
                chat.putExtra("reciverId", senderId);
                chat.putExtra("name", name);
                startActivity(chat);
                break;
        }

    }

    private void ShowpopUp() {
        final Dialog dialog = new Dialog(PackageDetailsSenderActivity.this);
        dialog.setContentView(R.layout.disclaimer_layout);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button btn_done = dialog.findViewById(R.id.btn_done);
        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                status = "1";
            }
        });
        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (App.getSinltonPojo().getStatus() != null) {
            if (App.getSinltonPojo().getStatus().equalsIgnoreCase("0")) {
            }
        }
    }

    private void showPinDialog() {
        View customView = LayoutInflater.from(PackageDetailsSenderActivity.this).inflate(R.layout.popup_enter_pin, null);
        dialog = new AlertDialog.Builder(PackageDetailsSenderActivity.this).create();
        dialog.setView(customView);
        dialog.setCanceledOnTouchOutside(false);
        textWatchers(customView);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void textWatchers(View customView) {
        firstNumberVerify = customView.findViewById(R.id.firstNumberVerify);
        secondNumberVerify = customView.findViewById(R.id.secondNumberVerify);
        thirdNumberVerify = customView.findViewById(R.id.thirdNumberVerify);
        fourthNumberVerify = customView.findViewById(R.id.fourthNumberVerify);
        fifthNumberVerify = customView.findViewById(R.id.fifthNumberVerify);
        payPalButton = customView.findViewById(R.id.paypal_btn);


        firstNumberVerify.addTextChangedListener(new PackageDetailsSenderActivity.GenericTextWatcher(firstNumberVerify));
        secondNumberVerify.addTextChangedListener(new PackageDetailsSenderActivity.GenericTextWatcher(secondNumberVerify));
        thirdNumberVerify.addTextChangedListener(new PackageDetailsSenderActivity.GenericTextWatcher(thirdNumberVerify));
        fourthNumberVerify.addTextChangedListener(new PackageDetailsSenderActivity.GenericTextWatcher(fourthNumberVerify));
        fifthNumberVerify.addTextChangedListener(new PackageDetailsSenderActivity.GenericTextWatcher(fifthNumberVerify));


        payPalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String firstNum = firstNumberVerify.getText().toString();
                final String secondNum = secondNumberVerify.getText().toString();
                final String thirdNum = thirdNumberVerify.getText().toString();
                final String fourthNum = fourthNumberVerify.getText().toString();
                final String fifthNum = fifthNumberVerify.getText().toString();
                //paypal intergration
                if (TextUtils.isEmpty(firstNum) || TextUtils.isEmpty(secondNum) || TextUtils.isEmpty(thirdNum) || TextUtils.isEmpty(fourthNum) || TextUtils.isEmpty(fifthNum)) {
                    Toast.makeText(PackageDetailsSenderActivity.this, "Fill Mandatory Details.", Toast.LENGTH_LONG).show();
                } else {
                    dialog.dismiss();
                    String pin = firstNum + secondNum + thirdNum + fourthNum + fifthNum;
                    getMatchPaymentPinApi(pin);
                }
            }
        });
    }

    private void getMatchPaymentPinApi(String pin) {
        matchPaymentPinVM.matchpaymentpin(PackageDetailsSenderActivity.this, detail.getId(), pin).observe(this, new Observer<MatchPaymentPinModel>() {
            @Override
            public void onChanged(@Nullable MatchPaymentPinModel matchPaymentPinModel) {
                if (matchPaymentPinModel.getSuccess().equalsIgnoreCase("1")) {
//                    Toast.makeText(PackageDetailsSenderActivity.this, "Payment Received In Wallet.", Toast.LENGTH_SHORT).show();
                    getPaymentAcceptanceApi();
                } else {
                    Toast.makeText(PackageDetailsSenderActivity.this, matchPaymentPinModel.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void getPaymentAcceptanceApi() {
        paymentAcceptanceVM.paymentAcceptance(PackageDetailsSenderActivity.this, senderId, bookingID, "60",
                App.getAppPreference().GetString(ConstantData.USERID)).observe(this, new Observer<Map>() {
            @Override
            public void onChanged(@Nullable Map map) {
                if (map.get("success").toString().equalsIgnoreCase("1")) {
                    getUpdatePackageStatusApi();
//                    startActivity(new Intent(PackageDetailsSenderActivity.this, HomeCarrierActivity.class));
//                    finish();
//                    Toast.makeText(PackageDetailsSenderActivity.this, "Payment Received In Wallet!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(PackageDetailsSenderActivity.this, map.get("message").toString(), Toast.LENGTH_SHORT).show();
//                    Toast.makeText(PackageDetailsSenderActivity.this, "Pin verification Not Matched", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public class GenericTextWatcher implements TextWatcher {

        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            switch (view.getId()) {

                case R.id.firstNumberVerify:
                    if (text.length() == 1) {
                        firstNumberVerify.setBackground(getResources().getDrawable(R.drawable.verificationet_design));
                        secondNumberVerify.requestFocus();
                    } else if (text.length() == 0) {
                        firstNumberVerify.setBackground(getResources().getDrawable(R.drawable.socialbutton_design));
                    }

                    break;
                case R.id.secondNumberVerify:
                    if (text.length() == 1) {
                        secondNumberVerify.setBackground(getResources().getDrawable(R.drawable.verificationet_design));
                        thirdNumberVerify.requestFocus();
                    } else if (text.length() == 0) {
                        secondNumberVerify.setBackground(getResources().getDrawable(R.drawable.socialbutton_design));
                        firstNumberVerify.requestFocus();
                    }


                    break;
                case R.id.thirdNumberVerify:
                    if (text.length() == 1) {
                        thirdNumberVerify.setBackground(getResources().getDrawable(R.drawable.verificationet_design));
                        fourthNumberVerify.requestFocus();
                    } else if (text.length() == 0) {
                        secondNumberVerify.requestFocus();
                        thirdNumberVerify.setBackground(getResources().getDrawable(R.drawable.socialbutton_design));
                    }

                    break;
                case R.id.fourthNumberVerify:
                    if (text.length() == 1) {
                        fourthNumberVerify.setBackground(getResources().getDrawable(R.drawable.verificationet_design));
                        fifthNumberVerify.requestFocus();
                    } else if (text.length() == 0) {
                        thirdNumberVerify.requestFocus();
                        fourthNumberVerify.setBackground(getResources().getDrawable(R.drawable.socialbutton_design));
                    }
                    break;

                case R.id.fifthNumberVerify:
                    if (text.length() == 1) {
                        fifthNumberVerify.setBackground(getResources().getDrawable(R.drawable.verificationet_design));
                        SubmitButton.requestFocus();
                    } else if (text.length() == 0) {
                        fourthNumberVerify.requestFocus();
                        fifthNumberVerify.setBackground(getResources().getDrawable(R.drawable.socialbutton_design));
                    }
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }
    }

    private void getUpdatePackageStatusApi() {
        final String bookingServiceId = bookingID;
        updatePackageStatusVM.updatepackagestatus(PackageDetailsSenderActivity.this, bookingServiceId, status, "Package_Delivered").observe(this, new Observer<UpdatePackageStatusModel>() {
            @Override
            public void onChanged(@Nullable UpdatePackageStatusModel updatePackageStatusModel) {
                if (updatePackageStatusModel.getSuccess().equalsIgnoreCase("1")) {
                    App.getAppPreference().SaveString(ConstantData.Type, "Package_Delivered");
                    App.getSinltonPojo().setStatus(null);
                    Intent intent = new Intent(PackageDetailsSenderActivity.this, HomeCarrierActivity.class);
                    intent.putExtra("bookingid", bookingServiceId);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(PackageDetailsSenderActivity.this, "No Data Found!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


}
