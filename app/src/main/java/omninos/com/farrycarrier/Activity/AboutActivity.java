package omninos.com.farrycarrier.activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import omninos.com.farrycarrier.R;

public class AboutActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView BackButton;
    private WebView webWew;
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        findIDs();
        onClicks();
    }

    private void findIDs() {
        BackButton=findViewById(R.id.back_btn);
        webWew=findViewById(R.id.webWew);
    }

    private void onClicks() {
        BackButton.setOnClickListener(this);

        pd = new ProgressDialog(AboutActivity.this);
        pd.setMessage("Loading...");
        pd.show();
        WebSettings webSettings = webWew.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webWew.setWebViewClient(new MyWebViewClient());
//        http://www.omninos.in/laybay/
        webWew.loadUrl("http://infosif.in/ferryApplication/index.php/api/User/termsAndCondition");
//        webWew.loadUrl("https://www.instagram.com/");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back_btn:
                finish();
                break;
        }

    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);

            if (!pd.isShowing()) {
                pd.show();
            }

            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            System.out.println("on finish");
            if (pd.isShowing()) {
                pd.dismiss();
            }

        }
    }
}
