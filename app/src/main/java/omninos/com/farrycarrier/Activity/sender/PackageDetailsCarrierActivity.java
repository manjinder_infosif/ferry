package omninos.com.farrycarrier.activity.sender;

import android.app.AlertDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.bumptech.glide.Glide;

import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.activity.ChatActivity;
import omninos.com.farrycarrier.activity.MapActivity;
import omninos.com.farrycarrier.model.GenerateTokenModel;
import omninos.com.farrycarrier.model.MatchPaymentPinModel;
import omninos.com.farrycarrier.model.PayPalPaymentModel;
import omninos.com.farrycarrier.model.sender.SenderCurrentPackageListModel;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import omninos.com.farrycarrier.util.App;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.viewmodels.CancelPackageViewModel;
import omninos.com.farrycarrier.viewmodels.MatchPaymentPinVM;
import omninos.com.farrycarrier.viewmodels.PayPalPaymentVM;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PackageDetailsCarrierActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView BackButton, Call, Chat, Track;
    private String carrierName, carrierImage, courierweight, courierSourcecountry, courierDestinationCountry, courierNumber, carrierID, name, packageAmt;
    private TextView tvCourierNumber, tvCourierWeight, tvCouriersrcCountry, tvCourierDestcountry, tvCarrierName, tvPackageAmount, tvPackageStatus;
    private SenderCurrentPackageListModel.Detail detail;
    private CircleImageView carrierProfile;
    private Button payButton, payPalButton;
    private EditText firstNumberVerify, secondNumberVerify, thirdNumberVerify, fourthNumberVerify, fifthNumberVerify;
    private AlertDialog dialog;
    String token;
    private int REQUEST_OUT = 1234;
    private PayPalPaymentVM payPalPaymentVM;
    private MatchPaymentPinVM matchPaymentPinVM;
    private CancelPackageViewModel cancelPackageViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package_details_carrier);
        payPalPaymentVM = ViewModelProviders.of(this).get(PayPalPaymentVM.class);
        matchPaymentPinVM = ViewModelProviders.of(this).get(MatchPaymentPinVM.class);
        cancelPackageViewModel = ViewModelProviders.of(this).get(CancelPackageViewModel.class);

        findIds();
        setUps();
    }


    private void findIds() {
        BackButton = findViewById(R.id.back_btn);
        Call = findViewById(R.id.call);
        Chat = findViewById(R.id.chat);
        Track = findViewById(R.id.track_package);
        tvCourierNumber = findViewById(R.id.tv_courierNumber);
        tvCourierWeight = findViewById(R.id.tv_courier_weight);
        tvCouriersrcCountry = findViewById(R.id.tv_src_country);
        tvCourierDestcountry = findViewById(R.id.tv_dest_country);
        tvCarrierName = findViewById(R.id.tv_carriername);
        carrierProfile = findViewById(R.id.profilepic_carrier);
        tvPackageStatus = findViewById(R.id.tv_package_status);
        tvPackageAmount = findViewById(R.id.tv_package_amt);
        payButton = findViewById(R.id.pay_btn);
        Track.setVisibility(View.GONE);
    }


    private void setUps() {
        //carrier details
        detail = (SenderCurrentPackageListModel.Detail) getIntent().getSerializableExtra("details");
        courierNumber = detail.getJobId();
        carrierName = detail.getCareerName();
        carrierImage = detail.getCareerImage();
        courierSourcecountry = detail.getLocalAirportCode();
        courierDestinationCountry = detail.getDestinationAirportCode();
        courierweight = detail.getApproximateSize();
        carrierID = detail.getCarrierId();
        name = detail.getCareerName();
        packageAmt = detail.getAmount();

        //on click listeners
        BackButton.setOnClickListener(this);
        Call.setOnClickListener(this);
        Chat.setOnClickListener(this);
        Track.setOnClickListener(this);
        payButton.setOnClickListener(this);

        //set texts
        tvCourierNumber.setText(courierNumber);
        tvCourierWeight.setText(courierweight);
        tvCouriersrcCountry.setText(courierSourcecountry);
        tvCarrierName.setText(carrierName);
        tvCourierDestcountry.setText(courierDestinationCountry);
        tvPackageAmount.setText("$" + packageAmt);

        if (detail.getServiceStatus().equalsIgnoreCase("0")) {
            tvPackageStatus.setText("Pending");
            payButton.setVisibility(View.VISIBLE);
        } else if (detail.getServiceStatus().equalsIgnoreCase("1")) {
            tvPackageStatus.setText("Picked Up");
            payButton.setVisibility(View.GONE);
        } else {
            tvPackageStatus.setText("Delivered");
            payButton.setVisibility(View.GONE);
        }
        //set Image
        if (carrierImage.isEmpty())
            carrierProfile.setImageResource(R.drawable.ic_profile_placeholder);
        else
            Glide.with(PackageDetailsCarrierActivity.this).load(carrierImage).into(carrierProfile);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                finish();
                break;

            case R.id.call:
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + detail.getCarrierPhone()));//change the number
                startActivity(callIntent);
                break;

            case R.id.chat:
                Intent chat = new Intent(PackageDetailsCarrierActivity.this, ChatActivity.class);
                chat.putExtra("reciverId", carrierID);
                chat.putExtra("name", name);
                startActivity(chat);
                break;

            case R.id.track_package:
                Intent track = new Intent(PackageDetailsCarrierActivity.this, MapActivity.class);
                startActivity(track);
                break;

            case R.id.pay_btn:
                CancleBox();
                break;
        }
    }

    private void CancleBox() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(PackageDetailsCarrierActivity.this);
        builder1.setMessage("Are you arrived?");
        builder1.setCancelable(true);

        builder1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                CancelPackage();
                dialog.cancel();
            }
        });

        builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

    private void CancelPackage() {
        cancelPackageViewModel.PackageCancel(PackageDetailsCarrierActivity.this, detail.getJobId()).observe(PackageDetailsCarrierActivity.this, new Observer<Map>() {
            @Override
            public void onChanged(@Nullable Map map) {
                if (map.get("success").toString().equalsIgnoreCase("1")) {
                    Toast.makeText(PackageDetailsCarrierActivity.this, map.get("message").toString(), Toast.LENGTH_SHORT).show();
                    onBackPressed();
                } else {
                    Toast.makeText(PackageDetailsCarrierActivity.this, map.get("message").toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showPinDialog() {
        View customView = LayoutInflater.from(PackageDetailsCarrierActivity.this).inflate(R.layout.popup_enter_pin, null);
        dialog = new AlertDialog.Builder(PackageDetailsCarrierActivity.this).create();
        dialog.setView(customView);
        dialog.setCanceledOnTouchOutside(false);

        textWatchers(customView);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void textWatchers(View customView) {
        firstNumberVerify = customView.findViewById(R.id.firstNumberVerify);
        secondNumberVerify = customView.findViewById(R.id.secondNumberVerify);
        thirdNumberVerify = customView.findViewById(R.id.thirdNumberVerify);
        fourthNumberVerify = customView.findViewById(R.id.fourthNumberVerify);
        fifthNumberVerify = customView.findViewById(R.id.fifthNumberVerify);
        payPalButton = customView.findViewById(R.id.paypal_btn);


        firstNumberVerify.addTextChangedListener(new PackageDetailsCarrierActivity.GenericTextWatcher(firstNumberVerify));
        secondNumberVerify.addTextChangedListener(new PackageDetailsCarrierActivity.GenericTextWatcher(secondNumberVerify));
        thirdNumberVerify.addTextChangedListener(new PackageDetailsCarrierActivity.GenericTextWatcher(thirdNumberVerify));
        fourthNumberVerify.addTextChangedListener(new PackageDetailsCarrierActivity.GenericTextWatcher(fourthNumberVerify));
        fifthNumberVerify.addTextChangedListener(new PackageDetailsCarrierActivity.GenericTextWatcher(fifthNumberVerify));


        payPalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String firstNum = firstNumberVerify.getText().toString();
                final String secondNum = secondNumberVerify.getText().toString();
                final String thirdNum = thirdNumberVerify.getText().toString();
                final String fourthNum = fourthNumberVerify.getText().toString();
                final String fifthNum = fifthNumberVerify.getText().toString();
                //paypal intergration
                if (TextUtils.isEmpty(firstNum) || TextUtils.isEmpty(secondNum) || TextUtils.isEmpty(thirdNum) || TextUtils.isEmpty(fourthNum) || TextUtils.isEmpty(fifthNum)) {
                    Toast.makeText(PackageDetailsCarrierActivity.this, "Fill Mandatory Details.", Toast.LENGTH_LONG).show();
                } else {
                    dialog.dismiss();
                    String pin = firstNum + secondNum + thirdNum + fourthNum + fifthNum;
                    getMatchPaymentPinApi(pin);
                }
            }
        });
    }

    private void getMatchPaymentPinApi(String pin) {

        matchPaymentPinVM.matchpaymentpin(PackageDetailsCarrierActivity.this, detail.getId(), pin).observe(this, new Observer<MatchPaymentPinModel>() {
            @Override
            public void onChanged(@Nullable MatchPaymentPinModel matchPaymentPinModel) {
                if (matchPaymentPinModel.getSuccess().equalsIgnoreCase("1")) {
                    Toast.makeText(PackageDetailsCarrierActivity.this, matchPaymentPinModel.getMessage(), Toast.LENGTH_SHORT).show();
                    PayPalPayment();
                } else {
                    Toast.makeText(PackageDetailsCarrierActivity.this, matchPaymentPinModel.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public class GenericTextWatcher implements TextWatcher {

        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            switch (view.getId()) {

                case R.id.firstNumberVerify:
                    if (text.length() == 1) {
                        firstNumberVerify.setBackground(getResources().getDrawable(R.drawable.verificationet_design));
                        secondNumberVerify.requestFocus();
                    } else if (text.length() == 0) {
                        firstNumberVerify.setBackground(getResources().getDrawable(R.drawable.socialbutton_design));
                    }

                    break;
                case R.id.secondNumberVerify:
                    if (text.length() == 1) {
                        secondNumberVerify.setBackground(getResources().getDrawable(R.drawable.verificationet_design));
                        thirdNumberVerify.requestFocus();
                    } else if (text.length() == 0) {
                        secondNumberVerify.setBackground(getResources().getDrawable(R.drawable.socialbutton_design));
                        firstNumberVerify.requestFocus();
                    }


                    break;
                case R.id.thirdNumberVerify:
                    if (text.length() == 1) {
                        thirdNumberVerify.setBackground(getResources().getDrawable(R.drawable.verificationet_design));
                        fourthNumberVerify.requestFocus();
                    } else if (text.length() == 0) {
                        secondNumberVerify.requestFocus();
                        thirdNumberVerify.setBackground(getResources().getDrawable(R.drawable.socialbutton_design));
                    }

                    break;
                case R.id.fourthNumberVerify:
                    if (text.length() == 1) {
                        fourthNumberVerify.setBackground(getResources().getDrawable(R.drawable.verificationet_design));
                        fifthNumberVerify.requestFocus();
                        payButton.requestFocus();
                    } else if (text.length() == 0) {
                        thirdNumberVerify.requestFocus();
                        fourthNumberVerify.setBackground(getResources().getDrawable(R.drawable.socialbutton_design));

                    }
                    break;

                case R.id.fifthNumberVerify:
                    if (text.length() == 1) {
                        fifthNumberVerify.setBackground(getResources().getDrawable(R.drawable.verificationet_design));
                        payButton.requestFocus();
                    } else if (text.length() == 0) {
                        fourthNumberVerify.requestFocus();
                        fifthNumberVerify.setBackground(getResources().getDrawable(R.drawable.socialbutton_design));

                    }
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }
    }

    private void PayPalPayment() {
        ApiService api = ApiClient.getApiClient().create(ApiService.class);

        CommonUtils.showProgress(PackageDetailsCarrierActivity.this, "");
        api.generatetoken().enqueue(new Callback<GenerateTokenModel>() {
            @Override
            public void onResponse(retrofit2.Call<GenerateTokenModel> call, Response<GenerateTokenModel> response) {
                CommonUtils.dismissProgress();
                token = response.body().getDetails();
                submitPayment(token);
            }

            @Override
            public void onFailure(Call<GenerateTokenModel> call, Throwable t) {
                CommonUtils.dismissProgress();
                Toast.makeText(PackageDetailsCarrierActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void submitPayment(String token) {
        DropInRequest dropInRequest = new DropInRequest().clientToken(token);
        startActivityForResult(dropInRequest.getIntent(this), REQUEST_OUT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_OUT) {
            try {
                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                String nonces = result.getPaymentMethodNonce().getNonce();
                getPayPalPaymentApi(nonces);
//                Toast.makeText(this, nonces, Toast.LENGTH_SHORT).show();
                Log.d("nonce", nonces);
            } catch (Exception e) {
            }

        } else if (requestCode == RESULT_CANCELED)
            Toast.makeText(this, "User cancel", Toast.LENGTH_SHORT).show();
        else {
            Exception exception = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
            Toast.makeText(this, exception.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    private void getPayPalPaymentApi(String nonces) {
        payPalPaymentVM.payPalPayment(PackageDetailsCarrierActivity.this, detail.getId(), "65", nonces, App.getAppPreference().GetString(ConstantData.USERID), carrierID).observe(this, new Observer<PayPalPaymentModel>() {
            @Override
            public void onChanged(@Nullable PayPalPaymentModel payPalPaymentModel) {
                if (payPalPaymentModel.getSuccess().equalsIgnoreCase("1"))
                    Toast.makeText(PackageDetailsCarrierActivity.this, payPalPaymentModel.getMessage(), Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(PackageDetailsCarrierActivity.this, payPalPaymentModel.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
