package omninos.com.farrycarrier.activity.sender;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import omninos.com.farrycarrier.model.UpdateProfileModel;
import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.util.App;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.viewmodels.UpdateProfileVM;

public class EditSenderProfileActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView BackButton, profilePic;
    private Button SaveButton;
    private static final int CAMERA_REQUEST = 200;
    private static final int PICK_FROM_GALLERY = 100;
    private Bitmap bitmap;
    private Uri uri;
    private String imagepath = "", path;
    private EditText editTextName, editTextPhone, editTextAddress,et_lastname;
    private UpdateProfileVM updateProfileVM;
    private String Name, PhoneNumber, Address, WorkExperience,lastName;
    MultipartBody.Part image;
    private String iName, iPhoneNumber, iAddress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_sender_profile);

        //data thru intent
        iName = getIntent().getStringExtra("name");
        iPhoneNumber = getIntent().getStringExtra("phone");
        iAddress = getIntent().getStringExtra("address");
        lastName=getIntent().getStringExtra("lastName");
        findIds();
        setUps();
        OnClicks();
        updateProfileVM = ViewModelProviders.of(this).get(UpdateProfileVM.class);
    }

    private void findIds() {
        BackButton = findViewById(R.id.back_btn);
        SaveButton = findViewById(R.id.save_btn);
        profilePic = findViewById(R.id.profilepic);
        editTextName = findViewById(R.id.et_name);
        editTextPhone = findViewById(R.id.et_number);
        editTextAddress = findViewById(R.id.et_address);
        et_lastname=findViewById(R.id.et_lastname);
    }

    private void setUps() {
        if (App.getAppPreference().GetString(ConstantData.IMAGEPATH).isEmpty())
            profilePic.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile_placeholder));
        else
            Glide.with(EditSenderProfileActivity.this).load(App.getAppPreference().GetString(ConstantData.IMAGEPATH)).into(profilePic);

        editTextName.setText(iName);
        editTextPhone.setText(iPhoneNumber);
        editTextAddress.setText(iAddress);
        et_lastname.setText(lastName);
    }

    private void OnClicks() {
        BackButton.setOnClickListener(this);
        SaveButton.setOnClickListener(this);
        profilePic.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                finish();
                break;
            case R.id.save_btn:
                validate();
                break;
            case R.id.profilepic:
                showPopup();
                break;


        }

    }

    private void validate() {
        String userId = App.getAppPreference().GetString(ConstantData.USERID);
        Name = editTextName.getText().toString();
        lastName=et_lastname.getText().toString();
        PhoneNumber = editTextPhone.getText().toString();
        Address = editTextAddress.getText().toString();
        WorkExperience = "";
//        imagepath = App.getAppPreference().GetString(ConstantData.IMAGEPATH);

        if (TextUtils.isEmpty(Name) || TextUtils.isEmpty(PhoneNumber) || TextUtils.isEmpty(Address) || TextUtils.isEmpty(lastName))
            CommonUtils.showSnackbarAlert(editTextName, ConstantData.Fill_DETAILS);
        else if (PhoneNumber.length() < 10)
            CommonUtils.showSnackbarAlert(editTextPhone, "Enter minimun 10 digits");
        else {
            if (TextUtils.isEmpty(imagepath)) {
                RequestBody attachmentEmpty = RequestBody.create(MediaType.parse("text/plain"), "");

                image = MultipartBody.Part.createFormData("userImage", "", attachmentEmpty);
            } else {
                File file = new File(imagepath);
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                image = MultipartBody.Part.createFormData("userImage", file.getName(), requestFile);
                Glide.with(this).load(R.drawable.ic_profile_placeholder).into(profilePic);
            }
            RequestBody userIdbody = RequestBody.create(MediaType.parse("text/plain"), userId);
            RequestBody phonebody = RequestBody.create(MediaType.parse("text/plain"), PhoneNumber);
            RequestBody addressbody = RequestBody.create(MediaType.parse("text/plain"), Address);
            RequestBody workExperiencebody = RequestBody.create(MediaType.parse("text/plain"), WorkExperience);
            RequestBody namebody = RequestBody.create(MediaType.parse("text/plain"), Name);
            RequestBody lastNamebody = RequestBody.create(MediaType.parse("text/plain"), lastName);

            updateProfile(userIdbody, phonebody, addressbody, workExperiencebody, namebody, lastNamebody,image);
        }
    }

    private void updateProfile(RequestBody userIdbody, RequestBody phonebody, RequestBody addressbody, RequestBody workExperiencebody, RequestBody namebody,RequestBody lastNamebody, MultipartBody.Part image) {
        updateProfileVM.updateprofile(EditSenderProfileActivity.this, userIdbody, phonebody, addressbody, workExperiencebody, namebody, lastNamebody,image).observe(this, new Observer<UpdateProfileModel>() {
            @Override
            public void onChanged(@Nullable UpdateProfileModel updateProfileModel) {
                if (updateProfileModel.getSuccess().equalsIgnoreCase("1")) {
                    App.getAppPreference().SaveString(ConstantData.Name, Name);
//                    Toast.makeText(EditSenderProfileActivity.this, "Profile Updated", Toast.LENGTH_SHORT).show();
                    Intent saved = new Intent(EditSenderProfileActivity.this, SenderProfileActivity.class);
                    startActivity(saved);
                    Intent intent = new Intent("UpdateSenderProfilePic");
                    LocalBroadcastManager.getInstance(EditSenderProfileActivity.this).sendBroadcast(intent);
                    finish();
                } else
                    CommonUtils.showSnackbarAlert(editTextName, updateProfileModel.getMessage());
            }
        });
    }


    private void showPopup() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(EditSenderProfileActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo"))
                    cameraIntent();
                else if (items[item].equals("Choose from Library"))
                    galleryIntent();
                else if (items[item].equals("Cancel"))
                    dialog.dismiss();
            }
        });
        builder.show();

    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        PackageManager packageManager = this.getPackageManager();
        List<ResolveInfo> listcam = packageManager.queryIntentActivities(intent, 0);
        intent.setPackage(listcam.get(0).activityInfo.packageName);
        startActivityForResult(intent, CAMERA_REQUEST);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), PICK_FROM_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PICK_FROM_GALLERY)
                onSelectFromGalleryResult(data);
            else if (requestCode == CAMERA_REQUEST)
                onCaptureImageResult(data);
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        uri = getImageUri(this, bitmap);

        imagepath = getRealPathFromUri(uri);
        App.getAppPreference().SaveString(ConstantData.IMAGEPATH, imagepath);
        Glide.with(this).load("file://" + imagepath).into(profilePic);
    }


    private void onCaptureImageResult(Intent data) {
        try {
            bitmap = (Bitmap) data.getExtras().get("data");
            uri = getImageUri(EditSenderProfileActivity.this, bitmap);
            imagepath = getRealPathFromUri(uri);
            App.getAppPreference().SaveString(ConstantData.IMAGEPATH, imagepath);
            Glide.with(this).load("file://" + imagepath).into(profilePic);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private Uri getImageUri(EditSenderProfileActivity activity, Bitmap bitmap) {
        path = MediaStore.Images.Media.insertImage(activity.getContentResolver(), bitmap, "Title", null);
        return Uri.parse(path);
    }

    private String getRealPathFromUri(Uri tempUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = this.getContentResolver().query(tempUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}
