package omninos.com.farrycarrier.activity.sender;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import de.hdodenhof.circleimageview.CircleImageView;
import omninos.com.farrycarrier.model.UserProfileModel;
import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.util.App;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.viewmodels.UserProfileVM;

public class SenderProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView BackButton, EditButton;
    private String UserId;
    private TextView textViewName, textViewPhone, textViewAddress, tv_lastname;
    private UserProfileVM userProfileVM;
    private CircleImageView profilePic;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sender_profile);
        userProfileVM = ViewModelProviders.of(this).get(UserProfileVM.class);
        UserId = App.getAppPreference().GetString(ConstantData.USERID);

        findIds();
        setUps();

        getUserProfileApi();
        onClicks();

    }

    private void setUps() {
        if (App.getAppPreference().GetString(ConstantData.IMAGEPATH).isEmpty())
            profilePic.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile_placeholder));
        else
            Glide.with(SenderProfileActivity.this).load(App.getAppPreference().GetString(ConstantData.IMAGEPATH)).into(profilePic);
    }

    private void getUserProfileApi() {
        userProfileVM.userprofile(SenderProfileActivity.this, UserId).observe(this, new Observer<UserProfileModel>() {
            @Override
            public void onChanged(@Nullable UserProfileModel userProfileModel) {
                if (userProfileModel.getSuccess().equalsIgnoreCase("1")) {

                    App.getAppPreference().SaveString(ConstantData.Name, userProfileModel.getDetails().getName());
                    App.getAppPreference().SaveString(ConstantData.IMAGEPATH, userProfileModel.getDetails().getUserImage());
                    if (userProfileModel.getDetails().getUserImage().isEmpty()) {
                        profilePic.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile_placeholder));
                    } else
                        Glide.with(SenderProfileActivity.this).load(userProfileModel.getDetails().getUserImage()).into(profilePic);

                    textViewName.setText(userProfileModel.getDetails().getName());
                    textViewPhone.setText(userProfileModel.getDetails().getPhone());
                    textViewAddress.setText(userProfileModel.getDetails().getAddress());
                    tv_lastname.setText(userProfileModel.getDetails().getLastName());
                } else
                    CommonUtils.showSnackbarAlert(textViewName, userProfileModel.getMessage());
            }
        });
    }


    private void findIds() {
        //buttons
        BackButton = findViewById(R.id.back_btn);
        EditButton = findViewById(R.id.edit_btn);
        //text Views
        textViewName = findViewById(R.id.tv_name);
        textViewPhone = findViewById(R.id.tv_phone);
        textViewAddress = findViewById(R.id.tv_address);
        //Image View
        profilePic = findViewById(R.id.profilepic);
        tv_lastname = findViewById(R.id.tv_lastname);

    }

    private void onClicks() {
        BackButton.setOnClickListener(this);
        EditButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                finish();
                break;
            case R.id.edit_btn:
                Intent intent = new Intent(SenderProfileActivity.this, EditSenderProfileActivity.class);
                intent.putExtra("name", textViewName.getText().toString());
                intent.putExtra("phone", textViewPhone.getText().toString());
                intent.putExtra("address", textViewAddress.getText().toString());
                intent.putExtra("lastName", tv_lastname.getText().toString());
                startActivity(intent);
                finish();
                break;
        }

    }
}
