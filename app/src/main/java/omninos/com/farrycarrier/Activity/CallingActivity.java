package omninos.com.farrycarrier.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import omninos.com.farrycarrier.R;

public class CallingActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView callcancle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calling_);

        findids();

    }

    private void findids() {
        callcancle=findViewById(R.id.callcancle);
        callcancle.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.callcancle:
                onBackPressed();
                break;
        }


    }
}
