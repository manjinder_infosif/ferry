package omninos.com.farrycarrier.activity;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;

import java.util.Map;

import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.activity.sender.PackageDetailsCarrierActivity;
import omninos.com.farrycarrier.model.GenerateTokenModel;
import omninos.com.farrycarrier.model.PayPalPaymentModel;
import omninos.com.farrycarrier.model.WalletBalanceModel;
import omninos.com.farrycarrier.model.WalletRechargeModel;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import omninos.com.farrycarrier.util.App;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.viewmodels.AccountDetailsViewModel;
import omninos.com.farrycarrier.viewmodels.PayPalPaymentVM;
import omninos.com.farrycarrier.viewmodels.WalletBalanceVM;
import omninos.com.farrycarrier.viewmodels.WalletRechargeVM;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WalletActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView backButton;
    private Button topUpButton, Request_Money;
    private EditText editTextEnterAmount;
    private TextView tv20, tv50, tv100, tvWalletBalance;
    private String token, nonces, amountEntered;
    private int REQUEST_OUT = 1234;
    private PayPalPaymentVM payPalPaymentVM;
    private WalletRechargeVM walletRechargeVM;
    private WalletBalanceVM walletBalanceVM;
    private Activity activity;
    private AccountDetailsViewModel viewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        activity = WalletActivity.this;

        viewModel = ViewModelProviders.of(this).get(AccountDetailsViewModel.class);
        findIds();
        setUps();
        getWalletBalanceApi();
    }

    private void findIds() {
        backButton = findViewById(R.id.back_btn);
        topUpButton = findViewById(R.id.btn_topup);
        editTextEnterAmount = findViewById(R.id.et_enter_amount);

        tv20 = findViewById(R.id.tv_20);
        tv50 = findViewById(R.id.tv_50);
        tv100 = findViewById(R.id.tv_100);
        tvWalletBalance = findViewById(R.id.tv_wallet_balance);
        Request_Money = findViewById(R.id.Request_Money);

        //clickListeners
        backButton.setOnClickListener(this);
        topUpButton.setOnClickListener(this);
        tv20.setOnClickListener(this);
        tv50.setOnClickListener(this);
        tv100.setOnClickListener(this);
        Request_Money.setOnClickListener(this);

        if (getIntent().getStringExtra("Type").equalsIgnoreCase("0")) {
            Request_Money.setVisibility(View.VISIBLE);
        } else {
            Request_Money.setVisibility(View.GONE);
        }
    }

    private void setUps() {
        payPalPaymentVM = ViewModelProviders.of(this).get(PayPalPaymentVM.class);
        walletRechargeVM = ViewModelProviders.of(this).get(WalletRechargeVM.class);
        walletBalanceVM = ViewModelProviders.of(this).get(WalletBalanceVM.class);
    }

    private void getWalletBalanceApi() {
        walletBalanceVM.walletBalance(activity, App.getAppPreference().GetString(ConstantData.USERID)).observe(this, new Observer<WalletBalanceModel>() {
            @Override
            public void onChanged(@Nullable WalletBalanceModel walletBalanceModel) {
                if (walletBalanceModel.getSuccess().equalsIgnoreCase("1")) {
                    tvWalletBalance.setText("$ " + walletBalanceModel.getWalletBalance());
                    Toast.makeText(activity, walletBalanceModel.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    CommonUtils.showSnackbarAlert(tvWalletBalance, walletBalanceModel.getMessage());
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                finish();
                break;

            case R.id.btn_topup:
                validate();
                break;

            case R.id.tv_20:
                tv20.setBackground(getResources().getDrawable(R.drawable.design_et_black_stroke));
                tv50.setBackground(getResources().getDrawable(R.drawable.edit_text_stroke));
                tv100.setBackground(getResources().getDrawable(R.drawable.edit_text_stroke));
                editTextEnterAmount.setText("65");
                break;

            case R.id.tv_50:
                tv50.setBackground(getResources().getDrawable(R.drawable.design_et_black_stroke));
                tv20.setBackground(getResources().getDrawable(R.drawable.edit_text_stroke));
                tv100.setBackground(getResources().getDrawable(R.drawable.edit_text_stroke));
                editTextEnterAmount.setText("130");
                break;

            case R.id.tv_100:
                tv100.setBackground(getResources().getDrawable(R.drawable.design_et_black_stroke));
                tv20.setBackground(getResources().getDrawable(R.drawable.edit_text_stroke));
                tv50.setBackground(getResources().getDrawable(R.drawable.edit_text_stroke));
                editTextEnterAmount.setText("195");
                break;

            case R.id.Request_Money:
                SendRequest();
                break;
        }
    }

    private void SendRequest() {
        viewModel.requestMoneyData(WalletActivity.this, App.getAppPreference().GetString(ConstantData.USERID)).observe(WalletActivity.this, new Observer<Map>() {
            @Override
            public void onChanged(@Nullable Map map) {
                if (map.get("success").toString().equalsIgnoreCase("1")) {
                    Toast.makeText(activity, "Request send Successfully", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                }else {
                    Toast.makeText(activity, map.get("message").toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void validate() {
        amountEntered = editTextEnterAmount.getText().toString();
        if (TextUtils.isEmpty(amountEntered))
            CommonUtils.showSnackbarAlert(editTextEnterAmount, ConstantData.Fill_DETAILS);
        else
            PayPalPayment();
        //walletRechargeApi(amountEntered);
    }

    private void PayPalPayment() {
        ApiService api = ApiClient.getApiClient().create(ApiService.class);
        CommonUtils.showProgress(WalletActivity.this, ConstantData.SHOW_PROGRESS_MESSAGE);
        api.generatetoken().enqueue(new Callback<GenerateTokenModel>() {
            @Override
            public void onResponse(retrofit2.Call<GenerateTokenModel> call, Response<GenerateTokenModel> response) {
                CommonUtils.dismissProgress();
                token = response.body().getDetails();
                submitPayment(token);
            }

            @Override
            public void onFailure(Call<GenerateTokenModel> call, Throwable t) {
                CommonUtils.dismissProgress();
                Toast.makeText(WalletActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void submitPayment(String token) {
        DropInRequest dropInRequest = new DropInRequest().clientToken(token);
        startActivityForResult(dropInRequest.getIntent(this), REQUEST_OUT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_OUT) {
            try {
                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                nonces = result.getPaymentMethodNonce().getNonce();
                walletRechargeApi(amountEntered, nonces);
                //getPayPalPaymentApi(nonces);
                //Toast.makeText(this, nonces, Toast.LENGTH_S       HORT).show();
                Log.d("nonce", nonces);
            } catch (Exception e) {

            }

        } else if (requestCode == RESULT_CANCELED) {
            Toast.makeText(this, "User cancel", Toast.LENGTH_SHORT).show();
        } else {
            Exception exception = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
            Toast.makeText(this, exception.toString(), Toast.LENGTH_SHORT).show();
        }

    }

    private void walletRechargeApi(final String amountEntered, String nonces) {
        walletRechargeVM.walletRecharge(WalletActivity.this, amountEntered, App.getAppPreference().GetString(ConstantData.USERID), nonces).observe(this, new Observer<WalletRechargeModel>() {
            @Override
            public void onChanged(@Nullable WalletRechargeModel walletRechargeModel) {
                if (walletRechargeModel.getSuccess().equalsIgnoreCase("1")) {
                    //getPayPalPaymentApi(nonces);

                    editTextEnterAmount.setText("");
                    getWalletBalanceApi();
                    tv20.setBackground(getResources().getDrawable(R.drawable.edit_text_stroke));
                    tv50.setBackground(getResources().getDrawable(R.drawable.edit_text_stroke));
                    tv100.setBackground(getResources().getDrawable(R.drawable.edit_text_stroke));

                } else {
                    Toast.makeText(WalletActivity.this, walletRechargeModel.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void getPayPalPaymentApi(String nonces) {
        payPalPaymentVM.payPalPayment(WalletActivity.this, App.getAppPreference().GetString(ConstantData.USERID), editTextEnterAmount.getText().toString(), nonces, App.getAppPreference().GetString(ConstantData.USERID), App.getAppPreference().GetString(ConstantData.USERID)).observe(this, new Observer<PayPalPaymentModel>() {
            @Override
            public void onChanged(@Nullable PayPalPaymentModel payPalPaymentModel) {
                if (payPalPaymentModel.getSuccess().equalsIgnoreCase("1")) {
                    Toast.makeText(WalletActivity.this, payPalPaymentModel.getMessage(), Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(WalletActivity.this, payPalPaymentModel.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
