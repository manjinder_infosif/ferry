package omninos.com.farrycarrier.activity;

import android.app.Activity;
import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import omninos.com.farrycarrier.model.SendOTPModel;
import omninos.com.farrycarrier.util.App;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.model.RegisterUserModel;
import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.viewmodels.RegisterUserVM;
import omninos.com.farrycarrier.viewmodels.SendOtpVM;

public class VerificationCodeActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn_next;
    private Activity activity;
    private ImageView backbtn;
    private RegisterUserVM registerUserVM;
    private String name, email, password, phonenumber, token, otp, lastName;
    private TextView tvResendCode;
    private EditText firstNumberVerify, secondNumberVerify, thirdNumberVerify, fourthNumberVerify;
    private SendOtpVM sendOtpVM;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_code_);
        //viewModels
        registerUserVM = ViewModelProviders.of(this).get(RegisterUserVM.class);
        sendOtpVM = ViewModelProviders.of(this).get(SendOtpVM.class);

        //Data passed through intent
        Intent intent = getIntent();
        name = intent.getStringExtra("username");
        email = intent.getStringExtra("useremail");
        password = intent.getStringExtra("userpassword");
        phonenumber = intent.getStringExtra("userphonenumber");
        otp = intent.getStringExtra("otp");
        lastName = intent.getStringExtra("lastName");

        findids();
        setUps();
        getToken();
    }

    private void findids() {
        btn_next = findViewById(R.id.btn_next);
        backbtn = findViewById(R.id.backbtn);


        firstNumberVerify = findViewById(R.id.firstNumberVerify);
        secondNumberVerify = findViewById(R.id.secondNumberVerify);
        thirdNumberVerify = findViewById(R.id.thirdNumberVerify);
        fourthNumberVerify = findViewById(R.id.fourthNumberVerify);

        tvResendCode = findViewById(R.id.tv_resend);
    }

    private void setUps() {
        activity = VerificationCodeActivity.this;
        btn_next.setOnClickListener(this);
        backbtn.setOnClickListener(this);
        tvResendCode.setOnClickListener(this);

        firstNumberVerify.addTextChangedListener(new GenericTextWatcher(firstNumberVerify));
        secondNumberVerify.addTextChangedListener(new GenericTextWatcher(secondNumberVerify));
        thirdNumberVerify.addTextChangedListener(new GenericTextWatcher(thirdNumberVerify));
        fourthNumberVerify.addTextChangedListener(new GenericTextWatcher(fourthNumberVerify));
    }

    private void getToken() {
        token = FirebaseInstanceId.getInstance().getToken();
    }


    public class GenericTextWatcher implements TextWatcher {

        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            switch (view.getId()) {

                case R.id.firstNumberVerify:
                    if (text.length() == 1) {
                        firstNumberVerify.setBackground(getResources().getDrawable(R.drawable.verificationet_design));
                        secondNumberVerify.requestFocus();
                    } else if (text.length() == 0) {
                        firstNumberVerify.setBackground(getResources().getDrawable(R.drawable.socialbutton_design));
                    }

                    break;
                case R.id.secondNumberVerify:
                    if (text.length() == 1) {
                        secondNumberVerify.setBackground(getResources().getDrawable(R.drawable.verificationet_design));
                        thirdNumberVerify.requestFocus();
                    } else if (text.length() == 0) {
                        secondNumberVerify.setBackground(getResources().getDrawable(R.drawable.socialbutton_design));
                        firstNumberVerify.requestFocus();
                    }


                    break;
                case R.id.thirdNumberVerify:
                    if (text.length() == 1) {
                        thirdNumberVerify.setBackground(getResources().getDrawable(R.drawable.verificationet_design));
                        fourthNumberVerify.requestFocus();
                    } else if (text.length() == 0) {
                        secondNumberVerify.requestFocus();
                        thirdNumberVerify.setBackground(getResources().getDrawable(R.drawable.socialbutton_design));
                    }

                    break;
                case R.id.fourthNumberVerify:
                    if (text.length() == 1) {
                        fourthNumberVerify.setBackground(getResources().getDrawable(R.drawable.verificationet_design));

                        btn_next.requestFocus();
                    } else if (text.length() == 0) {
                        thirdNumberVerify.requestFocus();
                        fourthNumberVerify.setBackground(getResources().getDrawable(R.drawable.socialbutton_design));

                    }
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
                validate();
                break;

            case R.id.backbtn:
                onBackPressed();
                break;

            case R.id.tv_resend:
                resendCode(phonenumber);
                break;
        }
    }

    private void validate() {
        String firstNum = firstNumberVerify.getText().toString();
        String secondNum = secondNumberVerify.getText().toString();
        String thirdNum = thirdNumberVerify.getText().toString();
        String fourthNum = fourthNumberVerify.getText().toString();
        String otpValue = firstNum + secondNum + thirdNum + fourthNum;

        if (TextUtils.isEmpty(firstNum) || TextUtils.isEmpty(secondNum) || TextUtils.isEmpty(thirdNum) || TextUtils.isEmpty(fourthNum))
            CommonUtils.showSnackbarAlert(firstNumberVerify, ConstantData.Fill_DETAILS);
        else if (!otpValue.equals(otp))
            CommonUtils.showSnackbarAlert(firstNumberVerify, "Incorrect OTP.");
        else
            CheckUserReg();
    }

    private void CheckUserReg() {
        registerUserVM.registeruser(activity, name, email, phonenumber, password, "Android", token, "", "",lastName).observe(this, new Observer<RegisterUserModel>() {
            @Override
            public void onChanged(@Nullable RegisterUserModel registerUserModel) {
                if (registerUserModel.getSuccess().equalsIgnoreCase("1")) {
                    App.getAppPreference().SaveString(ConstantData.USERID, registerUserModel.getDetails().getId());

                    dialogVerifyCode();
                } else
                    CommonUtils.showSnackbarAlert(firstNumberVerify, registerUserModel.getMessage());
            }
        });
    }

    private void resendCode(String phonenumber) {
        sendOtpVM.checkphonenumber(VerificationCodeActivity.this, phonenumber).observe(this, new Observer<SendOTPModel>() {
            @Override
            public void onChanged(@Nullable SendOTPModel sendOTPModel) {
                if (sendOTPModel.getSuccess().equalsIgnoreCase("1")) {
                    String newOTP = sendOTPModel.getDetails().toString();
                    otp = newOTP;
//                    validate();
                    Toast.makeText(activity, sendOTPModel.getDetails().toString(), Toast.LENGTH_LONG).show();
                } else
                    CommonUtils.showSnackbarAlert(tvResendCode, sendOTPModel.getMessage());
            }
        });
    }

    private void dialogVerifyCode() {
        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.confirmation_dialog);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button btn_done = dialog.findViewById(R.id.btn_done);
        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                App.getAppPreference().SaveString(ConstantData.TOKEN, "1");
                startActivity(new Intent(activity, SelectTypeActivity.class));
                finishAffinity();
            }
        });
        dialog.show();
    }
}

