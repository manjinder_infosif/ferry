package omninos.com.farrycarrier.activity;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import omninos.com.farrycarrier.activity.carrier.HomeCarrierActivity;
import omninos.com.farrycarrier.activity.sender.HomeSenderActivity;
import omninos.com.farrycarrier.util.App;
import omninos.com.farrycarrier.model.CheckStatusModel;
import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.viewmodels.CheckStatusVM;

public class SelectTypeActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btn_sender, btn_carrier;
    private Activity activity;
    private CheckStatusVM checkStatusVM;
    private String status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_type_);
        checkStatusVM = ViewModelProviders.of(this).get(CheckStatusVM.class);
        findids();
    }

    private void findids() {
        activity = SelectTypeActivity.this;
        btn_sender = findViewById(R.id.btn_sender);
        btn_carrier = findViewById(R.id.btn_carrier);

        //click Listeners
        btn_sender.setOnClickListener(this);
        btn_carrier.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_sender:
                senderButton();
                break;

            case R.id.btn_carrier:
                carrierButton();
                break;
        }
    }


    private void senderButton() {
        status = "1";
        checkStatus(status);
        btn_sender.setTextColor(getResources().getColor(R.color.whiteColor));
        btn_sender.setBackground(getResources().getDrawable(R.drawable.design_button_black));
        btn_carrier.setTextColor(getResources().getColor(R.color.blackColor));
        btn_carrier.setBackground(getResources().getDrawable(R.drawable.buttondesign_signin_white));
    }

    private void carrierButton() {
        status = "2";
        checkStatus(status);
        btn_carrier.setTextColor(getResources().getColor(R.color.whiteColor));
        btn_carrier.setBackground(getResources().getDrawable(R.drawable.design_button_black));
        btn_sender.setTextColor(getResources().getColor(R.color.blackColor));
        btn_sender.setBackground(getResources().getDrawable(R.drawable.buttondesign_signin_white));
    }

    private void checkStatus(final String status1) {
        String token = FirebaseInstanceId.getInstance().getToken();
        checkStatusVM.checkStatus(activity, status1, App.getAppPreference().GetString(ConstantData.USERID), token).observe(this, new Observer<CheckStatusModel>() {
            @Override
            public void onChanged(@Nullable CheckStatusModel status) {
                if (status.getSuccess().equalsIgnoreCase("1")) {
                    if (status1.equalsIgnoreCase("1")) {
                        Intent i = new Intent(activity, HomeSenderActivity.class);
                        startActivity(i);
                        CommonUtils.dismissProgress();
                    } else {
                        Intent it = new Intent(activity, HomeCarrierActivity.class);
                        startActivity(it);
                        CommonUtils.dismissProgress();
                    }
                } else
                    Toast.makeText(activity, status.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
