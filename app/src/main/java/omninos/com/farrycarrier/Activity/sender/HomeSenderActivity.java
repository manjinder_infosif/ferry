package omninos.com.farrycarrier.activity.sender;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.activity.AboutActivity;
import omninos.com.farrycarrier.activity.AboutAppActivity;
import omninos.com.farrycarrier.activity.LoginActivity;
import omninos.com.farrycarrier.activity.SettingsActivity;
import omninos.com.farrycarrier.activity.WalletActivity;
import omninos.com.farrycarrier.fragment.SenderCurrentPackageFragment;
import omninos.com.farrycarrier.fragment.SenderPastPackageFragment;
import omninos.com.farrycarrier.util.App;
import omninos.com.farrycarrier.util.ConstantData;

public class HomeSenderActivity extends AppCompatActivity implements View.OnClickListener, TabLayout.OnTabSelectedListener {
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ImageView OpenDrawer;
    private TabLayout tabLayout;
    private ViewPager viewPagerPackages;
    private String[] string = {"Current Packages", "Past Packages"};
    private String status, name;
    private TextView textViewName;
    CircleImageView profilePic;
    GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        status = getIntent().getStringExtra("status");

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);


        findIds();
        setUps();
        onclicks();
    }

    private void setUps() {
        setupViewPager(viewPagerPackages);
        tabLayout.setupWithViewPager(viewPagerPackages);
        name = App.getAppPreference().GetString(ConstantData.Name);


        View headerView = navigationView.getHeaderView(0);
        textViewName = headerView.findViewById(R.id.tv_username);
        textViewName.setText(name);
        profilePic = headerView.findViewById(R.id.profilepic);
        if (App.getAppPreference().GetString(ConstantData.IMAGEPATH).isEmpty()) {
            profilePic.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile_placeholder));
        } else {
            Glide.with(HomeSenderActivity.this).load(App.getAppPreference().GetString(ConstantData.IMAGEPATH)).into(profilePic);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (App.getAppPreference().GetString(ConstantData.IMAGEPATH).isEmpty())
            profilePic.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile_placeholder));
        else
            Glide.with(HomeSenderActivity.this).load(App.getAppPreference().GetString(ConstantData.IMAGEPATH)).into(profilePic);

        name = App.getAppPreference().GetString(ConstantData.Name);
        textViewName.setText(name);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (App.getAppPreference().GetString(ConstantData.IMAGEPATH).isEmpty())
            profilePic.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile_placeholder));
        else
            Glide.with(HomeSenderActivity.this).load(App.getAppPreference().GetString(ConstantData.IMAGEPATH)).into(profilePic);

        name = App.getAppPreference().GetString(ConstantData.Name);
        textViewName.setText(name);
    }

    private void findIds() {
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        OpenDrawer = findViewById(R.id.open_menu);
        tabLayout = findViewById(R.id.tab_layout);
        viewPagerPackages = findViewById(R.id.view_pager_packages);

//        LocalBroadcastManager.getInstance(HomeSenderActivity.this).registerReceiver(new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//
//                Glide.with(HomeSenderActivity.this).load(App.getAppPreference().GetString(ConstantData.IMAGEPATH)).into(profilePic);
//                name = App.getAppPreference().GetString(ConstantData.Name);
//                textViewName.setText(name);
//
//            }
//        }, new IntentFilter("UpdateSenderProfilePic"));
    }

    private void setupViewPager(ViewPager viewPagerPackages) {
        HomeSenderActivity.ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(new SenderCurrentPackageFragment(), string[0]);
        adapter.addFragment(new SenderPastPackageFragment(), string[1]);

        viewPagerPackages.setAdapter(adapter);
    }

    private void onclicks() {
        OpenDrawer.setOnClickListener(this);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                switch (menuItem.getItemId()) {
                    case R.id.myprofile:
                        drawerLayout.closeDrawer(GravityCompat.START);
                        Intent profile = new Intent(HomeSenderActivity.this, SenderProfileActivity.class);
                        startActivity(profile);
                        break;

                    case R.id.sendpackage:
                        drawerLayout.closeDrawer(GravityCompat.START);
                        Intent intent = new Intent(HomeSenderActivity.this, SendPackageActivity.class);
                        startActivity(intent);
                        break;

                    case R.id.wallet:
                        drawerLayout.closeDrawer(GravityCompat.START);
                        Intent wallet = new Intent(HomeSenderActivity.this, WalletActivity.class);
                        wallet.putExtra("Type", "1");
                        startActivity(wallet);
                        break;

                    case R.id.about:
                        drawerLayout.closeDrawer(GravityCompat.START);
                        Intent about = new Intent(HomeSenderActivity.this, AboutActivity.class);
                        startActivity(about);
                        break;

                    case R.id.settings:
                        drawerLayout.closeDrawer(GravityCompat.START);
                        Intent settings = new Intent(HomeSenderActivity.this, SettingsActivity.class);
                        startActivity(settings);
                        break;

                    case R.id.logout:
                        mGoogleSignInClient.signOut();
                        disconnectFromFacebook();
                        drawerLayout.closeDrawer(GravityCompat.START);
                        App.getAppPreference().Logout(HomeSenderActivity.this);
                        Intent login = new Intent(HomeSenderActivity.this, LoginActivity.class);
                        startActivity(login);
                        finishAffinity();
                        break;

                    case R.id.aboutApp:
                        startActivity(new Intent(HomeSenderActivity.this, AboutAppActivity.class));
                        break;
                }
                return false;
            }
        });

    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPagerPackages.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.open_menu:
                drawerLayout.openDrawer(GravityCompat.START);
                break;
        }
    }

    public void disconnectFromFacebook() {

        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {

                LoginManager.getInstance().logOut();

            }
        }).executeAsync();
    }


    public class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmenttitle = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmenttitle.add(title);
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return fragmenttitle.get(position);
        }
    }
}
