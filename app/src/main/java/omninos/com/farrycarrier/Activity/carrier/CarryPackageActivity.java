package omninos.com.farrycarrier.activity.carrier;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.model.AirportListModel;
import omninos.com.farrycarrier.model.carrier.CarrierInformationModel;
import omninos.com.farrycarrier.util.App;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.viewmodels.CarrierInformationVM;

public class CarryPackageActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private Activity activity;
    private ImageView appbarBack, appbarnotificationicon;
    private Button btn_submit;
    private TextView appbarText, tvTravleDateFrom, carpinn, tvTravelDateEnd, tvlocalAirportCode, tvdestinationAirportCode;
    private RadioGroup radiogrp;
    private RadioButton rbselfDelivery, rbcourierDelivery;
    private String fromDate, toDate, checkString, country, deliveryStatus, LocalAirports, DestinationAirports, type, startTime, endTime;
    private CarrierInformationVM carrierInformationVM;
    private Spinner spinnerSourceCountry, spinnerDestinationCountry;
    private ArrayList<String> SourceCountries = new ArrayList<>();
    private ArrayList<String> DestinationCountries = new ArrayList<>();
    private RecyclerView recyclerViewLocalAirports, recyclerViewDestinationAirports;
    final List<String> airportList = new ArrayList<>();
    private List<AirportListModel.Detail> details = new ArrayList<>();


    SingleDateAndTimePickerDialog.Builder singleBuilder;

    SimpleDateFormat simpleDateFormat, simpleDateFormat1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carrier__profile_);
        carrierInformationVM = ViewModelProviders.of(this).get(CarrierInformationVM.class);
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        simpleDateFormat1 = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());


        findids();
        setUps();
        spinnerSource_Country();
        spinnerDestination_Country();
    }

    private void setUps() {
        appbarBack.setVisibility(View.VISIBLE);
        appbarnotificationicon.setVisibility(View.GONE);
        appbarText.setText("Carry Package");
        appbarBack.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
        radiogrp.setOnCheckedChangeListener(this);
        tvTravleDateFrom.setOnClickListener(this);
        tvTravelDateEnd.setOnClickListener(this);
        tvlocalAirportCode.setOnClickListener(this);
        tvdestinationAirportCode.setOnClickListener(this);

    }

    private void findids() {
        activity = CarryPackageActivity.this;
        appbarBack = findViewById(R.id.appbarBack);
        appbarnotificationicon = findViewById(R.id.appbarnotificationicon);
        btn_submit = findViewById(R.id.btn_submit);
        radiogrp = findViewById(R.id.radiogrp);
        //textviews
        tvTravleDateFrom = findViewById(R.id.eddate_from);
        tvlocalAirportCode = findViewById(R.id.tv_local_airport);
        tvdestinationAirportCode = findViewById(R.id.tv_destination_airport);
        tvTravelDateEnd = findViewById(R.id.eddate_to);
        appbarText = findViewById(R.id.appbarText);
        //spinners
        spinnerSourceCountry = findViewById(R.id.spinner_source_country);
        spinnerDestinationCountry = findViewById(R.id.spinner_destination_country);
        //recycler views
        recyclerViewLocalAirports = findViewById(R.id.recyclerview_locallist);
        recyclerViewDestinationAirports = findViewById(R.id.recyclerview_destlist);
    }

    private void spinnerDestination_Country() {
        DestinationCountries.add("Select Country");
        DestinationCountries.add("Canada");
        DestinationCountries.add("United Kingdom");
        DestinationCountries.add("India");
        DestinationCountries.add("Philippines");
        DestinationCountries.add("United States");
        DestinationCountries.add("Guyana");

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(CarryPackageActivity.this, R.layout.support_simple_spinner_dropdown_item, DestinationCountries);
        arrayAdapter.setDropDownViewResource(R.layout.spinner_text_view);
        spinnerDestinationCountry.setAdapter(arrayAdapter);
        spinnerDestinationCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                country = DestinationCountries.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void spinnerSource_Country() {
        SourceCountries.add("Select Country");
        SourceCountries.add("Canada");
        SourceCountries.add("United Kingdom");
        SourceCountries.add("India");
        SourceCountries.add("Philippines");
        SourceCountries.add("United States");
        SourceCountries.add("Guyana");

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(CarryPackageActivity.this, R.layout.support_simple_spinner_dropdown_item, SourceCountries);
        arrayAdapter.setDropDownViewResource(R.layout.spinner_text_view);
        spinnerSourceCountry.setAdapter(arrayAdapter);

        spinnerSourceCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                country = SourceCountries.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_submit:
                validate();
                break;

            case R.id.eddate_from:
                type = "from";
                ChooseDate(type);
                break;

            case R.id.eddate_to:
                type = "to";
                ChooseDate(type);
                break;

            case R.id.appbarBack:
                onBackPressed();
                break;

            case R.id.tv_local_airport:
                Intent intent = new Intent(CarryPackageActivity.this, AirportListActivity.class);
                intent.putExtra("country", country);
                startActivityForResult(intent, 100);
                break;

            case R.id.tv_destination_airport:
                Intent intent1 = new Intent(CarryPackageActivity.this, AirportListActivity.class);
                intent1.putExtra("country", country);
                startActivityForResult(intent1, 101);
                break;
        }

    }

    private void ChooseDate(final String type) {

        String msg;
        if (type.equalsIgnoreCase("from")) {
            msg = "Choose Departure Date";
        } else {
            msg = "Choose Arrival Date";
        }
        Calendar calendar = Calendar.getInstance();
        final Date defaultDate = calendar.getTime();
        singleBuilder = new SingleDateAndTimePickerDialog.Builder(this)
                //.bottomSheet()
//                .curved()
                .minutesStep(1)
                .defaultDate(defaultDate)
                .displayMinutes(true)
                .displayHours(true)
                .displayDays(true)
//                .displayYears(true)
//                .backgroundColor(Color.BLACK)
                .mainColor(Color.BLACK)
                .titleTextColor(Color.WHITE)
                .minDateRange(defaultDate)
//                .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
//                    @Override
//                    public void onDisplayed(SingleDateAndTimePicker picker) {
//
//                    }
//                })

                .title(msg)
                .listener(new SingleDateAndTimePickerDialog.Listener() {
                    @Override
                    public void onDateSelected(Date date) {
                        if (type.equalsIgnoreCase("from")) {
                            if (!tvTravelDateEnd.getText().toString().isEmpty()) {
                                Date second = stringToDate(tvTravelDateEnd.getText().toString(), "yyyy-MM-dd");
                                Date first = stringToDate(simpleDateFormat.format(date), "yyyy-MM-dd");
                                fromDate = simpleDateFormat.format(date);
                                if (first.after(second)) {
                                    CommonUtils.showSnackbarAlert(tvTravelDateEnd, "choose correct date");
                                } else {
                                    tvTravleDateFrom.setText(simpleDateFormat.format(date));
                                    fromDate = simpleDateFormat.format(date);
                                    startTime = simpleDateFormat1.format(date);
                                }
                            } else {
                                tvTravleDateFrom.setText(simpleDateFormat.format(date));
                                fromDate = simpleDateFormat.format(date);
                                startTime = simpleDateFormat1.format(date);
                            }
                        } else {
                            if (!tvTravleDateFrom.getText().toString().isEmpty()) {
                                Date first = stringToDate(tvTravleDateFrom.getText().toString(), "yyyy-MM-dd");
                                Date second = stringToDate(simpleDateFormat.format(date), "yyyy-MM-dd");
                                toDate = simpleDateFormat.format(date);
                                if (second.before(first)) {
                                    CommonUtils.showSnackbarAlert(tvTravelDateEnd, "choose correct date");
                                } else {
                                    toDate = simpleDateFormat.format(date);
                                    tvTravelDateEnd.setText(simpleDateFormat.format(date));
                                    endTime = simpleDateFormat1.format(date);
                                }
                            } else {
                                toDate = simpleDateFormat.format(date);
                                tvTravelDateEnd.setText(simpleDateFormat.format(date));
                                endTime = simpleDateFormat1.format(date);
                            }

                        }

                    }
                });
        singleBuilder.display();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                tvlocalAirportCode.setText(data.getStringExtra("airport") + "(" + data.getStringExtra("city") + ")");
                LocalAirports = data.getStringExtra("airport");
            }
        }
        if (requestCode == 101) {
            if (resultCode == RESULT_OK) {
                tvdestinationAirportCode.setText(data.getStringExtra("airport") + "(" + data.getStringExtra("city") + ")");
                DestinationAirports = data.getStringExtra("airport");
            }
        }
    }

    private void validate() {
        String LocalAirport = LocalAirports;
        String DestinationAirport = DestinationAirports;
        String DeliveryStatus = deliveryStatus;
        if (TextUtils.isEmpty(LocalAirport) || (TextUtils.isEmpty(DestinationAirport)) || (TextUtils.isEmpty(fromDate)) || (TextUtils.isEmpty(toDate)) || (TextUtils.isEmpty(DeliveryStatus)))
            CommonUtils.showSnackbarAlert(tvlocalAirportCode, "Fill Mandatory Fields");
        else if (LocalAirport.equals(DestinationAirport))
            CommonUtils.showSnackbarAlert(tvlocalAirportCode, "Source And Destination Airport cannot be the same!");
        else {
            System.out.println("Data: " + fromDate);
            System.out.println("Data: " + toDate);
            carrierinformation(LocalAirport, DestinationAirport, fromDate, toDate, DeliveryStatus);
        }

    }

    private void carrierinformation(String localAirport, String destinationAirport, String fromDate, String toDate, String deliveryStatus) {
        carrierInformationVM.carrierinformation(activity, App.getAppPreference().GetString(ConstantData.USERID), localAirport, destinationAirport, fromDate, toDate, deliveryStatus, startTime, endTime).observe(this, new Observer<CarrierInformationModel>() {
            @Override
            public void onChanged(@Nullable CarrierInformationModel carrierInformationModel) {
                if (carrierInformationModel.getSuccess().equalsIgnoreCase("1")) {
                    startActivity(new Intent(activity, HomeCarrierActivity.class));
                    finishAffinity();
                    Toast.makeText(activity, carrierInformationModel.getMessage(), Toast.LENGTH_SHORT).show();
                } else
                    CommonUtils.showSnackbarAlert(tvTravleDateFrom, carrierInformationModel.getMessage());
            }
        });
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (checkedId == R.id.rbselfDelivery)
            deliveryStatus = "1";
        else if (checkedId == R.id.rbcourierDelivery)
            deliveryStatus = "2";
    }

//    private void datepicker(final String type) {
//
//        final Calendar c = Calendar.getInstance();
//        int mYear = c.get(Calendar.YEAR);
//        int mMonth = c.get(Calendar.MONTH);
//        int mDay = c.get(Calendar.DAY_OF_MONTH);
//
//        DatePickerDialog datePickerDialog = new DatePickerDialog(activity, android.R.style.Theme_Holo_Light_Dialog,
//                new DatePickerDialog.OnDateSetListener() {
//                    @Override
//                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                        fromDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
////                        SimpleDateFormat input = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
//                        SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd");
//                        Date d = null;
//                        try {
//                            d = output.parse(fromDate);
//                        } catch (ParseException e) {
//                            e.printStackTrace();
//                        }
//                        String formatted = output.format(d);
//                        if (type.equalsIgnoreCase("from")) {
//                            tvTravleDateFrom.setText(formatted);
//                            fromDate = formatted;
//                        } else {
//                            tvTravelDateEnd.setText(formatted);
//                            toDate = formatted;
//                        }
//
//                        if (tvTravleDateFrom.getText().equals(tvTravelDateEnd.getText()))
//                            CommonUtils.showSnackbarAlert(tvTravleDateFrom, "Dates cannot be same.");
//
//
//                    }
//                }, mYear, mMonth, mDay);
//        Window dialogWindow = datePickerDialog.getWindow();
//        dialogWindow.setGravity(Gravity.CENTER);
//        datePickerDialog.show();
//    }

    private void PickDate(final String type) {
        final Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(
                activity, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker,
                                  int selectedyear, int selectedmonth,
                                  int selectedday) {

                mcurrentDate.set(Calendar.YEAR, selectedyear);
                mcurrentDate.set(Calendar.MONTH, selectedmonth);
                mcurrentDate.set(Calendar.DAY_OF_MONTH,
                        selectedday);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                if (type.equalsIgnoreCase("from")) {
                    if (!tvTravelDateEnd.getText().toString().isEmpty()) {
                        Date second = stringToDate(tvTravelDateEnd.getText().toString(), "yyyy-MM-dd");
                        Date first = stringToDate(sdf.format(mcurrentDate
                                .getTime()), "yyyy-MM-dd");
                        fromDate = sdf.format(mcurrentDate
                                .getTime());
                        if (first.after(second)) {
                            CommonUtils.showSnackbarAlert(tvTravelDateEnd, "choose correct date");
                        } else {
                            tvTravleDateFrom.setText(sdf.format(mcurrentDate
                                    .getTime()));
                            fromDate = sdf.format(mcurrentDate
                                    .getTime());
                        }
                    } else {
                        tvTravleDateFrom.setText(sdf.format(mcurrentDate
                                .getTime()));
                        fromDate = sdf.format(mcurrentDate
                                .getTime());
                    }
                } else {
                    if (!tvTravleDateFrom.getText().toString().isEmpty()) {
                        Date first = stringToDate(tvTravleDateFrom.getText().toString(), "yyyy-MM-dd");
                        Date second = stringToDate(sdf.format(mcurrentDate
                                .getTime()), "yyyy-MM-dd");
                        toDate = sdf.format(mcurrentDate
                                .getTime());
                        if (second.before(first)) {
                            CommonUtils.showSnackbarAlert(tvTravelDateEnd, "choose correct date");
                        } else {
                            toDate = sdf.format(mcurrentDate
                                    .getTime());
                            tvTravelDateEnd.setText(sdf.format(mcurrentDate.getTime()));
                        }
                    } else {
                        toDate = sdf.format(mcurrentDate
                                .getTime());
                        tvTravelDateEnd.setText(sdf.format(mcurrentDate.getTime()));
                    }

                }

            }
        }, mYear, mMonth, mDay);

        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
        mDatePicker.setTitle("Date of Birth");
        mDatePicker.show();
    }

    private Date stringToDate(String aDate, String aFormat) {

        if (aDate == null) return null;
        ParsePosition pos = new ParsePosition(0);
        SimpleDateFormat simpledateformat = new SimpleDateFormat(aFormat);
        Date stringDate = simpledateformat.parse(aDate, pos);
        return stringDate;

    }

//    private void getLocalAirport() {
//        TextWatcher textWatcher = new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                LocalAirports = localAirportCode.getText().toString();
////                showLocalAirportlist(s.toString());
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//            }
//        };
//        localAirportCode.addTextChangedListener(textWatcher);
//
//    }

//    private void getDestinationAirport() {
//        TextWatcher textWatcher = new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                DestinationAirports = destinationAirportCode.getText().toString();
//                showDestinationAirportlist(s.toString());
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//            }
//        };
//        destinationAirportCode.addTextChangedListener(textWatcher);
//    }

//    private void showDestinationAirportlist(String airports) {
//
//        recyclerViewDestinationAirports.setLayoutManager(new LinearLayoutManager(activity));
//        ApiService api = ApiClient.getApiClient().create(ApiService.class);
//        api.airportCodeList(country.toUpperCase(), airports).enqueue(new Callback<AirportListModel>() {
//            @Override
//            public void onResponse(Call<AirportListModel> call, Response<AirportListModel> response) {
//                if (response.body() != null) {
//                    airportList.clear();
//                    details.clear();
//                    if (!response.body().getSuccess().equalsIgnoreCase("0")) {
//
//                        details = response.body().getDetails();
//
//
//                        for (AirportListModel.Detail detail : response.body().getDetails()) {
//                            airportList.add(detail.getCode());
//                        }
//                        recyclerViewDestinationAirports.setVisibility(View.VISIBLE);
//                        recyclerViewDestinationAirports.setAdapter(new LocalAirportListAdapter(activity, airportList, new LocalAirportListAdapter.CallbackForAirports() {
//                            @Override
//                            public void callbackForAirports(int i) {
//                                recyclerViewDestinationAirports.setVisibility(GONE);
//
//                                String airport = airportList.get(i);
//                                String city = details.get(i).getCityName();
//
//                                destinationAirport = airport;
//
//
//                                destinationAirportCode.setText(airport + "(" + city + ")");
//
//                                airportList.clear();
//                                details.clear();
//
//
//                            }
//                        }));
//                    } else {
//
//                        airportList.clear();
//                        recyclerViewLocalAirports.setVisibility(GONE);
//                        details.clear();
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<AirportListModel> call, Throwable t) {
//                airportList.clear();
//
//                recyclerViewDestinationAirports.setVisibility(GONE);
//            }
//        });
//    }

//    private void showLocalAirportlist(final String airports) {
//
//        recyclerViewLocalAirports.setLayoutManager(new LinearLayoutManager(activity));
//        ApiService api = ApiClient.getApiClient().create(ApiService.class);
//        api.airportCodeList(country.toUpperCase(), airports).enqueue(new Callback<AirportListModel>() {
//            @Override
//            public void onResponse(Call<AirportListModel> call, Response<AirportListModel> response) {
//
//                if (response.body() != null) {
//                    airportList.clear();
//                    details.clear();
//                    if (!response.body().getSuccess().equalsIgnoreCase("0")) {
//
//                        details = response.body().getDetails();
//
//                        for (AirportListModel.Detail detail : response.body().getDetails()) {
//                            airportList.add(detail.getCode());
//                        }
//                        recyclerViewLocalAirports.setVisibility(View.VISIBLE);
//                        recyclerViewLocalAirports.setAdapter(new LocalAirportListAdapter(activity, airportList, new LocalAirportListAdapter.CallbackForAirports() {
//
//                            @Override
//                            public void callbackForAirports(int i) {
//                                recyclerViewLocalAirports.setVisibility(GONE);
//
//                                String airport = airportList.get(i);
//                                String city = details.get(i).getCityName();
//
//                                localAirport = airport;
//
//                                localAirportCode.setText(airport + "(" + city + ")");
//
//                                airportList.clear();
//                                details.clear();
//
//                            }
//                        }));
//                    } else {
//                        airportList.clear();
//                        details.clear();
//                        recyclerViewLocalAirports.setVisibility(GONE);
//                    }
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<AirportListModel> call, Throwable t) {
//
//                airportList.clear();
//
//                recyclerViewLocalAirports.setVisibility(GONE);
//            }
//        });
//
//    }

}
