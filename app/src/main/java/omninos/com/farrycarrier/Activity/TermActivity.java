package omninos.com.farrycarrier.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.util.CommonUtils;

public class TermActivity extends AppCompatActivity implements View.OnClickListener {

    private Switch switch1;
    private Activity activity;
    private ImageView backbtn;
    private Button btn_continue;
    private String checkString;
    private WebView webWew;
    ProgressDialog pd;
    private TextView textData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_);

        findids();
    }

    private void findids() {
        activity = TermActivity.this;
        backbtn = findViewById(R.id.backbtn);
        switch1 = findViewById(R.id.switch1);
        btn_continue = findViewById(R.id.btn_continue);
        webWew = findViewById(R.id.webWew);
        textData = findViewById(R.id.textData);

        pd = new ProgressDialog(TermActivity.this);
        pd.setMessage("Loading...");
        pd.show();
        WebSettings webSettings = webWew.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webWew.setWebViewClient(new MyWebViewClient());
//        http://www.omninos.in/laybay/
        webWew.loadUrl("http://infosif.in/ferryApplication/index.php/api/User/termsAndCondition");


        //click listeners
        backbtn.setOnClickListener(this);
        btn_continue.setOnClickListener(this);
        if (getIntent().getStringExtra("Status").equalsIgnoreCase("2")) {
            btn_continue.setText("Back");
            switch1.setVisibility(View.GONE);
            textData.setVisibility(View.GONE);
        }

    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);

            if (!pd.isShowing()) {
                pd.show();
            }

            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            System.out.println("on finish");
            if (pd.isShowing()) {
                pd.dismiss();
            }

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_continue:
                if (getIntent().getStringExtra("Status").equalsIgnoreCase("2")) {
                    onBackPressed();
                } else {
                    if (!switch1.isChecked()) {
                        CommonUtils.showSnackbarAlert(btn_continue, "Agree to the terms!");
                    } else {
                        check();
                    }
                }
                break;
            case R.id.backbtn:
                onBackPressed();
                break;
        }

    }

    private void check() {
        if (getIntent().getStringExtra("Status").equalsIgnoreCase("0")) {
            finish();
        } else if (getIntent().getStringExtra("Status").equalsIgnoreCase("1")) {
            Intent intent = new Intent(TermActivity.this, SelectTypeActivity.class);
            startActivity(intent);
            finishAffinity();
        }
    }
}
