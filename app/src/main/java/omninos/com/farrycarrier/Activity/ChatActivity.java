package omninos.com.farrycarrier.activity;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;
import omninos.com.farrycarrier.adapter.InboxAdapter;
import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.util.App;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.viewmodels.ConversationVM;
import omninos.com.farrycarrier.viewmodels.SendMessageVM;
import omninos.com.farrycarrier.model.ConversationModel;
import omninos.com.farrycarrier.model.sender.SendMessageModel;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener {

    private Activity activity;
    private CircleImageView profileAppbar;
    private ImageView BackButton, TrackButton, sendMessageButton;
    private TextView tvName;
    private String reciverID, Message;
    private RecyclerView recyclerViewInbox;
    private ConversationVM conversationVM;
    private List<ConversationModel.MessageDetail> conversationList = new ArrayList<>();
    private EditText etSendMessage;
    private String senderId, receiverId, name;
    private SendMessageVM sendMessageVM;
    private Timer timer=new Timer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_);
        conversationVM = ViewModelProviders.of(this).get(ConversationVM.class);
        sendMessageVM = ViewModelProviders.of(this).get(SendMessageVM.class);

        reciverID = getIntent().getStringExtra("reciverId");
        name = getIntent().getStringExtra("name");

        findids();
        setUps();
        initTimer();

    }

    private void setUps() {
        BackButton.setOnClickListener(this);
        TrackButton.setOnClickListener(this);
        sendMessageButton.setOnClickListener(this);
        recyclerViewInbox.setLayoutManager(new LinearLayoutManager(activity));

    }

    private void findids() {
        activity = ChatActivity.this;
        BackButton = findViewById(R.id.back_btn);
        TrackButton = findViewById(R.id.track_btn);
        tvName = findViewById(R.id.tv_name);
        tvName.setText(name);
        etSendMessage = findViewById(R.id.et_message);
        sendMessageButton = findViewById(R.id.sendInbox);
        recyclerViewInbox = findViewById(R.id.recyclerview_inbox);
    }

    private void getConversationApi() {
        senderId = App.getAppPreference().GetString(ConstantData.USERID);
        conversationVM.userConversation(activity, senderId, reciverID).observe(this, new Observer<ConversationModel>() {
            @Override
            public void onChanged(@Nullable ConversationModel conversationModel) {
                if (conversationModel.getSuccess().equalsIgnoreCase("1")) {
                    conversationList = conversationModel.getMessageDetails();
                    recyclerViewInbox.setAdapter(new InboxAdapter(activity, conversationList));
                    recyclerViewInbox.scrollToPosition(conversationList.size() - 1);

                } else {
                    Toast.makeText(activity, conversationModel.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.back_btn:
                onBackPressed();
                break;

            case R.id.track_btn:
                startActivity(new Intent(activity, MapActivity.class));
                break;

            case R.id.appbarPayment:
                startActivity(new Intent(activity, PaymentActivity.class));
                break;

            case R.id.sendInbox:
                validation();
                break;

        }

    }

    private void validation() {
        Message = etSendMessage.getText().toString();

        if (!Message.isEmpty()) {
            getSendMessageApi();
        }
    }

    private void getSendMessageApi() {
        senderId = reciverID;
        receiverId = App.getAppPreference().GetString(ConstantData.USERID);

        sendMessageVM.sendMessage(activity, senderId, receiverId, Message).observe(this, new Observer<SendMessageModel>() {
            @Override
            public void onChanged(@Nullable SendMessageModel sendMessageModel) {
                if (sendMessageModel.getSuccess().equalsIgnoreCase("1")) {
                    etSendMessage.setText("");
                    getConversationApi();
                } else {
                    Toast.makeText(activity, sendMessageModel.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void initTimer() {
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getConversationApi();
                    }
                });

            }
        }, 0, 5000);

    }

}
