package omninos.com.farrycarrier.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.adapter.CarrierListAdapter;
import omninos.com.farrycarrier.model.CarrierInfoList;
import omninos.com.farrycarrier.util.App;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.viewmodels.CancelPackageViewModel;

public class CarrierInfoListActivity extends AppCompatActivity {

    private RecyclerView recycler_view;
    private ImageView back_btn;
    private CarrierListAdapter adapter;
    private CancelPackageViewModel viewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carrier_info_list);

        viewModel = ViewModelProviders.of(this).get(CancelPackageViewModel.class);

        initView();
        SetUp();
    }

    private void initView() {
        recycler_view = findViewById(R.id.recycler_view);
        back_btn = findViewById(R.id.back_btn);
    }

    private void SetUp() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler_view.setLayoutManager(linearLayoutManager);


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        getList();
    }

    private void getList() {
        viewModel.carrierInfoListLiveData(CarrierInfoListActivity.this, App.getAppPreference().GetString(ConstantData.USERID)).observe(CarrierInfoListActivity.this, new Observer<CarrierInfoList>() {
            @Override
            public void onChanged(@Nullable CarrierInfoList carrierInfoList) {
                if (carrierInfoList.getSuccess().equalsIgnoreCase("1")) {
                    adapter = new CarrierListAdapter(CarrierInfoListActivity.this, carrierInfoList.getDetails());
                    recycler_view.setAdapter(adapter);
                } else {
                    Toast.makeText(CarrierInfoListActivity.this, carrierInfoList.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
