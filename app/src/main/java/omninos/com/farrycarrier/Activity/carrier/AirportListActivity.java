package omninos.com.farrycarrier.activity.carrier;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.adapter.LocalAirportListAdapter;
import omninos.com.farrycarrier.model.AirportListModel;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

public class AirportListActivity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView recyclerViewAirportList;
    private ImageView ivBackButton;
    private List<AirportListModel.Detail> details = new ArrayList<>();
    final List<String> airportList = new ArrayList<>();
    private String selectedCountry,localAirport, destinationAirport, LocalAirports;
    private EditText etAirport;
    private ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_airport_list);
        findIds();
        setUps();
        selectedCountry = getIntent().getStringExtra("country");
        getLocalAirport();
        showLocalAirportlist("");

    }

    private void findIds() {
        recyclerViewAirportList = findViewById(R.id.recyclerview_airport_list);
        etAirport = findViewById(R.id.et_airport);
        ivBackButton = findViewById(R.id.back_btn);
        progressBar = findViewById(R.id.progress_bar);
    }

    private void setUps() {
        ivBackButton.setOnClickListener(this);
        recyclerViewAirportList.addItemDecoration(new DividerItemDecoration(AirportListActivity.this, LinearLayoutManager.VERTICAL));
    }

    private void getLocalAirport() {
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                LocalAirports = etAirport.getText().toString();
                showLocalAirportlist(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        etAirport.addTextChangedListener(textWatcher);

    }

    private void showLocalAirportlist(final String airports) {

        recyclerViewAirportList.setLayoutManager(new LinearLayoutManager(AirportListActivity.this));
        ApiService api = ApiClient.getApiClient().create(ApiService.class);
        api.airportCodeList(selectedCountry.toUpperCase(), airports).enqueue(new Callback<AirportListModel>() {
            @Override
            public void onResponse(Call<AirportListModel> call, Response<AirportListModel> response) {
                if (response.body() != null) {
                    airportList.clear();
                    details.clear();
                    if (!response.body().getSuccess().equalsIgnoreCase("0")) {

                        details = response.body().getDetails();

                        for (AirportListModel.Detail detail : response.body().getDetails()) {
                            airportList.add(detail.getCode());
                        }
                        recyclerViewAirportList.setVisibility(View.VISIBLE);
                        recyclerViewAirportList.setAdapter(new LocalAirportListAdapter(AirportListActivity.this, airportList, new LocalAirportListAdapter.CallbackForAirports() {

                            @Override
                            public void callbackForAirports(int i) {
                                recyclerViewAirportList.setVisibility(GONE);
                                //getting airport code and city name
                                String airport = airportList.get(i);
                                String city = details.get(i).getCityName();

                                localAirport = airport;
                                Intent returnIntent = new Intent();
                                returnIntent.putExtra("airport", localAirport);
                                returnIntent.putExtra("city",city);
                                setResult(Activity.RESULT_OK, returnIntent);

                                finish();
                                airportList.clear();
                                details.clear();
                            }
                        }));
                        progressBar.setVisibility(GONE);
                    } else {
                        airportList.clear();
                        details.clear();
                        recyclerViewAirportList.setVisibility(GONE);
                        progressBar.setVisibility(GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<AirportListModel> call, Throwable t) {
                airportList.clear();
                recyclerViewAirportList.setVisibility(GONE);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                finish();
                break;
        }
    }
}
