package omninos.com.farrycarrier.activity;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.hbb20.CountryCodePicker;

import omninos.com.farrycarrier.model.SendOTPModel;
import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.viewmodels.SendOtpVM;

public class VerificationActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn_next;
    private Activity activity;
    private ImageView backbtn;
    private SendOtpVM sendOtpVM;
    private EditText etPhoneNumber;
    private String name, email, password, countyCode, lastName;
    private CountryCodePicker ccp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_);
        sendOtpVM = ViewModelProviders.of(this).get(SendOtpVM.class);
        activity = VerificationActivity.this;

        findids();

        //data through intent
        Intent intent = getIntent();
        name = intent.getStringExtra("username");
        email = intent.getStringExtra("useremail");
        password = intent.getStringExtra("userpassword");
        lastName = intent.getStringExtra("lastName");
    }

    private void findids() {
        activity = VerificationActivity.this;
        etPhoneNumber = findViewById(R.id.etPhoneNumber);
        btn_next = findViewById(R.id.btn_next);
        backbtn = findViewById(R.id.backbtn);
        ccp = findViewById(R.id.ccp);
        countyCode = ccp.getSelectedCountryCodeWithPlus();

        //click Listeners
        btn_next.setOnClickListener(this);
        backbtn.setOnClickListener(this);
        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                countyCode = ccp.getSelectedCountryCodeWithPlus();
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
                validate();
                break;

            case R.id.backbtn:
                onBackPressed();
                break;
        }

    }

    private void validate() {
        String phone = etPhoneNumber.getText().toString();
        if (TextUtils.isEmpty(phone))
            CommonUtils.showSnackbarAlert(etPhoneNumber, "Fill Mandatory Fields");
        else if (phone.length() < 10)
            CommonUtils.showSnackbarAlert(etPhoneNumber, "Minimum 10 digits required");
        else
            checkPhonenumber(countyCode + phone);
    }

    private void checkPhonenumber(final String phone) {
        sendOtpVM.checkphonenumber(activity, phone).observe(this, new Observer<SendOTPModel>() {
            @Override
            public void onChanged(@Nullable SendOTPModel sendOTPModel) {
                if (sendOTPModel.getSuccess().equalsIgnoreCase("1")) {
                    String otpvalue = sendOTPModel.getDetails().toString();
                    Toast.makeText(activity, otpvalue, Toast.LENGTH_LONG).show();
                    Intent otpintent = new Intent(VerificationActivity.this, VerificationCodeActivity.class);
                    otpintent.putExtra("userphonenumber", phone);
                    otpintent.putExtra("username", name);
                    otpintent.putExtra("useremail", email);
                    otpintent.putExtra("userpassword", password);
                    otpintent.putExtra("otp", otpvalue);
                    otpintent.putExtra("lastName", lastName);
                    startActivity(otpintent);
                } else {
                    CommonUtils.showSnackbarAlert(etPhoneNumber, sendOTPModel.getMessage());
                }
            }
        });
    }
}
