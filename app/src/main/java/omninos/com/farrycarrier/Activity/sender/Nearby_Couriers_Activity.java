package omninos.com.farrycarrier.activity.sender;

import android.app.Activity;
import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;

import java.util.ArrayList;
import java.util.List;

import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.activity.WalletActivity;
import omninos.com.farrycarrier.adapter.sender.NearbyCarrierListAdapter;
import omninos.com.farrycarrier.model.WalletBalanceModel;
import omninos.com.farrycarrier.model.sender.NearbyCarrierListModel;
import omninos.com.farrycarrier.model.sender.RecieverInformationModel;
import omninos.com.farrycarrier.util.App;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.util.Constants;
import omninos.com.farrycarrier.viewmodels.CarrierBookingVM;
import omninos.com.farrycarrier.viewmodels.NearbyCarrierListVM;
import omninos.com.farrycarrier.viewmodels.RecieverInformationVM;
import omninos.com.farrycarrier.viewmodels.WalletBalanceVM;

public class Nearby_Couriers_Activity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView senderRC;
    private ImageView appbarBack;
    private TextView appbarText, tvMessage;
    private String checkString, id, CarrierID, CarrierInfoID, senderInformationId, carrierInformationId, senderId, carrierId, walletBalance;
    private Activity activity;
    private RecieverInformationVM recieverInformationVM;
    private CarrierBookingVM carrierBookingVM;
    private List<RecieverInformationModel.Detail> recieverInformationModalList = new ArrayList<>();
    private NearbyCarrierListVM nearbyCarrierListVM;

    private WalletBalanceVM walletBalanceVM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby__couriers_);

        checkString = getIntent().getExtras().getString(Constants.key);
        walletBalanceVM = ViewModelProviders.of(this).get(WalletBalanceVM.class);

        findids();
        setUps();
    }

    private void setUps() {
        recieverInformationVM = ViewModelProviders.of(this).get(RecieverInformationVM.class);
        carrierBookingVM = ViewModelProviders.of(this).get(CarrierBookingVM.class);
        nearbyCarrierListVM = ViewModelProviders.of(this).get(NearbyCarrierListVM.class);

        senderRC.setLayoutManager(new LinearLayoutManager(activity));
        appbarBack.setVisibility(View.VISIBLE);
        appbarBack.setOnClickListener(this);
        appbarText.setText("Available Couriers");

        id = App.getAppPreference().GetString(ConstantData.USERID);

        recieverInformationModalList = App.getSinltonPojo().getDetailsList();
        senderRC.setAdapter(new NearbyCarrierListAdapter(activity, recieverInformationModalList, new NearbyCarrierListAdapter.ClickListner() {
            @Override
            public void Onclick(int position) {
                senderInformationId = recieverInformationModalList.get(position).getSenderInformationId();
                CarrierID = recieverInformationModalList.get(position).getUserId();
                CarrierInfoID = recieverInformationModalList.get(position).getId();
                getWalletBalanceApi(CarrierID, CarrierInfoID);
//
            }
        }));
    }

    private void getWalletBalanceApi(String carrierID, String carrierInfoID) {
        walletBalanceVM.walletBalance(activity, App.getAppPreference().GetString(ConstantData.USERID)).observe(this, new Observer<WalletBalanceModel>() {
            @Override
            public void onChanged(@Nullable WalletBalanceModel walletBalanceModel) {
                if (walletBalanceModel.getSuccess().equalsIgnoreCase("1")) {
                    walletBalance = walletBalanceModel.getWalletBalance();
                    if (Integer.parseInt(walletBalance) <= 65) {
                        NofifyUser();
//                        CommonUtils.showSnackbarAlert(tvdeliveryDate, "Insufficient Wallet Balance");
                    } else {
                        getPendingRequestInfoApi();
//                        Intent i1 = new Intent(activity, Nearby_Couriers_Activity.class);
//                        i1.putExtra(Constants.key, checkString);
//                        startActivity(i1);

                    }
                } else {
                    NofifyUser();
                }
            }
        });
    }

    private void NofifyUser() {
        final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder.getInstance(this);

        dialogBuilder
                .withTitle("Wallet Balance")
                .withMessage("You have Insufficient Wallet Balance")
                .withButton1Text("Load Amount")
                .withButton2Text("Cancel")
                .setButton1Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogBuilder.dismiss();
                        Intent i1 = new Intent(activity, WalletActivity.class);
                        i1.putExtra("Type", "1");
                        startActivity(i1);
                    }
                })
                .setButton2Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogBuilder.dismiss();
                    }
                })
                .isCancelableOnTouchOutside(false)
                .withDialogColor("#FFFFFF")
                .withTitleColor("#000000")
                .withMessageColor("#000000")
                .show();
    }

    private void findids() {
        activity = Nearby_Couriers_Activity.this;
        //recyclerview
        senderRC = findViewById(R.id.senderRC);
        //text views
        tvMessage = findViewById(R.id.tv_nodata);
        //app bars
        appbarBack = findViewById(R.id.appbarBack);
        appbarText = findViewById(R.id.appbarText);
    }


//    private void getRecieverInformationApi() {
//        recieverInformationVM.recieverInfo(activity, id).observe(this, new Observer<RecieverInformationModel>() {
//            @Override
//            public void onChanged(@Nullable final RecieverInformationModel recieverInformationModel) {
//                if (recieverInformationModel.getSuccess().equalsIgnoreCase("1")) {
//                    recieverInformationModalList = recieverInformationModel.getDetails();
//
//                } else {
//                    tvMessage.setText(recieverInformationModel.getMessage());
//                }
//            }
//        });
//
//
//    }


    private void getPendingRequestInfoApi() {

        carrierInformationId = CarrierInfoID;
        senderId = App.getAppPreference().GetString(ConstantData.USERID);
        carrierId = CarrierID;
        nearbyCarrierListVM.nearbycarrierlist(activity, senderInformationId, carrierInformationId, senderId, carrierId, "userBookingServices").observe(this, new Observer<NearbyCarrierListModel>() {
            @Override
            public void onChanged(@Nullable NearbyCarrierListModel nearbyCarrierListModel) {
                if (nearbyCarrierListModel.getSuccess().equalsIgnoreCase("1")) {
                    App.getAppPreference().SaveString(ConstantData.Type, "userBookingServices");
                    dialogVerifyCode();
                } else
                    CommonUtils.showSnackbarAlert(appbarBack, "No Couriers Found!");
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.appbarBack:
                onBackPressed();
                break;
        }

    }

    private void dialogVerifyCode() {
        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.popup_request);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button continueButton = dialog.findViewById(R.id.btn_continue);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                activity.startActivity(new Intent(activity, HomeSenderActivity.class));
                finishAffinity();
            }
        });
        dialog.show();
    }
}
