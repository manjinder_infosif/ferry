package omninos.com.farrycarrier.activity;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;

import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.viewmodels.PayPalPaymentVM;
import omninos.com.farrycarrier.model.GenerateTokenModel;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView backbtn;
    private Button btn_Paymnt;
    private Activity activity;
    private RelativeLayout paypalRL;
    private String Type;
    private int paymentType = 0;
    String token, ammount;
    private int REQUEST_OUT = 1234;
    private PayPalPaymentVM payPalPaymentVM;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_);
        payPalPaymentVM= ViewModelProviders.of(this).get(PayPalPaymentVM.class);

        findIds();
    }


    private void findIds() {
        activity = PaymentActivity.this;

        backbtn = findViewById(R.id.backbtn);
        backbtn.setOnClickListener(this);

        btn_Paymnt = findViewById(R.id.btn_Paymnt);
        btn_Paymnt.setOnClickListener(this);

        paypalRL = findViewById(R.id.paypalRL);
        paypalRL.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.backbtn:
                onBackPressed();
                break;

            case R.id.paypalRL:
                paymentType = 2;
                paypalRL.setPadding(5, 5, 5, 5);
                paypalRL.setBackgroundColor(getResources().getColor(R.color.grey));
                break;

            case R.id.btn_Paymnt:
                PayPalPayment();
                break;
        }
    }



    private void PayPalPayment() {
        ApiService api = ApiClient.getApiClient().create(ApiService.class);
        CommonUtils.showProgress(activity, "");
        api.generatetoken().enqueue(new Callback<GenerateTokenModel>() {
            @Override
            public void onResponse(Call<GenerateTokenModel> call, Response<GenerateTokenModel> response) {
                CommonUtils.dismissProgress();
                token = response.body().getDetails();
                submitPayment(token);
            }

            @Override
            public void onFailure(Call<GenerateTokenModel> call, Throwable t) {
                CommonUtils.dismissProgress();
                Toast.makeText(activity, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void submitPayment(String token) {
        DropInRequest dropInRequest = new DropInRequest().clientToken(token);
        startActivityForResult(dropInRequest.getIntent(this), REQUEST_OUT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_OUT) {
            try {
                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                String nonces = result.getPaymentMethodNonce().getNonce();
//                if (Type.equalsIgnoreCase("Product")) {
////                    BookProduct(nonces);
//                } else {
////                    BookingServices(nonces);
//                }
//                Toast.makeText(this, nonces, Toast.LENGTH_SHORT).show();
            }catch (Exception e){

            }

        } else if (requestCode == RESULT_CANCELED) {
            Toast.makeText(this, "User cancel", Toast.LENGTH_SHORT).show();
        } else {
            Exception exception = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);

            Toast.makeText(this, exception.toString(), Toast.LENGTH_SHORT).show();
        }

    }


}
