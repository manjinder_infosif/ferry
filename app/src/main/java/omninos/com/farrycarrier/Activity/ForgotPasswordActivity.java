package omninos.com.farrycarrier.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.viewmodels.ForgotPasswordVM;
import omninos.com.farrycarrier.model.ForgotPasswordModel;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView backbutton;
    private Button sendButton;
    private ForgotPasswordVM forgotPasswordVM;
    private EditText editTextEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        forgotPasswordVM = ViewModelProviders.of(this).get(ForgotPasswordVM.class);
        findIDs();
        setUps();
    }


    private void findIDs() {
        backbutton = findViewById(R.id.back_btn);
        sendButton = findViewById(R.id.btn_send);
        editTextEmail = findViewById(R.id.edmail);
    }

    private void setUps() {
        backbutton.setOnClickListener(this);
        sendButton.setOnClickListener(this);
        editTextEmail.setText(getIntent().getStringExtra("Email"));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_send:
                validate();
                break;

            case R.id.back_btn:
                finish();
                break;
        }
    }

    private void validate() {
        String email = editTextEmail.getText().toString();
        if (TextUtils.isEmpty(email))
            CommonUtils.showSnackbarAlert(editTextEmail, ConstantData.Fill_DETAILS);
        else if (!isValidEmail(email))
            CommonUtils.showSnackbarAlert(editTextEmail, "Please Enter a valid Email");
        else
            sendEmail(email);
    }

    private void sendEmail(String email) {
        forgotPasswordVM.forgotPassword(ForgotPasswordActivity.this, email).observe(this, new Observer<ForgotPasswordModel>() {
            @Override
            public void onChanged(@Nullable ForgotPasswordModel forgotPasswordModel) {
                if (forgotPasswordModel.getSuccess().equalsIgnoreCase("1")) {
                    Toast.makeText(ForgotPasswordActivity.this, forgotPasswordModel.getMessage(), Toast.LENGTH_LONG).show();
                    finish();
                } else
                    CommonUtils.showSnackbarAlert(editTextEmail, forgotPasswordModel.getMessage());

            }
        });
    }

    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
