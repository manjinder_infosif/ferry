package omninos.com.farrycarrier.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import omninos.com.farrycarrier.activity.sender.SendPackageActivity;
import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.util.Constants;

public class Document_Verification_Activity extends AppCompatActivity implements View.OnClickListener{

    private Activity activity;
    private ImageView backbtn;
    private Button btn_submit;
    private String checkString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document__verification_);
        Intent intent = getIntent();
        checkString = intent.getExtras().getString(Constants.key);

        findids();
    }

    private void findids() {

        activity=Document_Verification_Activity.this;

        backbtn=findViewById(R.id.backbtn);
        backbtn.setOnClickListener(this);


        btn_submit=findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_submit:

                if (checkString.equals(Constants.sender))
                {
                    Intent i1 = new Intent(this, SendPackageActivity.class);
                    i1.putExtra(Constants.key, checkString);
                    startActivity(i1);

                }
                break;

            case R.id.backbtn:
                onBackPressed();
                break;
        }

    }


}
