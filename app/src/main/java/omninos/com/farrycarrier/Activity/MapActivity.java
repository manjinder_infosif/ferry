package omninos.com.farrycarrier.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.util.App;
import omninos.com.farrycarrier.util.ConstantData;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener, LocationListener, View.OnClickListener {

    public static Location location;
    GoogleMap gMap;
    Marker marker, marker1;
    LatLng latLng1;
    LocationManager locationManager;
    private ArrayList<LatLng> pathPoints;
    private ImageView gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_);

        gps = findViewById(R.id.gps);
        gps.setOnClickListener(this);
        pathPoints = new ArrayList<>();

        pathPoints.add(new LatLng(Double.parseDouble(App.getAppPreference().GetString(ConstantData.CURRENT_LAT)),
                Double.parseDouble(App.getAppPreference().GetString(ConstantData.CURRENT_LONG))));

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        // TODO: Consider calling
        // ActivityCompat#requestPermissions
        // here to request the missing permissions, and then overriding
        // public void onRequestPermissionsResult(int requestCode, String[] permissions,
        // int[] grantResults)
        // to handle the case where the user grants the permission. See the documentation
        // for ActivityCompat#requestPermissions for more details.
            return;
        }
        setMarker();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.gps:
                setMarker();
        }

    }

    private void setMarker() {
        gMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(App.getAppPreference()
                .GetString(ConstantData.CURRENT_LAT)), Double.parseDouble(App.getAppPreference()
                .GetString(ConstantData.CURRENT_LONG))))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(pathPoints.get(0), 15);
        gMap.animateCamera(location);
    }
}

