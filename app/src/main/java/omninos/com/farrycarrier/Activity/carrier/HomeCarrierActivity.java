package omninos.com.farrycarrier.activity.carrier;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.activity.AboutActivity;
import omninos.com.farrycarrier.activity.AboutAppActivity;
import omninos.com.farrycarrier.activity.AccountDetailActivity;
import omninos.com.farrycarrier.activity.CarrierInfoListActivity;
import omninos.com.farrycarrier.activity.LoginActivity;
import omninos.com.farrycarrier.activity.SettingsActivity;
import omninos.com.farrycarrier.activity.WalletActivity;
import omninos.com.farrycarrier.fragment.CarrierCurrentPackageFragment;
import omninos.com.farrycarrier.fragment.CarrierPastPackageFragment;
import omninos.com.farrycarrier.util.App;
import omninos.com.farrycarrier.util.ConstantData;

public class HomeCarrierActivity extends AppCompatActivity implements View.OnClickListener, TabLayout.OnTabSelectedListener {
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ImageView OpenDrawer;
    private TabLayout tabLayout;
    private ViewPager viewPagerPackages;
    private String[] string = {"Current Packages", "Past Packages"};
    private String status, bookingId, name;
    private TextView textViewName;
    private CircleImageView profilePic;
    GoogleSignInClient mGoogleSignInClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_carrier);
//        status = getIntent().getStringExtra("status");
        bookingId = getIntent().getStringExtra("bookingid");

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        findIds();
        onclicks();
        setUps();
    }

    private void setUps() {
        setupViewPager(viewPagerPackages);
        tabLayout.setupWithViewPager(viewPagerPackages);

        name = App.getAppPreference().GetString(ConstantData.Name);
        textViewName.setText(name);

        if (App.getAppPreference().GetString(ConstantData.IMAGEPATH).isEmpty()) {
            profilePic.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile_placeholder));
        } else {
            Glide.with(HomeCarrierActivity.this).load(App.getAppPreference().GetString(ConstantData.IMAGEPATH)).into(profilePic);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (App.getAppPreference().GetString(ConstantData.IMAGEPATH).isEmpty()) {
            profilePic.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile_placeholder));
        } else {
            Glide.with(HomeCarrierActivity.this).load(App.getAppPreference().GetString(ConstantData.IMAGEPATH)).into(profilePic);
        }
        name = App.getAppPreference().GetString(ConstantData.Name);
        textViewName.setText(name);
    }

    private void findIds() {
        drawerLayout = findViewById(R.id.drawer_layout_carrier);
        navigationView = findViewById(R.id.nav_view_carrier);
        OpenDrawer = findViewById(R.id.open_menu);
        tabLayout = findViewById(R.id.tab_layout);
        viewPagerPackages = findViewById(R.id.view_pager_packages);

        View headerView = navigationView.getHeaderView(0);
        textViewName = headerView.findViewById(R.id.tv_username);
        profilePic = headerView.findViewById(R.id.profilepic);

    }

    private void setupViewPager(ViewPager viewPagerPackages) {
        HomeCarrierActivity.ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        CarrierCurrentPackageFragment carrierCurrentPackageFragment = new CarrierCurrentPackageFragment();
        adapter.addFragment(carrierCurrentPackageFragment, string[0]);
        Bundle bundle = new Bundle();
        bundle.putString("bookingID", bookingId);

        carrierCurrentPackageFragment.setArguments(bundle);
        adapter.addFragment(new CarrierPastPackageFragment(), string[1]);

        viewPagerPackages.setAdapter(adapter);
    }

    private void onclicks() {
        OpenDrawer.setOnClickListener(this);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                switch (menuItem.getItemId()) {
                    case R.id.myprofile:
                        Intent profile = new Intent(HomeCarrierActivity.this, CarrierProfileActivity.class);
                        startActivity(profile);
                        break;

                    case R.id.recievepackage:
                        Intent intent = new Intent(HomeCarrierActivity.this, CarryPackageActivity.class);
                        startActivity(intent);
                        break;

                    case R.id.pendingrequests:
                        Intent requests = new Intent(HomeCarrierActivity.this, PendingRequestActivity.class);
                        startActivity(requests);
                        break;

                    case R.id.wallet:
                        Intent wallet = new Intent(HomeCarrierActivity.this, WalletActivity.class);
                        wallet.putExtra("Type", "0");
                        startActivity(wallet);
                        break;

                    case R.id.about:
                        Intent about = new Intent(HomeCarrierActivity.this, AboutActivity.class);
                        startActivity(about);
                        break;

                    case R.id.settings:
                        Intent settings = new Intent(HomeCarrierActivity.this, SettingsActivity.class);
                        startActivity(settings);
                        break;

                    case R.id.logout:
                        mGoogleSignInClient.signOut();
                        disconnectFromFacebook();
                        App.getAppPreference().Logout(HomeCarrierActivity.this);
                        Intent login = new Intent(HomeCarrierActivity.this, LoginActivity.class);
                        startActivity(login);
                        finish();
                        break;
                    case R.id.account:
                        startActivity(new Intent(HomeCarrierActivity.this, AccountDetailActivity.class));
                        break;

                    case R.id.aboutApp:
                        startActivity(new Intent(HomeCarrierActivity.this, AboutAppActivity.class));
                        break;
                    case R.id.packagehistory:
                        startActivity(new Intent(HomeCarrierActivity.this, CarrierInfoListActivity.class));
                        break;
                }
                return false;
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.open_menu:
                drawerLayout.openDrawer(GravityCompat.START);
                break;
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPagerPackages.setCurrentItem(tab.getPosition());


    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmenttitle = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmenttitle.add(title);
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return fragmenttitle.get(position);
        }
    }

    public void disconnectFromFacebook() {

        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {

                LoginManager.getInstance().logOut();

            }
        }).executeAsync();
    }

}
