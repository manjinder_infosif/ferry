package omninos.com.farrycarrier.activity;

import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Map;

import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.util.App;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.viewmodels.AccountDetailsViewModel;

public class AccountDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText et_account_number, et_account_name, et_ifsc_code, et_branch_name, et_routing_number,et_payapl_userName;
    private String accName = "", accNumber = "", accIFSC = "", accBranch = "", routingNumber = "",username;
    private ImageView back_btn;
    private Button submit_detail;
    private AccountDetailsViewModel viewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_detail);

        viewModel = ViewModelProviders.of(this).get(AccountDetailsViewModel.class);

        initView();
        SetUp();
    }

    private void initView() {
        et_account_number = findViewById(R.id.et_account_number);
        et_account_name = findViewById(R.id.et_account_name);
        et_ifsc_code = findViewById(R.id.et_ifsc_code);
        et_branch_name = findViewById(R.id.et_branch_name);
        et_routing_number = findViewById(R.id.et_routing_number);
        et_payapl_userName=findViewById(R.id.et_payapl_userName);

        back_btn = findViewById(R.id.back_btn);
        submit_detail = findViewById(R.id.submit_detail);

    }

    private void SetUp() {
        back_btn.setOnClickListener(this);
        submit_detail.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submit_detail:
                Validate();
                break;
            case R.id.back_btn:
                onBackPressed();
                break;
        }
    }

    private void Validate() {
        accName = et_account_name.getText().toString();
        accNumber = et_account_number.getText().toString();
        accIFSC = et_ifsc_code.getText().toString();
        accBranch = et_branch_name.getText().toString();
        routingNumber = et_routing_number.getText().toString();
        username=et_payapl_userName.getText().toString();

        if (accName.isEmpty()) {
            CommonUtils.showSnackbarAlert(et_account_name, "enter account holder name");
        } else if (accNumber.isEmpty()) {
            CommonUtils.showSnackbarAlert(et_account_name, "enter account holder number");

        } else if (accBranch.isEmpty()) {
            CommonUtils.showSnackbarAlert(et_account_name, "enter branch");
//        } else if (accIFSC.isEmpty()) {
//            CommonUtils.showSnackbarAlert(et_account_name, "enter ifsc code");
        } else if (routingNumber.isEmpty()) {
            CommonUtils.showSnackbarAlert(et_account_name, "enter routing number");
        }else if (username.isEmpty()){
            CommonUtils.showSnackbarAlert(et_account_name,"enter paypal username");
        }
        else {
            viewModel.Account(AccountDetailActivity.this, App.getAppPreference().GetString(ConstantData.USERID), accNumber, accName, accBranch, accIFSC, routingNumber,username).observe(AccountDetailActivity.this, new Observer<Map>() {
                @Override
                public void onChanged(@Nullable Map map) {
                    if (map.get("success").toString().equalsIgnoreCase("1")) {
                        dialogVerifyCode();
                    } else {

                    }
                }
            });
        }
    }

    private void dialogVerifyCode() {

        final Dialog dialog = new Dialog(AccountDetailActivity.this);
        dialog.setContentView(R.layout.payment_done_dialog);
        dialog.setCancelable(false);
        TextView messageData = dialog.findViewById(R.id.messageData);
        messageData.setText("Account Detail Submit Successfully");

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
                onBackPressed();
            }
        }, 3000);

        dialog.show();

    }
}
