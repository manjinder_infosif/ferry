package omninos.com.farrycarrier.activity;

import android.app.Activity;
import android.app.ActivityManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import omninos.com.farrycarrier.model.CheckLoginModel;
import omninos.com.farrycarrier.model.SocialLoginModel;
import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.util.App;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.util.LocationService;
import omninos.com.farrycarrier.viewmodels.CheckLoginVM;
import omninos.com.farrycarrier.viewmodels.SocialLoginVM;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn_signin, googlebtn, facebookbtn;
    private TextView noAcRegister, tvForgotPassword;
    private Activity activity;
    private CheckLoginVM checkLoginVM;
    private EditText edmail, edpassword;
    GoogleSignInClient mGoogleSignInClient;
    GoogleSignInAccount mGoogleSignInAccount;
    private static final int RC_SIGN_IN = 007;
    private SocialLoginVM socialLoginVM;
    private CallbackManager callbackManager;
    private GoogleSignInAccount account;
    private String userid, username, useremail, image;
    private String loginId, fbEmail, fbName, token, email, password, forgotEmail = "";
    private boolean data = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_);
        findids();

        callbackManager = CallbackManager.Factory.create();
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(activity);

        viewmodels();
        googleLogin();
        getToken();
    }

    private void findids() {

        activity = LoginActivity.this;
        //text view and edit texts
        edmail = findViewById(R.id.edmail);
        edpassword = findViewById(R.id.edpassword);
        noAcRegister = findViewById(R.id.noAcRegister);
        tvForgotPassword = findViewById(R.id.tvforgot);
        //buttons
        btn_signin = findViewById(R.id.btn_signin);
        googlebtn = findViewById(R.id.googlebtn);
        facebookbtn = findViewById(R.id.facebookbtn);

        //clickListeners
        tvForgotPassword.setOnClickListener(this);
        btn_signin.setOnClickListener(this);
        googlebtn.setOnClickListener(this);
        facebookbtn.setOnClickListener(this);
        noAcRegister.setOnClickListener(this);

    }

    private void viewmodels() {
        socialLoginVM = ViewModelProviders.of(this).get(SocialLoginVM.class);
        checkLoginVM = ViewModelProviders.of(this).get(CheckLoginVM.class);
    }

    private void googleLogin() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        mGoogleSignInAccount = GoogleSignIn.getLastSignedInAccount(getApplicationContext());

        if (mGoogleSignInAccount != null) {
        }
    }

    private void facebookLogin() {
        LoginManager.getInstance().logInWithReadPermissions(
                activity,
                Arrays.asList( "email", "public_profile"
                ));
        getKeyhash();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                loginId = loginResult.getAccessToken().getUserId();


                GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            if (object.getString("email") != null) {
                                fbEmail = object.getString("email");
                            } else {
                                fbEmail = "";
                            }
                            if (object.getString("first_name") != null || object.getString("last_name") != null) {
                                fbName = object.getString("first_name") + " " + object.getString("last_name");
                            } else {
                                fbName = "";
                            }
                            URL profilePicture = new URL("https://graph.facebook.com/" + loginId + "/picture?width=500&height=500");
                            sociallogin(loginId, fbName,object.getString("last_name"), fbEmail, profilePicture.toString(), "facebook");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,first_name,last_name,email,gender,birthday,location");
                graphRequest.setParameters(parameters);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException error) {
            }
        });
    }

    private void getToken() {
        token = FirebaseInstanceId.getInstance().getToken();
    }

    private void getData() {
        ApiClient.getApiClient();
        ApiService apiService = ApiClient.retrofit.create(ApiService.class);
    }

    private void getKeyhash() {

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "omninos.com.farrycarrier",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.noAcRegister:
                startActivity(new Intent(activity, SignUpActivity.class));
                break;
            case R.id.btn_signin:
                validate();
                break;
            case R.id.googlebtn:
                signin();
                break;
            case R.id.facebookbtn:
                facebookLogin();
                break;
            case R.id.tvforgot:
                startActivity(new Intent(activity, ForgotPasswordActivity.class).putExtra("Email", forgotEmail));
                break;
        }
    }

    private void validate() {
        email = edmail.getText().toString();
        password = edpassword.getText().toString();

        if (TextUtils.isEmpty(email)) {
            CommonUtils.showSnackbarAlert(edmail, "Enter email");
        } else if (TextUtils.isEmpty(password)) {
            CommonUtils.showSnackbarAlert(edmail, "Enter password");
        } else {
            startLocationService();
        }
    }

    private void signin() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {

            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);

        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            account = completedTask.getResult(ApiException.class);
            useremail = account.getEmail();
            userid = account.getId();
            username = account.getDisplayName();
            if (account.getPhotoUrl() != null) {
                image = account.getPhotoUrl().toString();
            } else {
                image = "";
            }
            sociallogin(userid, username,"", useremail, image, "google");
        } catch (ApiException e) {
            Log.e("MyTAG", "signInResult:failed code=" + e.getStatusCode());
            Toast.makeText(activity, "Failed", Toast.LENGTH_LONG);

        }
    }

    private void sociallogin(String userid, String username,String lastName, String useremail, String image, String logintype) {
        socialLoginVM.SocialLogin(activity, userid, username,lastName, useremail, image, "Android", token, logintype).observe(this, new Observer<SocialLoginModel>() {
            @Override
            public void onChanged(@Nullable SocialLoginModel socialLoginModel) {
                if (socialLoginModel.getSuccess().equalsIgnoreCase("1")) {
                    App.getAppPreference().SaveString(ConstantData.TOKEN, "1");
                    App.getAppPreference().SaveString(ConstantData.USERID, socialLoginModel.getDetails().getId());
                    App.getAppPreference().SaveString(ConstantData.Name, socialLoginModel.getDetails().getName());
                    App.getAppPreference().SaveString(ConstantData.IMAGEPATH, socialLoginModel.getDetails().getUserImage());
                    App.getAppPreference().SaveString(ConstantData.Name, socialLoginModel.getDetails().getName());
                    startActivity(new Intent(activity, SelectTypeActivity.class));
                    finishAffinity();
                }
            }
        });
    }


    private void startLocationService() {
        if (!isMyServiceRunning(LocationService.class)) {
            startService(new Intent(activity, LocationService.class));
            checkLoginDetails(email, password);
        } else {
            checkLoginDetails(email, password);
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        assert manager != null;
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void checkLoginDetails(final String email, String password) {
        checkLoginVM.checklogin(activity, email, password, token, "Android",
                App.getAppPreference().GetString(ConstantData.CURRENT_LAT),
                App.getAppPreference().GetString(ConstantData.CURRENT_LONG)).observe(this, new Observer<CheckLoginModel>() {
            @Override
            public void onChanged(@Nullable CheckLoginModel checkLoginModel) {
                if (checkLoginModel.getSuccess().equalsIgnoreCase("1")) {
                    App.getAppPreference().SaveString(ConstantData.TOKEN, "1");
                    App.getAppPreference().SaveString(ConstantData.USERID, checkLoginModel.getDetails().getId());
                    App.getAppPreference().SaveString(ConstantData.Name, checkLoginModel.getDetails().getName());
                    App.getAppPreference().SaveString(ConstantData.IMAGEPATH, checkLoginModel.getDetails().getUserImage());
//                    Toast.makeText(activity, "Welcome " + App.getAppPreference().GetString(ConstantData.Name), Toast.LENGTH_LONG).show();
                    startActivity(new Intent(activity, SelectTypeActivity.class));

                } else {
                    forgotEmail = email;
                    data = true;
                    CommonUtils.showSnackbarAlert(edmail, checkLoginModel.getMessage());

                }
            }
        });
    }
}
