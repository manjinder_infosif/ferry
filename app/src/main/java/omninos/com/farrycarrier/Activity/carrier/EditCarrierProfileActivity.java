package omninos.com.farrycarrier.activity.carrier;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import omninos.com.farrycarrier.model.UpdateProfileModel;
import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.util.App;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.viewmodels.UpdateProfileVM;

public class EditCarrierProfileActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView BackButton, profilePic;
    private Button SaveButton;
    private EditText editTextName, editTextPhone, editTextAddress, editTextWork, et_lastname;
    private static final int CAMERA_REQUEST = 200;
    private static final int PICK_FROM_GALLERY = 100;
    private Bitmap bitmap;
    private Uri uri;
    private String userChoosenTask = "", imagepath = "", path;
    private UpdateProfileVM updateProfileVM;
    private String Name, PhoneNumber, Address, WorkExperience;
    private String name, phoneNumber, address, workExp, lastname;
    MultipartBody.Part image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_carrier_profile);

        name = getIntent().getStringExtra("name");
        phoneNumber = getIntent().getStringExtra("phone");
        address = getIntent().getStringExtra("address");
        workExp = getIntent().getStringExtra("workExp");
        lastname = getIntent().getStringExtra("lastname");

        updateProfileVM = ViewModelProviders.of(this).get(UpdateProfileVM.class);
        findIds();
        setUps();

        onClicks();
    }

    private void setUps() {
        editTextName.setText(name);
        editTextPhone.setText(phoneNumber);
        editTextAddress.setText(address);
        editTextWork.setText(workExp);

        if (App.getAppPreference().GetString(ConstantData.IMAGEPATH).isEmpty())
            profilePic.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile_placeholder));
        else
            Glide.with(EditCarrierProfileActivity.this).load(App.getAppPreference().GetString(ConstantData.IMAGEPATH)).into(profilePic);
    }

    private void findIds() {
        BackButton = findViewById(R.id.back_btn);
        SaveButton = findViewById(R.id.save_btn);
        //edit texts
        editTextName = findViewById(R.id.et_name);
        editTextPhone = findViewById(R.id.et_phone);
        editTextAddress = findViewById(R.id.et_address);
        editTextWork = findViewById(R.id.et_work);
        et_lastname = findViewById(R.id.et_lastname);
        profilePic = findViewById(R.id.profilepic);
    }

    private void onClicks() {
        BackButton.setOnClickListener(this);
        SaveButton.setOnClickListener(this);
        profilePic.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                finish();
                break;
            case R.id.save_btn:
                validate();
                break;

            case R.id.profilepic:
                showPopup();
                break;
        }

    }

    private void validate() {
        String userId = App.getAppPreference().GetString(ConstantData.USERID);
        Name = editTextName.getText().toString();
        PhoneNumber = editTextPhone.getText().toString();
        Address = editTextAddress.getText().toString();
        WorkExperience = editTextWork.getText().toString();
        lastname = et_lastname.getText().toString();

        if (TextUtils.isEmpty(Name) || TextUtils.isEmpty(PhoneNumber) || TextUtils.isEmpty(Address) || TextUtils.isEmpty(WorkExperience) || TextUtils.isEmpty(lastname))
            CommonUtils.showSnackbarAlert(editTextName, ConstantData.Fill_DETAILS);
        else if (PhoneNumber.length() < 10)
            CommonUtils.showSnackbarAlert(editTextPhone, "Enter minimun 10 digits");
        else {
            if (TextUtils.isEmpty(imagepath)) {
                RequestBody attachmentEmpty = RequestBody.create(MediaType.parse("text/plain"), "");
                image = MultipartBody.Part.createFormData("userImage", "", attachmentEmpty);
            } else {
                File file = new File(imagepath);
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                image = MultipartBody.Part.createFormData("userImage", file.getName(), requestFile);
                Glide.with(this).load(R.drawable.ic_profile_placeholder).into(profilePic);
            }
            RequestBody userIdbody = RequestBody.create(MediaType.parse("text/plain"), userId);
            RequestBody phonebody = RequestBody.create(MediaType.parse("text/plain"), PhoneNumber);
            RequestBody addressbody = RequestBody.create(MediaType.parse("text/plain"), Address);
            RequestBody workExperiencebody = RequestBody.create(MediaType.parse("text/plain"), WorkExperience);
            RequestBody namebody = RequestBody.create(MediaType.parse("text/plain"), Name);
            RequestBody lastnamebody = RequestBody.create(MediaType.parse("text/plain"), lastname);
            updateProfile(userIdbody, phonebody, addressbody, workExperiencebody, namebody,lastnamebody, image);
        }
    }

    private void updateProfile(RequestBody userIdbody, RequestBody phonebody, RequestBody addressbody, RequestBody workExperiencebody, RequestBody namebody,RequestBody lastnamebody, MultipartBody.Part image) {
        updateProfileVM.updateprofile(EditCarrierProfileActivity.this, userIdbody, phonebody, addressbody, workExperiencebody, namebody,lastnamebody, image).observe(this, new Observer<UpdateProfileModel>() {
            @Override
            public void onChanged(@Nullable UpdateProfileModel updateProfileModel) {
                if (updateProfileModel.getSuccess().equalsIgnoreCase("1")) {
                    App.getAppPreference().SaveString(ConstantData.Name, Name + " " + lastname);
//                    Toast.makeText(EditCarrierProfileActivity.this, "Profile Updated", Toast.LENGTH_SHORT).show();
                    Intent saved = new Intent(EditCarrierProfileActivity.this, CarrierProfileActivity.class);
                    startActivity(saved);
                    Intent intent = new Intent("UpdateCarrierProfilePic");
                    LocalBroadcastManager.getInstance(EditCarrierProfileActivity.this).sendBroadcast(intent);
                    finish();
                } else
                    CommonUtils.showSnackbarAlert(editTextName, updateProfileModel.getMessage());
            }
        });
    }

    private void showPopup() {

        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(EditCarrierProfileActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo"))
                    cameraIntent();

                else if (items[item].equals("Choose from Library"))
                    galleryIntent();

                else if (items[item].equals("Cancel"))
                    dialog.dismiss();

            }
        });
        builder.show();

    }

    private void cameraIntent() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        PackageManager packageManager = this.getPackageManager();

        List<ResolveInfo> listcam = packageManager.queryIntentActivities(intent, 0);

        intent.setPackage(listcam.get(0).activityInfo.packageName);

        startActivityForResult(intent, CAMERA_REQUEST);
    }

    private void galleryIntent() {

        Intent intent = new Intent();

        intent.setType("image/*");

        intent.setAction(Intent.ACTION_GET_CONTENT);//

        startActivityForResult(Intent.createChooser(intent, "Select File"), PICK_FROM_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == PICK_FROM_GALLERY)

                onSelectFromGalleryResult(data);

            else if (requestCode == CAMERA_REQUEST)

                onCaptureImageResult(data);
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        if (data != null) {
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        uri = getImageUri(EditCarrierProfileActivity.this, bitmap);

        imagepath = getRealPathFromUri(uri);
        App.getAppPreference().SaveString(ConstantData.IMAGEPATH, imagepath);
        Glide.with(this).load("file://" + imagepath).into(profilePic);
//        profilePic.setImageBitmap(bitmap);
    }


    private void onCaptureImageResult(Intent data) {
        try {
            bitmap = (Bitmap) data.getExtras().get("data");
            uri = getImageUri(EditCarrierProfileActivity.this, bitmap);
            imagepath = getRealPathFromUri(uri);
            App.getAppPreference().SaveString(ConstantData.IMAGEPATH, imagepath);

            Glide.with(this).load("file://" + imagepath).into(profilePic);
//        profilePic.setImageBitmap(bitmap);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Uri getImageUri(EditCarrierProfileActivity activity, Bitmap bitmap) {
        path = MediaStore.Images.Media.insertImage(activity.getContentResolver(), bitmap, "Title", null);
        return Uri.parse(path);
    }

    private String getRealPathFromUri(Uri tempUri) {

        Cursor cursor = null;
        try {

            String[] proj = {MediaStore.Images.Media.DATA};

            cursor = this.getContentResolver().query(tempUri, proj, null, null, null);

            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

            cursor.moveToFirst();

            return cursor.getString(column_index);

        } finally {

            if (cursor != null) {

                cursor.close();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 100:
                boolean camera = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean gallery = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                boolean locationfine = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                boolean locationcourse = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                if (grantResults.length > 0 && camera && gallery && locationfine && locationcourse) {


                } else if (Build.VERSION.SDK_INT >= 23 && !shouldShowRequestPermissionRationale(permissions[0])) {
                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
                    builder.setTitle("Permissions");
                    builder.setMessage("Storage Permissions are Required");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //send to settings
                            Toast.makeText(EditCarrierProfileActivity.this, "Go to Settings to Grant the Storage Permissions and restart application", Toast.LENGTH_LONG).show();

                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", EditCarrierProfileActivity.this.getPackageName(), null);
                            intent.setData(uri);
                            startActivity(intent);
                        }
                    })
                            .create()
                            .show();
                } else {
                    Toast.makeText(EditCarrierProfileActivity.this, "Permission Denied", Toast.LENGTH_SHORT).show();
                    ActivityCompat.requestPermissions(EditCarrierProfileActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 100);
                }
                break;
        }


    }
}
