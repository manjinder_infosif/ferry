package omninos.com.farrycarrier.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import omninos.com.farrycarrier.R;

public class PaymentCardActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView appbarBack;
    TextView appbarText;
    Button payNowBtn;
    Activity activity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment__card_);
        findIds();

    }

    private void findIds() {

        activity = PaymentCardActivity.this;

        appbarBack = findViewById(R.id.appbarBack);
        appbarBack.setVisibility(View.VISIBLE);
        appbarBack.setOnClickListener(this);
        appbarText = findViewById(R.id.appbarText);
        appbarText.setText("Payment");

        payNowBtn=findViewById(R.id.payNowBtn);
        payNowBtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.appbarBack:
                onBackPressed();
                break;

            case R.id.payNowBtn:
                dialogVerifyCode();
                break;
        }

    }
    private void dialogVerifyCode() {

        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.payment_done_dialog);
        dialog.setCancelable(false);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
                Intent intent=new Intent(activity, SelectTypeActivity.class);
                startActivity(intent);
                finish();
            }
        }, 2000);

        dialog.show();

    }
}
