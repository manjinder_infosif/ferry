package omninos.com.farrycarrier.activity;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import omninos.com.farrycarrier.model.CheckEmailModel;
import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.util.App;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.viewmodels.CheckEmailVM;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn_signup;
    private TextView AcRegister, checkConditions;
    private Activity activity;
    private ImageView backbtn;
    private EditText edname, edmail, edpassword, edrepassword, ednameLast;
    private CheckEmailVM checkEmailVM;
    private Boolean aBoolean = true;
    private String name, email, password, cpassword, lastName;
    private CheckBox termCheckBox;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_);
        checkEmailVM = ViewModelProviders.of(this).get(CheckEmailVM.class);
        activity = SignUpActivity.this;

        findids();
        setUps();
    }


    private void findids() {

        activity = SignUpActivity.this;

        btn_signup = findViewById(R.id.btn_signup);
        AcRegister = findViewById(R.id.AcRegister);
        backbtn = findViewById(R.id.backbtn);
        edname = findViewById(R.id.edname);
        edmail = findViewById(R.id.edmail);
        edpassword = findViewById(R.id.edpassword);
        edrepassword = findViewById(R.id.edrepassword);

        termCheckBox = findViewById(R.id.termCheckBox);
        checkConditions = findViewById(R.id.checkConditions);

        ednameLast = findViewById(R.id.ednameLast);

    }

    private void setUps() {
        btn_signup.setOnClickListener(this);
        AcRegister.setOnClickListener(this);
        backbtn.setOnClickListener(this);
        checkConditions.setOnClickListener(this);

//        edmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//
//                if (!hasFocus) {
//                    if (isValidEmail(edmail.getText().toString())) {
//                        checkemail(edmail.getText().toString());
//                    } else {
//                        CommonUtils.showSnackbarAlert(edmail, "Please Enter a valid Email!");
//                    }
//                }
//            }
//        });
//
//        edpassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                    if (!isValidPassword(edpassword.getText().toString())) {
//                        CommonUtils.showSnackbarAlert(edmail, "Please Enter a valid Password!");
//                    } else {
//                        edrepassword.requestFocus();
//                    }
//                }
//            }
//        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.AcRegister:
                startActivity(new Intent(activity, LoginActivity.class));
                break;

            case R.id.btn_signup:
                validate();
                break;

            case R.id.backbtn:
                onBackPressed();
                break;

            case R.id.checkConditions:
                startActivity(new Intent(SignUpActivity.this, TermActivity.class).putExtra("Status", "2"));
                break;
        }
    }

    private void validate() {
        name = edname.getText().toString();
        email = edmail.getText().toString();
        password = edpassword.getText().toString();
        cpassword = edrepassword.getText().toString();
        lastName = ednameLast.getText().toString();
        App.getAppPreference().SaveString(ConstantData.Name, name + " " + lastName);

        if (TextUtils.isEmpty(name)) {
            CommonUtils.showSnackbarAlert(edname, "Fill First name Fields");
        } else if (TextUtils.isEmpty(lastName)) {
            CommonUtils.showSnackbarAlert(ednameLast, "Fill last name Fields");
        } else if (TextUtils.isEmpty(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            CommonUtils.showSnackbarAlert(edname, "Fill email Fields");
        } else if (TextUtils.isEmpty(password) || password.length() < 6) {
            CommonUtils.showSnackbarAlert(edname, "enter minimum 7 character password");
        } else if (TextUtils.isEmpty(cpassword)) {
            CommonUtils.showSnackbarAlert(edname, "enter minimum 7 character password");
        } else if (!password.equals(cpassword)) {
            CommonUtils.showSnackbarAlert(edname, "Password does not match.");
        } else if (!termCheckBox.isChecked()) {
            CommonUtils.showSnackbarAlert(btn_signup, "Agree to the terms!");
        } else {
            checkemail(email);
        }

    }

    private void checkemail(final String email) {
        checkEmailVM.checkemail(activity, email).observe(this, new Observer<CheckEmailModel>() {
            @Override
            public void onChanged(@Nullable CheckEmailModel checkEmailModel) {
                if (checkEmailModel.getSuccess().equalsIgnoreCase("1")) {
                    aBoolean = true;
                    Toast.makeText(activity, "Great, Now Let's Add Your Phone Number!", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(SignUpActivity.this, VerificationActivity.class);
                    intent.putExtra("username", name);
                    intent.putExtra("useremail", email);
                    intent.putExtra("userpassword", password);
                    intent.putExtra("lastName", lastName);
                    startActivity(intent);
                } else {
                    aBoolean = false;
                    CommonUtils.showSnackbarAlert(edmail, checkEmailModel.getMessage());
                }
            }
        });
    }

    private boolean isValidEmail(String email) {

        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);

        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }

    public static boolean isValidPassword(String s) {
        Pattern PASSWORD_PATTERN
                = Pattern.compile(
                "[a-zA-Z0-9\\!\\@\\#\\$]{8,24}");
        return !TextUtils.isEmpty(s) && PASSWORD_PATTERN.matcher(s).matches();
    }
}
