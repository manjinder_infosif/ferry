package omninos.com.farrycarrier.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;

import omninos.com.farrycarrier.R;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {

    private Switch aSwitchloc, aSwitchnoti;
    private ImageView backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        findIds();
        swicthToggle();
    }

    private void findIds() {
        aSwitchloc = findViewById(R.id.switch_loc);
        aSwitchloc.setTrackDrawable(getResources().getDrawable(R.drawable.non_active_toggle));

        aSwitchnoti = findViewById(R.id.switch_noti);
        aSwitchnoti.setTrackDrawable(getResources().getDrawable(R.drawable.non_active_toggle));

        backButton = findViewById(R.id.back_btn);
        backButton.setOnClickListener(this);
    }

    private void swicthToggle() {
        aSwitchloc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    aSwitchloc.setTrackDrawable(getResources().getDrawable(R.drawable.active_toggle));
                    aSwitchloc.setThumbDrawable(getResources().getDrawable(R.drawable.thumb_nonactive));

                } else {
                    aSwitchloc.setTrackDrawable(getResources().getDrawable(R.drawable.non_active_toggle));
                    aSwitchloc.setThumbDrawable(getResources().getDrawable(R.drawable.thumb_nonactive));
                }
            }
        });

        aSwitchnoti.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    aSwitchnoti.setTrackDrawable(getResources().getDrawable(R.drawable.active_toggle));
                    aSwitchnoti.setThumbDrawable(getResources().getDrawable(R.drawable.thumb_nonactive));
                } else {
                    aSwitchnoti.setTrackDrawable(getResources().getDrawable(R.drawable.non_active_toggle));
                    aSwitchnoti.setThumbDrawable(getResources().getDrawable(R.drawable.thumb_nonactive));
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                finish();
                break;

        }

    }
}
