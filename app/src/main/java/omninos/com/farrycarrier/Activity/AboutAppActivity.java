package omninos.com.farrycarrier.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import omninos.com.farrycarrier.BuildConfig;
import omninos.com.farrycarrier.R;

public class AboutAppActivity extends AppCompatActivity {

    private TextView terms, appVersion, copyData;
    private ImageView back_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_app);

        initView();
        SetUp();
    }

    private void initView() {
        appVersion = findViewById(R.id.appVersion);
        terms = findViewById(R.id.terms);
        copyData = findViewById(R.id.copyData);
        back_btn = findViewById(R.id.back_btn);
    }

    private void SetUp() {

        String buildname = BuildConfig.VERSION_NAME;
        appVersion.setText("Version " + buildname);
        copyData.setText("Copyrights@SHARED DIGITAL LOGISTICS");
        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AboutAppActivity.this, TermActivity.class).putExtra("Status", "2"));
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
