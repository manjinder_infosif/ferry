package omninos.com.farrycarrier.activity.carrier;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import de.hdodenhof.circleimageview.CircleImageView;
import omninos.com.farrycarrier.model.UserProfileModel;
import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.util.App;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.viewmodels.UserProfileVM;

public class CarrierProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView BackButton, EditProfileButton;
    private TextView textViewName, textViewPhone, textViewAddress, textViewWork,tv_lastname;
    private UserProfileVM userProfileVM;
    private CircleImageView profilePic;
    private String Name, PhoneNumber, Address, WorkExp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carrier_profile);
        userProfileVM = ViewModelProviders.of(this).get(UserProfileVM.class);

        findIds();
        setUps();

        getUserProfileApi();
        OnCLicks();
    }

    private void setUps() {
        if (App.getAppPreference().GetString(ConstantData.IMAGEPATH).isEmpty())
            profilePic.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile_placeholder));
        else
            Glide.with(CarrierProfileActivity.this).load(App.getAppPreference().GetString(ConstantData.IMAGEPATH)).into(profilePic);
    }


    private void getUserProfileApi() {
        userProfileVM.userprofile(CarrierProfileActivity.this, App.getAppPreference().GetString(ConstantData.USERID)).observe(this, new Observer<UserProfileModel>() {
            @Override
            public void onChanged(@Nullable UserProfileModel userProfileModel) {
                if (userProfileModel.getSuccess().equalsIgnoreCase("1")) {
                    App.getAppPreference().SaveString(ConstantData.Name, userProfileModel.getDetails().getName());
                    App.getAppPreference().SaveString(ConstantData.IMAGEPATH, userProfileModel.getDetails().getUserImage());
                    if (userProfileModel.getDetails().getUserImage().isEmpty())
                        profilePic.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile_placeholder));
                    else
                        Glide.with(CarrierProfileActivity.this).load(userProfileModel.getDetails().getUserImage()).into(profilePic);
                    Name = (userProfileModel.getDetails().getName());
                    PhoneNumber = (userProfileModel.getDetails().getPhone());
                    Address = (userProfileModel.getDetails().getAddress());
                    WorkExp = (userProfileModel.getDetails().getWorkExperience());


                    textViewName.setText(Name);
                    textViewPhone.setText(PhoneNumber);
                    textViewAddress.setText(Address);
                    textViewWork.setText(WorkExp);
                    tv_lastname.setText(userProfileModel.getDetails().getLastName());

                } else {
                    CommonUtils.showSnackbarAlert(textViewName, userProfileModel.getMessage());
                }
            }
        });
    }

    private void findIds() {
        BackButton = findViewById(R.id.back_btn);
        EditProfileButton = findViewById(R.id.edit_btn);

        //textviews
        textViewName = findViewById(R.id.tv_name);
        textViewPhone = findViewById(R.id.tv_phone);
        textViewAddress = findViewById(R.id.tv_address);
        textViewWork = findViewById(R.id.tv_work);
        tv_lastname=findViewById(R.id.tv_lastname);
        profilePic = findViewById(R.id.profile_pic);


    }

    private void OnCLicks() {
        BackButton.setOnClickListener(this);
        EditProfileButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.back_btn:
                finish();
                break;
            case R.id.edit_btn:
                Intent intent = new Intent(CarrierProfileActivity.this, EditCarrierProfileActivity.class);
                intent.putExtra("name", textViewName.getText().toString());
                intent.putExtra("phone", textViewPhone.getText().toString());
                intent.putExtra("address", textViewAddress.getText().toString());
                intent.putExtra("workExp", textViewWork.getText().toString());
                intent.putExtra("lastname",tv_lastname.getText().toString());
                startActivity(intent);
                finish();
                break;

        }
    }
}
