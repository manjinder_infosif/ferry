package omninos.com.farrycarrier.activity.sender;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import omninos.com.farrycarrier.activity.WalletActivity;
import omninos.com.farrycarrier.activity.carrier.AirportListActivity;
import omninos.com.farrycarrier.activity.carrier.CarryPackageActivity;
import omninos.com.farrycarrier.adapter.LocalAirportListAdapter;
import omninos.com.farrycarrier.model.AirportListModel;
import omninos.com.farrycarrier.model.WalletBalanceModel;
import omninos.com.farrycarrier.model.sender.RecieverInformationModel;
import omninos.com.farrycarrier.model.sender.SenderInformationModel;
import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.util.App;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.util.Constants;
import omninos.com.farrycarrier.viewmodels.SenderInformationVM;
import omninos.com.farrycarrier.retrofit.ApiClient;
import omninos.com.farrycarrier.retrofit.ApiService;
import omninos.com.farrycarrier.viewmodels.WalletBalanceVM;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

public class SendPackageActivity extends AppCompatActivity implements View.OnClickListener {

    private Activity activity;
    private ImageView appbarBack;
    private Button btn_submit;
    private TextView tvappbarText, tvdeliveryDate, tvLocalAirport, tvDestinationAirport;
    private String dobRegS = "", checkString, country, LocalAirports, DestinationAirports, walletBalance;
    private Spinner spinnerSourceCountry, spinnerDestinationCountry;
    private EditText packageType, packageSize, localAirportCode, destinationAirportCode, suggestions;
    private SenderInformationVM senderInformationVM;
    private WalletBalanceVM walletBalanceVM;
    private RecyclerView recyclerViewLocalAirports, recyclerViewDestinationAirports;
    private ArrayList<String> SourceCountries = new ArrayList<>();
    private ArrayList<String> DestinationCountries = new ArrayList<>();
    final List<String> airportList = new ArrayList<>();
    private List<AirportListModel.Detail> details = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sender__profile_);
        activity = SendPackageActivity.this;

        senderInformationVM = ViewModelProviders.of(this).get(SenderInformationVM.class);
        walletBalanceVM = ViewModelProviders.of(this).get(WalletBalanceVM.class);

        findids();
        setUps();
        spinnerSource_Country();
        spinnerDestination_Country();
//        getWalletBalanceApi();
//        getLocalAirport();
//        getDestinationAirport();

    }

    private void setUps() {
        appbarBack.setVisibility(View.VISIBLE);
        tvappbarText.setText("Send Package");
        appbarBack.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
        tvdeliveryDate.setOnClickListener(this);
        tvLocalAirport.setOnClickListener(this);
        tvDestinationAirport.setOnClickListener(this);
    }

    private void spinnerDestination_Country() {
        DestinationCountries.add("Select Country");
        DestinationCountries.add("Canada");
        DestinationCountries.add("United Kingdom");
        DestinationCountries.add("India");
        DestinationCountries.add("Philippines");
        DestinationCountries.add("United States");
        DestinationCountries.add("Guyana");

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(SendPackageActivity.this, R.layout.support_simple_spinner_dropdown_item, DestinationCountries);
        arrayAdapter.setDropDownViewResource(R.layout.spinner_text_view);
        spinnerDestinationCountry.setAdapter(arrayAdapter);
        spinnerDestinationCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                country = DestinationCountries.get(position);
//                showDestinationAirportlist("");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void spinnerSource_Country() {

        SourceCountries.add("Select Country");
        SourceCountries.add("Canada");
        SourceCountries.add("United Kingdom");
        SourceCountries.add("India");
        SourceCountries.add("Philippines");
        SourceCountries.add("United States");
        SourceCountries.add("Guyana");


        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(SendPackageActivity.this, R.layout.support_simple_spinner_dropdown_item, SourceCountries);
        arrayAdapter.setDropDownViewResource(R.layout.spinner_text_view);
        spinnerSourceCountry.setAdapter(arrayAdapter);

        spinnerSourceCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                country = SourceCountries.get(position);
//                showLocalAirportlist("");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }


    private void findids() {
        activity = SendPackageActivity.this;
        appbarBack = findViewById(R.id.appbarBack);
        btn_submit = findViewById(R.id.btn_submit);
        //textviews
        tvappbarText = findViewById(R.id.appbarText);
        tvdeliveryDate = findViewById(R.id.eddate);
        tvLocalAirport = findViewById(R.id.tv_local_airport);
        tvDestinationAirport = findViewById(R.id.tv_destination_airport);
        //spinners
        spinnerSourceCountry = findViewById(R.id.spinner_source_country);
        spinnerDestinationCountry = findViewById(R.id.spinner_destination_country);
        //edit texts
        packageType = findViewById(R.id.eddoctype);
        packageSize = findViewById(R.id.edaweight);
        suggestions = findViewById(R.id.edsug);
        //recyclerviews
        recyclerViewLocalAirports = findViewById(R.id.recyclerview_locallist);
        recyclerViewDestinationAirports = findViewById(R.id.recyclerview_destlist);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_submit:
                validate();
                break;

            case R.id.eddate:
                datepicker();
                break;

            case R.id.appbarBack:
                onBackPressed();
                break;

            case R.id.tv_local_airport:
                Intent intent = new Intent(SendPackageActivity.this, AirportListActivity.class);
                intent.putExtra("country", country);
                startActivityForResult(intent, 100);
                break;

            case R.id.tv_destination_airport:
                Intent intent1 = new Intent(SendPackageActivity.this, AirportListActivity.class);
                intent1.putExtra("country", country);
                startActivityForResult(intent1, 101);
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                tvLocalAirport.setText(data.getStringExtra("airport") + "(" + data.getStringExtra("city") + ")");
                LocalAirports = data.getStringExtra("airport");
            }
        }
        if (requestCode == 101) {
            if (resultCode == RESULT_OK) {
                tvDestinationAirport.setText(data.getStringExtra("airport") + "(" + data.getStringExtra("city") + ")");
                DestinationAirports = data.getStringExtra("airport");
            }
        }
    }


    private void validate() {
        String PackageType = packageType.getText().toString();
        String PackageSize = packageSize.getText().toString();
        String Country = country;
        String LocalAirportCode = LocalAirports;
        String DestinationAirportCode = DestinationAirports;
        String Date = dobRegS;
        String Suggestions = suggestions.getText().toString();
        String id = App.getAppPreference().GetString(ConstantData.USERID);

        if (TextUtils.isEmpty(PackageType) || (TextUtils.isEmpty(PackageSize)) || (TextUtils.isEmpty(Country)) || (TextUtils.isEmpty(LocalAirportCode)) || (TextUtils.isEmpty(DestinationAirportCode)) || (TextUtils.isEmpty(Date)) || (TextUtils.isEmpty(Suggestions))) {
            CommonUtils.showSnackbarAlert(packageType, "Fill Mandatory Fields");
        } else if (Double.parseDouble(PackageSize) < 2.5 || Double.parseDouble(PackageSize) > 4.0) {

            CommonUtils.showSnackbarAlert(packageType, "Weight should be between 2.5Kgs and 4Kgs");
        } else {
            senderinformation(id, PackageType, PackageSize, Country, LocalAirportCode, DestinationAirportCode, Date, Suggestions);
        }
//            senderinformation(id, PackageType, PackageSize, Country, LocalAirportCode, DestinationAirportCode, Date, Suggestions);
    }

    private void senderinformation(final String id, final String packageType, final String packageSize, String country, String localAirportCode, String destinationAirportCode, String date, String suggestions) {

        senderInformationVM.senderinformation(activity, id, packageType, packageSize, country.toUpperCase(), localAirportCode, destinationAirportCode, date, suggestions).observe(this, new Observer<RecieverInformationModel>() {
            @Override
            public void onChanged(@Nullable RecieverInformationModel senderInformationModel) {
                if (senderInformationModel.getSuccess().equalsIgnoreCase("1")) {
                    App.getSinltonPojo().setDetailsList(senderInformationModel.getDetails());
                    Intent i1 = new Intent(activity, Nearby_Couriers_Activity.class);
                    i1.putExtra(Constants.key, checkString);
                    startActivity(i1);
                } else
                    CommonUtils.showSnackbarAlert(tvdeliveryDate, senderInformationModel.getMessage());
            }
        });

    }

    private void getWalletBalanceApi() {
        walletBalanceVM.walletBalance(activity, App.getAppPreference().GetString(ConstantData.USERID)).observe(this, new Observer<WalletBalanceModel>() {
            @Override
            public void onChanged(@Nullable WalletBalanceModel walletBalanceModel) {
                if (walletBalanceModel.getSuccess().equalsIgnoreCase("1")) {
                    walletBalance = walletBalanceModel.getWalletBalance();
                    if (Integer.parseInt(walletBalance) < 65) {
                        NofifyUser();
//                        CommonUtils.showSnackbarAlert(tvdeliveryDate, "Insufficient Wallet Balance");
                    } else {
//                        Intent i1 = new Intent(activity, Nearby_Couriers_Activity.class);
//                        i1.putExtra(Constants.key, checkString);
//                        startActivity(i1);


                    }
                }else {
                    NofifyUser();
                }
            }
        });
    }

    private void NofifyUser() {
        NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder.getInstance(this);

        dialogBuilder
                .withTitle("Wallet Balance")
                .withMessage("You have Insufficient Wallet Balance")
                .withButton1Text("Load Amount")
                .withButton2Text("Cancel")
                .setButton1Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i1 = new Intent(activity, WalletActivity.class);
                        i1.putExtra("Type","1");
                        startActivity(i1);
                    }
                })
                .setButton2Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                })
                .isCancelableOnTouchOutside(false)
                .withDialogColor("#FFFFFF")
                .withTitleColor("#000000")
                .withMessageColor("#000000")
                .show();
    }

    private void datepicker() {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(activity, android.R.style.Theme_Holo_Light_Dialog,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        dobRegS = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;

                        SimpleDateFormat input = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
                        SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

                        Date d = null;
                        try {
                            d = input.parse(dobRegS);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        String formatted = output.format(d);

                        tvdeliveryDate.setText(formatted);
                        dobRegS = formatted;

                    }
                }, mYear, mMonth, mDay);

        Window dialogWindow = datePickerDialog.getWindow();
        dialogWindow.setGravity(Gravity.CENTER);
        datePickerDialog.show();

    }
}
