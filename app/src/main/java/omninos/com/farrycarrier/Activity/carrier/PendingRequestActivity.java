package omninos.com.farrycarrier.activity.carrier;

import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import omninos.com.farrycarrier.adapter.carrier.PendingRequestsAdapter;
import omninos.com.farrycarrier.model.carrier.PackageRequestModel;
import omninos.com.farrycarrier.model.carrier.PendingRequestListModel;
import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.util.App;
import omninos.com.farrycarrier.util.CommonUtils;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.viewmodels.PackageRequestVM;
import omninos.com.farrycarrier.viewmodels.PendingRequestListVM;

public class PendingRequestActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView recyclerViewRequests;
    private ImageView BackButton;
    private TextView NoRequests;
    private PackageRequestVM packageRequestVM;
    private String Type, bookingServiceId, Id;
    private PendingRequestListVM pendingRequestListVM;
    private List<PendingRequestListModel.Detail> pendingRequestListModalList = new ArrayList<>();
    private PendingRequestsAdapter requestsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_request);
        packageRequestVM = ViewModelProviders.of(this).get(PackageRequestVM.class);
        pendingRequestListVM = ViewModelProviders.of(this).get(PendingRequestListVM.class);

        Id = App.getAppPreference().GetString(ConstantData.USERID);
        findIds();
        onClicks();
        getPendingRequestListApi();
    }

    private void findIds() {
        BackButton = findViewById(R.id.back_btn);
        recyclerViewRequests = findViewById(R.id.recyclerview_requests);
        recyclerViewRequests.setLayoutManager(new LinearLayoutManager(PendingRequestActivity.this));
        recyclerViewRequests.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        NoRequests = findViewById(R.id.tv_norequ);
    }

    private void onClicks() {
        BackButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                finish();
                break;
        }
    }

    private void getPendingRequestListApi() {
        if (pendingRequestListModalList != null) {
            pendingRequestListModalList.clear();
        }
        pendingRequestListVM.pendingrequestlist(PendingRequestActivity.this, Id).observe(this, new Observer<PendingRequestListModel>() {
            @Override
            public void onChanged(@Nullable PendingRequestListModel pendingRequestListModel) {
                if (pendingRequestListModel.getSuccess().equalsIgnoreCase("1")) {
                    pendingRequestListModalList = pendingRequestListModel.getDetails();
                    if (pendingRequestListModalList.size() == 0) {
                        NoRequests.setText(pendingRequestListModel.getMessage());
                        recyclerViewRequests.setVisibility(View.GONE);
                        requestsAdapter.notifyDataSetChanged();
                    } else {
                        NoRequests.setVisibility(View.GONE);
                        requestsAdapter = new PendingRequestsAdapter(PendingRequestActivity.this, pendingRequestListModalList, new PendingRequestsAdapter.ClickListener() {
                            @Override
                            public void Onclick(int position, String status) {
                                showVerifyDialog();
                                bookingServiceId = pendingRequestListModalList.get(position).getId();
                                Type = status;
                            }
                        });
                        recyclerViewRequests.setAdapter(requestsAdapter);
                        requestsAdapter.notifyDataSetChanged();
                    }
                } else {
//                    pendingRequestListModalList.clear();
//                    requestsAdapter.notifyDataSetChanged();
                    NoRequests.setText(pendingRequestListModel.getMessage());
                }
            }
        });
    }

    private void showVerifyDialog() {
        final Dialog dialog = new Dialog(PendingRequestActivity.this);
        dialog.setContentView(R.layout.popup_verify_request);
        dialog.setCancelable(true);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button continueButton = dialog.findViewById(R.id.btn_continue);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getProviderServiceApi();
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    private void getProviderServiceApi() {
        packageRequestVM.packagerequest(PendingRequestActivity.this, Type, bookingServiceId).observe(this, new Observer<PackageRequestModel>() {
            @Override
            public void onChanged(@Nullable PackageRequestModel packageRequestModel) {
                if (packageRequestModel.getSuccess().equalsIgnoreCase("1")) {
//                    getPendingRequestListApi();
                    startActivity(new Intent(PendingRequestActivity.this, HomeCarrierActivity.class));
                    finish();
                } else {

                    CommonUtils.showSnackbarAlert(BackButton, packageRequestModel.getMessage());
                }
            }
        });
    }


}
