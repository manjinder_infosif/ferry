package omninos.com.farrycarrier.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.adapter.carrier.CarrierPastPackageAdapter;
import omninos.com.farrycarrier.model.carrier.CarrierPastPackageListModel;
import omninos.com.farrycarrier.util.App;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.viewmodels.CarrierPastPackageListVM;

public class CarrierPastPackageFragment extends Fragment {

    private RecyclerView recyclerViewPastPackages;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView textViewMessage;
    private CarrierPastPackageListVM carrierPastPackageListVM;
    private List<CarrierPastPackageListModel.Detail> carrierPastPackageListModelList;
    private ProgressBar progressBar;
    private CarrierPastPackageAdapter adapter;
    private boolean doInOnAttach = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_carrier_past_package, container, false);
        carrierPastPackageListVM = ViewModelProviders.of(this).get(CarrierPastPackageListVM.class);
        findIds(view);
        getPastPackageListApi();
        setUps();
        return view;
    }

    private void getPastPackageListApi() {
        carrierPastPackageListVM.carrierPastPackageList(getActivity(), App.getAppPreference().GetString(ConstantData.USERID)).observe(this, new Observer<CarrierPastPackageListModel>() {
            @Override
            public void onChanged(@Nullable CarrierPastPackageListModel carrierPastPackageListModel) {
                swipeRefreshLayout.setRefreshing(false);
                if (carrierPastPackageListModel.getSuccess().equalsIgnoreCase("1")) {
                    carrierPastPackageListModelList = carrierPastPackageListModel.getDetails();
                    adapter = new CarrierPastPackageAdapter(getActivity(), carrierPastPackageListModelList);
                    recyclerViewPastPackages.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    progressBar.setVisibility(View.GONE);
                } else {
                    textViewMessage.setText(carrierPastPackageListModel.getMessage());
                    progressBar.setVisibility(View.GONE);
                }
            }
        });
    }

    private void setUps() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getPastPackageListApi();
            }
        });
    }

    private void findIds(View view) {
        recyclerViewPastPackages = view.findViewById(R.id.recyclerview_pastpackages);
        recyclerViewPastPackages.setLayoutManager(new LinearLayoutManager(getActivity()));
//        recyclerViewPastPackages.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        textViewMessage = view.findViewById(R.id.tv_nodata);
        swipeRefreshLayout = view.findViewById(R.id.pullToRefresh);
        progressBar = view.findViewById(R.id.progress_bar_past_packages);
    }

}
