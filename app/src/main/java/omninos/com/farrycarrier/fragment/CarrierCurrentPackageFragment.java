package omninos.com.farrycarrier.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import omninos.com.farrycarrier.adapter.carrier.CarrierCurrentPackageAdapter;
import omninos.com.farrycarrier.model.carrier.CarrierCurrentAcceptedPackagesModel;
import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.util.App;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.viewmodels.CarrierCurrentAcceptedPackageVM;


public class CarrierCurrentPackageFragment extends Fragment {
    private RecyclerView recyclerViewCurrentPackage;
    private LinearLayoutManager layoutManager;
    private String status, carrierId;
    private CarrierCurrentAcceptedPackageVM carrierCurrentAcceptedPackageVM;
    private List<CarrierCurrentAcceptedPackagesModel.Detail> carrierCurrentAcceptedPackagesModalList = new ArrayList<>();
    private TextView textViewMessage;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ProgressBar progressBar;
    private CarrierCurrentPackageAdapter adapter;
    private boolean doInOnAttach = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_current_package, container, false);
        if (getArguments() != null) {
//            status = getArguments().getString("status");
        }
        carrierId = App.getAppPreference().GetString(ConstantData.USERID);
        findIds(view);
        setUps();
        getCarrierAcceptListApi();
        return view;
    }

    private void findIds(View view) {
        recyclerViewCurrentPackage = view.findViewById(R.id.recyclerview_currentpackages);
        textViewMessage = view.findViewById(R.id.tv_nodata);
        swipeRefreshLayout = view.findViewById(R.id.pullToRefresh);
        progressBar = view.findViewById(R.id.progress_bar_current_packages);
    }

    private void setUps() {
        carrierCurrentAcceptedPackageVM = ViewModelProviders.of(this).get(CarrierCurrentAcceptedPackageVM.class);

        recyclerViewCurrentPackage.setLayoutManager(new LinearLayoutManager(getActivity()));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getCarrierAcceptListApi();
            }
        });
    }

    private void getCarrierAcceptListApi() {
        carrierCurrentAcceptedPackageVM.carriercurrentpackagelist(getActivity(), carrierId).observe(this, new Observer<CarrierCurrentAcceptedPackagesModel>() {
            @Override
            public void onChanged(@Nullable CarrierCurrentAcceptedPackagesModel carrierCurrentAcceptedPackagesModel) {
                swipeRefreshLayout.setRefreshing(false);
                if (carrierCurrentAcceptedPackagesModel.getSuccess().equalsIgnoreCase("1")) {
                    carrierCurrentAcceptedPackagesModalList = carrierCurrentAcceptedPackagesModel.getDetails();
                    adapter = new CarrierCurrentPackageAdapter(getActivity(), carrierCurrentAcceptedPackagesModel.getDetails());
                    recyclerViewCurrentPackage.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    progressBar.setVisibility(View.GONE);

                } else {
                    textViewMessage.setText(carrierCurrentAcceptedPackagesModel.getMessage());
                    progressBar.setVisibility(View.GONE);
                    recyclerViewCurrentPackage.setVisibility(View.GONE);
                }
            }
        });

    }
}

