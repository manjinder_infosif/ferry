package omninos.com.farrycarrier.fragment;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import omninos.com.farrycarrier.adapter.sender.SenderPastPackageAdpater;
import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.util.App;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.viewmodels.PastPackageListVM;
import omninos.com.farrycarrier.model.sender.SenderPastPackageListModel;

public class SenderPastPackageFragment extends Fragment {

    private RecyclerView recyclerViewPastPackages;
    private PastPackageListVM pastPackageListVM;
    private List<SenderPastPackageListModel.Detail> pastPackageListModelArrayList = new ArrayList<>();
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView textViewMessage;
    private ProgressBar progressBar;
    private SenderPastPackageAdpater adpater;
    private boolean doInOnAttach=false;
    private Activity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sender_past_package, container, false);
        activity=getActivity();
        pastPackageListVM = ViewModelProviders.of(this).get(PastPackageListVM.class);
        // Inflate the layout for this fragment
        findIds(view);
        setUps();
        return view;
    }

    private void findIds(View view) {
        recyclerViewPastPackages = view.findViewById(R.id.recyclerview_pastpackages);
        textViewMessage = view.findViewById(R.id.tv_nodata);
        swipeRefreshLayout = view.findViewById(R.id.pullToRefresh);
        progressBar = view.findViewById(R.id.progress_bar_past_packages);

        textViewMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPastPackageListApi();
            }
        });
    }

    private void setUps() {
        recyclerViewPastPackages.setLayoutManager(new LinearLayoutManager(getActivity()));
        //recyclerViewPastPackages.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        getPastPackageListApi();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getPastPackageListApi();
            }
        });

    }

    private void getPastPackageListApi() {
        pastPackageListVM.pastPackageList(getActivity(), App.getAppPreference().GetString(ConstantData.USERID)).observe(this, new Observer<SenderPastPackageListModel>() {
            @Override
            public void onChanged(@Nullable SenderPastPackageListModel senderPastPackageListModel) {
                swipeRefreshLayout.setRefreshing(false);
                if (senderPastPackageListModel.getSuccess().equalsIgnoreCase("1")) {

                    textViewMessage.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                    pastPackageListModelArrayList = senderPastPackageListModel.getDetails();
                    adpater = new SenderPastPackageAdpater(getActivity(), pastPackageListModelArrayList);
                    adpater.notifyDataSetChanged();
                    recyclerViewPastPackages.setAdapter(adpater);

                } else {
                    textViewMessage.setText(senderPastPackageListModel.getMessage());
                    progressBar.setVisibility(View.GONE);
                }
            }
        });

    }

}
