package omninos.com.farrycarrier.fragment;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import omninos.com.farrycarrier.adapter.sender.SenderCurrentPackageAdapter;
import omninos.com.farrycarrier.model.sender.RequestLIstModel;
import omninos.com.farrycarrier.model.sender.SenderCurrentPackageListModel;
import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.util.App;
import omninos.com.farrycarrier.util.ConstantData;
import omninos.com.farrycarrier.viewmodels.SenderCurrentPackageListVM;


public class SenderCurrentPackageFragment extends Fragment {
    private RecyclerView recyclerViewCurrentPackage;
    private List<RequestLIstModel> requestLIstModelList = new ArrayList<>();
    private TextView textViewMessage;
    private SenderCurrentPackageListVM currentPackageListVM;
    public List<SenderCurrentPackageListModel.Detail> senderCurrentPackageListModalList;
    private String senderId;
    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeRefreshLayout;
    private SenderCurrentPackageAdapter adapter;
    private boolean doInOnAttach=false;
    private Activity activity;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sender_current_package, container, false);
        activity=getActivity();
        currentPackageListVM = ViewModelProviders.of(this).get(SenderCurrentPackageListVM.class);
        findIds(view);
        setUps();
        return view;
    }

    private void findIds(View view) {
        recyclerViewCurrentPackage = view.findViewById(R.id.recyclerview_currentpackages);
        textViewMessage = view.findViewById(R.id.tv_nodata);
        swipeRefreshLayout = view.findViewById(R.id.pullToRefresh);
        progressBar = view.findViewById(R.id.progress_bar_current_packages);
        textViewMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSenderCurrentPackageListApi();
            }
        });
    }

    private void setUps() {
        recyclerViewCurrentPackage.setLayoutManager(new LinearLayoutManager(getActivity()));
//        recyclerViewCurrentPackage.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));

        getSenderCurrentPackageListApi();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getSenderCurrentPackageListApi();
            }
        });
    }

    private void getSenderCurrentPackageListApi() {
        senderId = App.getAppPreference().GetString(ConstantData.USERID);
        currentPackageListVM.sendercurrentpackagelist(getActivity(), senderId).observe(this, new Observer<SenderCurrentPackageListModel>() {
            @Override
            public void onChanged(@Nullable SenderCurrentPackageListModel senderCurrentPackageListModel) {
                swipeRefreshLayout.setRefreshing(false);
                if (senderCurrentPackageListModel.getSuccess().equalsIgnoreCase("1")) {
                    textViewMessage.setVisibility(View.GONE);
                    senderCurrentPackageListModalList = senderCurrentPackageListModel.getDetails();
                    adapter = new SenderCurrentPackageAdapter(getActivity(), senderCurrentPackageListModalList);
                    adapter.notifyDataSetChanged();
                    recyclerViewCurrentPackage.setAdapter(adapter);
                    progressBar.setVisibility(View.GONE);
                } else {
                    textViewMessage.setText(senderCurrentPackageListModel.getMessage());
                    progressBar.setVisibility(View.GONE);
                    recyclerViewCurrentPackage.setVisibility(View.GONE);
                }
            }
        });

    }
}
