package omninos.com.farrycarrier.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import omninos.com.farrycarrier.activity.SelectTypeActivity;
import omninos.com.farrycarrier.activity.carrier.HomeCarrierActivity;
import omninos.com.farrycarrier.activity.carrier.PendingRequestActivity;
import omninos.com.farrycarrier.activity.sender.HomeSenderActivity;
import omninos.com.farrycarrier.R;
import omninos.com.farrycarrier.activity.sender.PackageDetailsCarrierActivity;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final int REQUEST_CODE = 1;
    private static final int NOTIFICATION_ID = 6578;
    private NotificationChannel mChannel;
    private Notification notification;
    private Uri defaultSound;
    private Intent[] intent;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        final String title = remoteMessage.getData().get("title");
        final String message = remoteMessage.getData().get("message");
        final String uniquePinNumber = remoteMessage.getData().get("uniquePinNumber");
        final String type = remoteMessage.getData().get("type");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            setOreoNotification(title, message, type);
        } else {
            showNotification(title, message, type);
        }
    }

    private void showNotification(String title, String message, String type) {

        if (type.equalsIgnoreCase("userBookingServices"))
            intent = new Intent[]{new Intent(this, PendingRequestActivity.class)};
        else if (type.equalsIgnoreCase("accept"))
            intent = new Intent[]{new Intent(this, HomeSenderActivity.class)};
        else if (type.equalsIgnoreCase("reject"))
            intent = new Intent[]{new Intent(this, HomeSenderActivity.class)};
        else if (type.equalsIgnoreCase("Package_Delivered")){
            intent = new Intent[]{new Intent(this, HomeCarrierActivity.class)};
        }else
            intent = new Intent[]{new Intent(this, SelectTypeActivity.class)};


        PendingIntent pendingIntent = PendingIntent.getActivities(this, REQUEST_CODE, intent, PendingIntent.FLAG_ONE_SHOT);
        defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        notification = new NotificationCompat.Builder(this)
                .setContentText(message)
                .setContentTitle(title)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.logo)
                .setStyle(new NotificationCompat.BigTextStyle())
                .setSound(defaultSound)
                .setAutoCancel(true)
                .build();


        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, notification);
    }


    private void setOreoNotification(String title, String message, String type) {
        if (type.equalsIgnoreCase("userBookingServices"))
            intent = new Intent[]{new Intent(this, PendingRequestActivity.class)};
        else if (type.equalsIgnoreCase("accept"))
            intent = new Intent[]{new Intent(this, HomeSenderActivity.class)};
        else if (type.equalsIgnoreCase("reject"))
            intent = new Intent[]{new Intent(this, HomeSenderActivity.class)};
        else if (type.equalsIgnoreCase("Package_Delivered"))
            intent = new Intent[]{new Intent(this, PackageDetailsCarrierActivity.class)};
        else
            intent = new Intent[]{new Intent(this, SelectTypeActivity.class)};

        PendingIntent pendingIntent = PendingIntent.getActivities(this, REQUEST_CODE, intent, PendingIntent.FLAG_ONE_SHOT);
        defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        // Sets an ID for the notification, so it can be updated.
        String CHANNEL_ID = "my_channel_01";// The id of the channel.
        CharSequence name = "huky";// The user-visible name of the channel.

        int importance = NotificationManager.IMPORTANCE_HIGH;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
        }
// Create a notification and set the notification channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notification = new Notification.Builder(this)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setSubText(type)
                    .setSmallIcon(R.drawable.logo)
                    .setStyle(new Notification.BigTextStyle())
                    .setSound(defaultSound)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)
                    .setChannelId(CHANNEL_ID)
                    .build();
        }


        NotificationManager mNotificationManager = (NotificationManager) getSystemService(this.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mNotificationManager.createNotificationChannel(mChannel);
        }

// Issue the notification.
        mNotificationManager.notify(NOTIFICATION_ID, notification);
    }
}