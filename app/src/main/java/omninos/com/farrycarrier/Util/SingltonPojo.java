package omninos.com.farrycarrier.util;

import java.util.List;

import omninos.com.farrycarrier.model.sender.RecieverInformationModel;

public class SingltonPojo {

    private List<RecieverInformationModel.Detail> detailsList;

    public List<RecieverInformationModel.Detail> getDetailsList() {
        return detailsList;
    }

    public void setDetailsList(List<RecieverInformationModel.Detail> detailsList) {
        this.detailsList = detailsList;
    }
    //    public SingltonPojo(String check) {
//        this.check = check;
//    }

    public String getCheck() {
        return check;
    }

    public void setCheck(String check) {
        this.check = check;
    }

    String check,status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
