package omninos.com.farrycarrier.util;

public class ConstantData {

    public static final String SHOW_PROGRESS_MESSAGE = "Please Wait...";
    public static final String Fill_DETAILS = "Fill Mandatory Fields";
    public static final String USERID = "User_id";
    public static final String TOKEN = "Token";
    public static final String IMAGEPATH = "imagePath";
    public static final String Name = "name";
    public static final String Type = "Type";
    public static final String Wallet_Balance = "wallet_balance";

    public static final String CURRENT_LAT = "current_lat";
    public static final String CURRENT_LONG = "current_long";


}
